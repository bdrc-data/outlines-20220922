@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O8LS77509 {
    bda:LG0BDSWT5KVJ0QNJTG
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_o2.py" ;
        adm:logDate         "2024-02-12T14:35:53.546016"^^xsd:dateTime ;
        adm:logMessage      "update volume titles from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0O8LS77509_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/batch/import_isbn_o.py" ;
        adm:logDate         "2024-01-23T17:01:15.808647"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O8LS77509  a        adm:AdminData ;
        adm:adminAbout      bdr:O8LS77509 ;
        adm:graphId         bdg:O8LS77509 ;
        adm:logEntry        bda:LG0BDSWT5KVJ0QNJTG , bda:LG0O8LS77509_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW8LS77509_O8LS77509_LSNOTM6VOSEI_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationInstance  bdr:W8LS77509 ;
        bdo:contentLocationVolume  3 .
    
    bdr:CLMW8LS77509_O8LS77509_UUF2W8R2VNXS_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationInstance  bdr:W8LS77509 ;
        bdo:contentLocationVolume  2 .
    
    bdr:CLMW8LS77509_O8LS77509_VE6KRDZTVP0K_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationInstance  bdr:W8LS77509 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW8LS77509_BD3XA4NTM0F3
        a                   bf:Isbn ;
        rdf:value           "9787800570315" .
    
    bdr:IDMW8LS77509_M3L4D4R8HO7K
        a                   bf:Isbn ;
        rdf:value           "780057041X" .
    
    bdr:IDMW8LS77509_MZCK9YQKTS6F
        a                   bf:Isbn ;
        rdf:value           "7800570428" .
    
    bdr:IDMW8LS77509_V28403MYANPQ
        a                   bf:Isbn ;
        rdf:value           "9787800570414" .
    
    bdr:IDMW8LS77509_VN84LOGI0CM8
        a                   bf:Isbn ;
        rdf:value           "9787800570421" .
    
    bdr:IDMW8LS77509_WWDRM6MYVNQE
        a                   bf:Isbn ;
        rdf:value           "7800570312" .
    
    bdr:MW8LS77509_O8LS77509_LSNOTM6VOSEI
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW8LS77509_MZCK9YQKTS6F , bdr:IDMW8LS77509_VN84LOGI0CM8 ;
        bdo:contentLocation  bdr:CLMW8LS77509_O8LS77509_LSNOTM6VOSEI_001 ;
        bdo:inRootInstance  bdr:MW8LS77509 ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW8LS77509 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeVolume ;
        skos:prefLabel      "blo bzang dpal ldan gyi ljags rtsom phyogs bsgrigs/_(smad cha/"@bo-x-ewts .
    
    bdr:MW8LS77509_O8LS77509_UUF2W8R2VNXS
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW8LS77509_M3L4D4R8HO7K , bdr:IDMW8LS77509_V28403MYANPQ ;
        bdo:contentLocation  bdr:CLMW8LS77509_O8LS77509_UUF2W8R2VNXS_001 ;
        bdo:inRootInstance  bdr:MW8LS77509 ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW8LS77509 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeVolume ;
        skos:prefLabel      "blo bzang dpal ldan gyi ljags rtsom phyogs bsgrigs/_(bar cha/)"@bo-x-ewts .
    
    bdr:MW8LS77509_O8LS77509_VE6KRDZTVP0K
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW8LS77509_BD3XA4NTM0F3 , bdr:IDMW8LS77509_WWDRM6MYVNQE ;
        bdo:contentLocation  bdr:CLMW8LS77509_O8LS77509_VE6KRDZTVP0K_001 ;
        bdo:inRootInstance  bdr:MW8LS77509 ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW8LS77509 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeVolume ;
        skos:prefLabel      "blo bzang dpal ldan gyi ljags rtsom phyogs bsgrigs/_(stod cha/)"@bo-x-ewts .
    
    bdr:O8LS77509  a        bdo:Outline ;
        bdo:authorshipStatement  "Outline imported for recording ISBNs, should be completed in the future."@en ;
        bdo:isComplete      false ;
        bdo:outlineOf       bdr:MW8LS77509 .
}
