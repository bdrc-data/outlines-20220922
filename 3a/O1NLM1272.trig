@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM1272 {
    bda:O1NLM1272 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM1272 ;
        adm:gitPath "3a/O1NLM1272.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM1272 ;
        adm:logEntry bda:LG0NLMOO1NLM1272_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM1272_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM1272_O1NLM1272_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 334 ;
        bdo:contentLocationInstance bdr:W1NLM1272 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM1272_O1NLM1272_001_001 a bdr:NLMId ;
        rdf:value "M0059190-001" .

    bdr:IDMW1NLM1272_O1NLM1272_002_001 a bdr:NLMId ;
        rdf:value "M0059190-002" .

    bdr:MW1NLM1272_O1NLM1272_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1272_O1NLM1272_001_001 ;
        bdo:colophon "zhes pa 'di yang mdzod dge dgon gsar dga' ldan rab rgyas gling nas par du bkod/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM1272_O1NLM1272_001_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "53" ;
        bdo:hasTitle bdr:TTMW1NLM1272_O1NLM1272_001_001 ;
        bdo:inRootInstance bdr:MW1NLM1272 ;
        bdo:paginationExtentStatement "1a-167b" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM1272 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "dga' ldan rab rgyas gling/"@bo-x-ewts ;
        skos:prefLabel "bla ma rdo rje 'chang chen pos dkyil chog rdo rje phreng ba sogs kyis mtshon pa'i nyer mkho'i thig dang bshad pa phyag len du ma'i skor la brjed byang mdzad pa/"@bo-x-ewts .

    bdr:MW1NLM1272_O1NLM1272_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1272_O1NLM1272_002_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "53" ;
        bdo:hasTitle bdr:TTMW1NLM1272_O1NLM1272_002_001 ;
        bdo:inRootInstance bdr:MW1NLM1272 ;
        bdo:note bdr:NTMW1NLM1272_O1NLM1272_002CL ;
        bdo:paginationExtentStatement "1a-167a inc [*missing page from 126a-128b, 167b*]" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM1272 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bla ma rdo rje 'chang chen pos dkyil chog rdo rje phreng ba sogs kyis mtshon pa'i nyer mkho'i thig dang bshad pa phyag len du ma'i skor la brjed byang mdzad pa/"@bo-x-ewts .

    bdr:NTMW1NLM1272_O1NLM1272_002CL a bdo:Note ;
        bdo:noteText "Title present in the catalog but not digitized"@en .

    bdr:O1NLM1272 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM1272 .

    bdr:TTMW1NLM1272_O1NLM1272_001_001 a bdo:TitlePageTitle ;
        rdfs:label "bla ma rdo rje 'chang chen pos dkyil chog rdo rje phreng ba sogs kyis mtshon pa'i nyer mkho'i thig dang bshad pa phyag len du ma'i skor la brjed byang mdzad pa/"@bo-x-ewts .

    bdr:TTMW1NLM1272_O1NLM1272_002_001 a bdo:TitlePageTitle ;
        rdfs:label "bla ma rdo rje 'chang chen pos dkyil chog rdo rje phreng ba sogs kyis mtshon pa'i nyer mkho'i thig dang bshad pa phyag len du ma'i skor la brjed byang mdzad pa/"@bo-x-ewts .

    bdr:MW1NLM1272 bdo:hasPart bdr:MW1NLM1272_O1NLM1272_001,
            bdr:MW1NLM1272_O1NLM1272_002 .
}

