@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM1239 {
    bda:O1NLM1239 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM1239 ;
        adm:gitPath "6b/O1NLM1239.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM1239 ;
        adm:logEntry bda:LG0NLMOO1NLM1239_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM1239_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM1239_O1NLM1239_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 816 ;
        bdo:contentLocationInstance bdr:W1NLM1239 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM1239_O1NLM1239_001_001 a bdr:NLMId ;
        rdf:value "M0059157-001" .

    bdr:IDMW1NLM1239_O1NLM1239_002_001 a bdr:NLMId ;
        rdf:value "M0059157-002" .

    bdr:MW1NLM1239_O1NLM1239_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1239_O1NLM1239_001_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM1239_O1NLM1239_001_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "54" ;
        bdo:hasTitle bdr:TTMW1NLM1239_O1NLM1239_001_001 ;
        bdo:inRootInstance bdr:MW1NLM1239 ;
        bdo:paginationExtentStatement "1a-408a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM1239 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bstan bcos mngon pa rtogs pa'i rgyan 'grel pa dang bcas pa'i rnam bshad rnam pa gnyis kyi dga' ba'i gnas gsal bar byed pa legs bshad skal bzang klu dbang gi rol mtsho zhes bya ba las skabs dang po'i spyi don bzhus so/"@bo-x-ewts .

    bdr:MW1NLM1239_O1NLM1239_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1239_O1NLM1239_002_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentWidth "47" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "51.5" ;
        bdo:hasTitle bdr:TTMW1NLM1239_O1NLM1239_002_001 ;
        bdo:inRootInstance bdr:MW1NLM1239 ;
        bdo:note bdr:NTMW1NLM1239_O1NLM1239_002CL ;
        bdo:paginationExtentStatement "1a-408a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM1239 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "sku 'bum byams pa gling/"@bo-x-ewts ;
        skos:prefLabel "bstan bcos mngon pa rtogs pa'i rgyan 'grel pa dang bcas pa'i rnam bshad rnam pa gnyis kyi dga' ba'i gnas gsal bar byed pa legs bshad skal bzang klu dbang gi rol mtsho zhes bya ba las skabs dang po'i spyi don bzhus so/"@bo-x-ewts .

    bdr:NTMW1NLM1239_O1NLM1239_002CL a bdo:Note ;
        bdo:noteText "Title present in the catalog but not digitized"@en .

    bdr:O1NLM1239 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM1239 .

    bdr:TTMW1NLM1239_O1NLM1239_001_001 a bdo:TitlePageTitle ;
        rdfs:label "bstan bcos mngon pa rtogs pa'i rgyan 'grel pa dang bcas pa'i rnam bshad rnam pa gnyis kyi dga' ba'i gnas gsal bar byed pa legs bshad skal bzang klu dbang gi rol mtsho zhes bya ba las skabs dang po'i spyi don bzhus so/"@bo-x-ewts .

    bdr:TTMW1NLM1239_O1NLM1239_002_001 a bdo:TitlePageTitle ;
        rdfs:label "bstan bcos mngon pa rtogs pa'i rgyan 'grel pa dang bcas pa'i rnam bshad rnam pa gnyis kyi dga' ba'i gnas gsal bar byed pa legs bshad skal bzang klu dbang gi rol mtsho zhes bya ba las skabs dang po'i spyi don bzhus so/"@bo-x-ewts .

    bdr:MW1NLM1239 bdo:hasPart bdr:MW1NLM1239_O1NLM1239_001,
            bdr:MW1NLM1239_O1NLM1239_002 .
}

