@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM5290 {
    bda:O1NLM5290 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM5290 ;
        adm:gitPath "df/O1NLM5290.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM5290 ;
        adm:logEntry bda:LG0NLMOO1NLM5290_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM5290_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM5290_O1NLM5290_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 121 ;
        bdo:contentLocationInstance bdr:W1NLM5290 ;
        bdo:contentLocationPage 2 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5290_O1NLM5290_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 135 ;
        bdo:contentLocationInstance bdr:W1NLM5290 ;
        bdo:contentLocationPage 122 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5290_O1NLM5290_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 155 ;
        bdo:contentLocationInstance bdr:W1NLM5290 ;
        bdo:contentLocationPage 136 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5290_O1NLM5290_004_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 169 ;
        bdo:contentLocationInstance bdr:W1NLM5290 ;
        bdo:contentLocationPage 156 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5290_O1NLM5290_005_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 208 ;
        bdo:contentLocationInstance bdr:W1NLM5290 ;
        bdo:contentLocationPage 170 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM5290_O1NLM5290_001_001 a bdr:NLMId ;
        rdf:value "M0063208-001" .

    bdr:IDMW1NLM5290_O1NLM5290_002_001 a bdr:NLMId ;
        rdf:value "M0063208-002" .

    bdr:IDMW1NLM5290_O1NLM5290_003_001 a bdr:NLMId ;
        rdf:value "M0063208-003" .

    bdr:IDMW1NLM5290_O1NLM5290_004_001 a bdr:NLMId ;
        rdf:value "M0063208-004" .

    bdr:IDMW1NLM5290_O1NLM5290_005_001 a bdr:NLMId ;
        rdf:value "M0063208-005" .

    bdr:MW1NLM5290_O1NLM5290_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5290_O1NLM5290_001_001 ;
        bdo:authorshipStatement "rje thams cad mkhyen pa/"@bo-x-ewts ;
        bdo:colophon "rje thams cad mkhyen pas mdzad pa'o// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM5290_O1NLM5290_001_001 ;
        bdo:contentWidth "16" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "22" ;
        bdo:hasTitle bdr:TTMW1NLM5290_O1NLM5290_001_001 ;
        bdo:inRootInstance bdr:MW1NLM5290 ;
        bdo:paginationExtentStatement "1a-60a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM5290 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dod khams dbang phyug ma dmag zor rgyal mo'i sgrub thabs mngon rtogs/"@bo-x-ewts .

    bdr:MW1NLM5290_O1NLM5290_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5290_O1NLM5290_002_001 ;
        bdo:authorshipStatement "blo bzang rgyal ba'i ming can/"@bo-x-ewts ;
        bdo:colophon "ces bzlog pa bya zhing/ gtor ma glud dang bcas pa gnod phyogs su gom pa zhe dgu'i bar du skyal nas phyi mig mi blta'o/ de nas gtang rag gi mchod pa bzod gsol bsngo ba smon lam bkra shis dkyus bzhin bya'o/ 'di shin tu dka' snyan pas bag yod par bya'o/ /zhes dam can chos kyi rgyal po la brten nas bar gcod thams cad bzlog pa'i bzlog bsgyur gnam lcags 'bar ba'i rdo rje zhes bya ba 'di ni blo bzang rgyal ba'i ming can gyi sbyar ba'o// //bkra shis par gyur cig /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM5290_O1NLM5290_002_001 ;
        bdo:contentWidth "16" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "21.5" ;
        bdo:hasTitle bdr:TTMW1NLM5290_O1NLM5290_002_001 ;
        bdo:inRootInstance bdr:MW1NLM5290 ;
        bdo:paginationExtentStatement "1a-7b" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM5290 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dam can chos rgyal po la brten nas bar chad thams cad bzlog pa'i bzlog bzlog bsgyur gnam lcag 'bar ba'i rdo rje/"@bo-x-ewts .

    bdr:MW1NLM5290_O1NLM5290_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5290_O1NLM5290_003_001 ;
        bdo:authorshipStatement "skal bzang ming can/"@bo-x-ewts ;
        bdo:colophon "ces sogs/ spyi'i bzlog pa bya/ dpal ldan lha mo la brten pa'i glud rdzongs 'di ni rang dang/ skal mnyam gzhan la phan pa'i phyir ma mo'i glud rdzongs/ rgyas pa rnams las cung zad bsdus/ bsdus pa rnams las cung zad mnon te/ chos rje'i tha shal skal bzang ming can bdag gis bsgrigs pa la/ spyan ldan mkhas pa rnams la bzod par gsol/ yi ge pa ni u lim cu pi lig thu dge slong @#/ /blo bzang 'jam dpal lo// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM5290_O1NLM5290_003_001 ;
        bdo:contentWidth "17" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "21.5" ;
        bdo:hasTitle bdr:TTMW1NLM5290_O1NLM5290_003_001 ;
        bdo:inRootInstance bdr:MW1NLM5290 ;
        bdo:paginationExtentStatement "1a-10a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM5290 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "na mo'i glud rdzongs 'phral rkyen phyir bzlog gdon bgegs tshar gcod/"@bo-x-ewts .

    bdr:MW1NLM5290_O1NLM5290_004 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5290_O1NLM5290_004_001 ;
        bdo:colophon "zhes pa 'di ni chos mdzad bla mas grwa tshang gi 'don rgyun gyi go rim bzhin bsgrigs pa 'dis kyang rgyal ba'i bstan pa rin po che dar zhing rgyas la yun ring du gnas par gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM5290_O1NLM5290_004_001 ;
        bdo:contentWidth "18" ;
        bdo:dimHeight "7" ;
        bdo:dimWidth "22.5" ;
        bdo:hasTitle bdr:TTMW1NLM5290_O1NLM5290_004_001 ;
        bdo:inRootInstance bdr:MW1NLM5290 ;
        bdo:paginationExtentStatement "1a-7a" ;
        bdo:partIndex 4 ;
        bdo:partOf bdr:MW1NLM5290 ;
        bdo:partTreeIndex "004" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dpal ldan lha mo'i glo bur las bcol gyi gtor ma 'phen tshog nag po'i g.yul rgyal/"@bo-x-ewts .

    bdr:MW1NLM5290_O1NLM5290_005 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5290_O1NLM5290_005_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM5290_O1NLM5290_005_001 ;
        bdo:contentWidth "17" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "22" ;
        bdo:hasTitle bdr:TTMW1NLM5290_O1NLM5290_005_001 ;
        bdo:inRootInstance bdr:MW1NLM5290 ;
        bdo:paginationExtentStatement "1a-19a" ;
        bdo:partIndex 5 ;
        bdo:partOf bdr:MW1NLM5290 ;
        bdo:partTreeIndex "005" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "rgyal po chen po rnam thos sras kyi mngon rtogs bskang bshags bstod pa mnga' gsol/"@bo-x-ewts .

    bdr:O1NLM5290 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM5290 .

    bdr:TTMW1NLM5290_O1NLM5290_001_001 a bdo:TitlePageTitle ;
        rdfs:label "dod khams dbang phyug ma dmag zor rgyal mo'i sgrub thabs mngon rtogs/"@bo-x-ewts .

    bdr:TTMW1NLM5290_O1NLM5290_002_001 a bdo:TitlePageTitle ;
        rdfs:label "dam can chos rgyal po la brten nas bar chad thams cad bzlog pa'i bzlog bzlog bsgyur gnam lcag 'bar ba'i rdo rje/"@bo-x-ewts .

    bdr:TTMW1NLM5290_O1NLM5290_003_001 a bdo:TitlePageTitle ;
        rdfs:label "na mo'i glud rdzongs 'phral rkyen phyir bzlog gdon bgegs tshar gcod/"@bo-x-ewts .

    bdr:TTMW1NLM5290_O1NLM5290_004_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal ldan lha mo'i glo bur las bcol gyi gtor ma 'phen tshog nag po'i g.yul rgyal/"@bo-x-ewts .

    bdr:TTMW1NLM5290_O1NLM5290_005_001 a bdo:TitlePageTitle ;
        rdfs:label "rgyal po chen po rnam thos sras kyi mngon rtogs bskang bshags bstod pa mnga' gsol/"@bo-x-ewts .

    bdr:MW1NLM5290 bdo:hasPart bdr:MW1NLM5290_O1NLM5290_001,
            bdr:MW1NLM5290_O1NLM5290_002,
            bdr:MW1NLM5290_O1NLM5290_003,
            bdr:MW1NLM5290_O1NLM5290_004,
            bdr:MW1NLM5290_O1NLM5290_005 .
}

