@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7068 {
    bda:LG0NLMOO1NLM7068_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7068  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7068 ;
        adm:gitPath         "2a/O1NLM7068.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7068 ;
        adm:logEntry        bda:LG0NLMOO1NLM7068_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7068_O1NLM7068_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  102 ;
        bdo:contentLocationInstance  bdr:W1NLM7068 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7068_O1NLM7068_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  169 ;
        bdo:contentLocationInstance  bdr:W1NLM7068 ;
        bdo:contentLocationPage  103 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7068_O1NLM7068_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  274 ;
        bdo:contentLocationInstance  bdr:W1NLM7068 ;
        bdo:contentLocationPage  170 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7068_O1NLM7068_004_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  326 ;
        bdo:contentLocationInstance  bdr:W1NLM7068 ;
        bdo:contentLocationPage  275 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7068_O1NLM7068_005_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  368 ;
        bdo:contentLocationInstance  bdr:W1NLM7068 ;
        bdo:contentLocationPage  327 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7068_O1NLM7068_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0064986-001" .
    
    bdr:IDMW1NLM7068_O1NLM7068_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0064986-002" .
    
    bdr:IDMW1NLM7068_O1NLM7068_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0064986-003" .
    
    bdr:IDMW1NLM7068_O1NLM7068_004_001
        a                   bdr:NLMId ;
        rdf:value           "M0064986-004" .
    
    bdr:IDMW1NLM7068_O1NLM7068_005_001
        a                   bdr:NLMId ;
        rdf:value           "M0064986-005" .
    
    bdr:MW1NLM7068  bdo:hasPart  bdr:MW1NLM7068_O1NLM7068_001 , bdr:MW1NLM7068_O1NLM7068_002 , bdr:MW1NLM7068_O1NLM7068_003 , bdr:MW1NLM7068_O1NLM7068_004 , bdr:MW1NLM7068_O1NLM7068_005 .
    
    bdr:MW1NLM7068_O1NLM7068_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7068_O1NLM7068_001_001 ;
        bdo:authorshipStatement  "sid+d+his sa mu tra/"@bo-x-ewts ;
        bdo:colophon        "ces pa 'di yang srang rgyud rdo rje 'dzin pa'i slob ma'i tha shal sid+d+his sa mu tra ste dngos grub rgya mtshos bla ma'i gdams ngag gzhan dang thun mong ma yin+pa zab pa pas kyang ches zab pa 'di nyid brjed par phangs te zin bris su bkod pa'o/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7068_O1NLM7068_001_001 ;
        bdo:contentWidth    "18" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "22" ;
        bdo:hasTitle        bdr:TTMW1NLM7068_O1NLM7068_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7068 ;
        bdo:paginationExtentStatement  "1a-50a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7068 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "dpal rdo rje 'jigs byed chen po zab don rim pa gnyis pa'i lam la 'khrid tshul yongs 'dzin bla ma'i gsung bzhin zin bris su bkod pa/"@bo-x-ewts .
    
    bdr:MW1NLM7068_O1NLM7068_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7068_O1NLM7068_002_001 ;
        bdo:authorshipStatement  "bstan pa'i sgron me/"@bo-x-ewts ;
        bdo:colophon        "zhes gsungs pa ltar! bla ma dam pa rnams kyi gzhung man ngag byin rlabs can byang chub lam rm gyi snying po'i nying khu zhim mngar 'o ma ldud pa'i bdud rtsi'i bcud len lam bzang bgrod pa'i 'jug ngogs zhes bya ba 'di ni/ bla ma dge ba bshes gnyen la bka' drin dran pa'i sgo nas gus pas lus ngag yid gsum gyis bsnyen bkur dang phan btags pa'i bsam pa blo dad pa dang ldan pa'i slob ma dge slong bstan pa'i sgron mes/ lam rim la bag chags ci thebs pa dang shar bsgoms kyi tsam byed pa'i tshe bla ma'i man ngag nyams su len dgos zhes yang yang bskul ba'i ngor/ btsun gzugs 'dzin pa'i dge slong blo bzang bsam gtan gyis bla ma gong ma rnams kyi man ngag la brten nas/ de'i man ngag la gzhung las btus nas lam rim grwa tshang bsam 'phel gling la bsgrigs pa'i @#/ /'di man ngag gzhung bzang stong gi chu bo 'du ba'i phyir/ thun dang thun gyi mtshams su sems ngal zhum mer par brtson 'grus yang yang blta na mig nas mchi ma byung nas/ yid 'gyur ba'i myong ba nges par thon pas yid ches gces spras su bsten par gyis shig /yi ge pa ni bskul ba po dge slong bstan pa'i sgron mes bris pa'o/ /'dis kyang rgyal ba'i bstan pa'i snyin po byang chub lam rim gyi gdams pa zang mo lam bzang de dag rang gzhan gyi rgyud la myur du skye bar gyur cig /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7068_O1NLM7068_002_001 ;
        bdo:contentWidth    "18.5" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "22" ;
        bdo:hasTitle        bdr:TTMW1NLM7068_O1NLM7068_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7068 ;
        bdo:paginationExtentStatement  "1a-34a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7068 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "bla ma dam pa rnams kyi gzhung man ngag byin rlabs can byang chub lam rim gyi snying po'i nying khu zhim mngar 'o ma ldud pa'i bdud rtsi'i bcud len lam bzang bgrod pa'i 'jug ngogs/"@bo-x-ewts .
    
    bdr:MW1NLM7068_O1NLM7068_003
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7068_O1NLM7068_003_001 ;
        bdo:colophon        "'di phag lo'i ston dus dbus su lha sa ser chos sdings su/ bla ma rin po che blo bzang grags pa'i zhal snga nas/ drung 'khor nyi shu rtsa lnga la gsang 'dus rim lnga'i gnang dus/ slob dpon bka' bcu pa dar ma rin chen gyis zin bris su mdzad cing bka' rgya shin tu dam par mdzad do// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7068_O1NLM7068_003_001 ;
        bdo:contentWidth    "18.5" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "22" ;
        bdo:hasTitle        bdr:TTMW1NLM7068_O1NLM7068_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM7068 ;
        bdo:paginationExtentStatement  "1a-53a" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM7068 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "gsang 'dus dka' gnad zin bris/"@bo-x-ewts .
    
    bdr:MW1NLM7068_O1NLM7068_004
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7068_O1NLM7068_004_001 ;
        bdo:authorshipStatement  "blo bzang chos kyi nyi ma/"@bo-x-ewts ;
        bdo:colophon        "ces byang chub lam gyi rim pa'i sngon 'gro @#/ /bla ma'i rnal 'byor thun mongs dang min pa kun mkhyen lam bzang zhes bya ba 'di ni/ chos sde chen po dpal ldan 'bras spungs su don nyer pa mang po la/ byang chub lam gyi rim pa'i dmar khrid bgyis skabs su zhal shes snyan brgyud sngon chad yi ger med pa rnams kyis shigs ces rang gi slob ma'i rnams kyis dngul gyi maN+Dal lha rdzasma dri med dang bcas bskul ba la brten pa yongs rdzogs bstan pa'i mnga' bdag rdo rje 'chang ye shes bstan pa'i sgron me dpal bzang po dang! rje btsun bla ma thub dbang rdo rje 'chang dang dbyer ma mchis pa bka' drin tshungs med mai tri'i mtshan can sogs yongs 'dzin chos bzhin du spyod pa du ma'i zhabs rdul spyi bor brten pa'i btsun gzugs snyoms las pa blo bzang chos kyi nyi ma sbyar ba'i yi ge pa ni dpyod ldan rab 'byams smras pa blo bzang rgyal mtshan gyis bgyis pa'o/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7068_O1NLM7068_004_001 ;
        bdo:contentWidth    "18" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "22" ;
        bdo:hasTitle        bdr:TTMW1NLM7068_O1NLM7068_004_001 ;
        bdo:inRootInstance  bdr:MW1NLM7068 ;
        bdo:paginationExtentStatement  "1a-25a" ;
        bdo:partIndex       4 ;
        bdo:partOf          bdr:MW1NLM7068 ;
        bdo:partTreeIndex   "004" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "byang chub lam gyi rim pa'i sngon du bla ma'i rnal 'byor zhal shes thun mongs ma yin pa kun mkhyen lam bzang /"@bo-x-ewts .
    
    bdr:MW1NLM7068_O1NLM7068_005
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7068_O1NLM7068_005_001 ;
        bdo:authorshipStatement  "dbyangs can dga' ba'i blo gros/"@bo-x-ewts ;
        bdo:colophon        "ces pa 'di ni chu mo bya lo ston dus mkhas grub chen po shing za rdo rje 'chang blo bzang dar rgyas dpal bzang po nas bstan 'dzin skyes chen dam pa mang pos gtso mdzad 'khor slob brgya phrag mang por dpal gsang ba 'dus pa'i man ngag 'grel @#/ /pa bzhi sprag gi bshad lung gnang skabs gsud bshad rgya mtsho lta bu las dam pa'i phyag len dang gsung bshad 'ga' zhig rab 'byams smra ba su d+hI ar+tha sid+d+hi'am/ ming gzhan dbyangs can dga' ba'i blo gros su 'bod pas brjed byang du bris pa dge legs 'phel// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7068_O1NLM7068_005_001 ;
        bdo:contentWidth    "18" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "22" ;
        bdo:hasTitle        bdr:TTMW1NLM7068_O1NLM7068_005_001 ;
        bdo:inRootInstance  bdr:MW1NLM7068 ;
        bdo:paginationExtentStatement  "1a-21a" ;
        bdo:partIndex       5 ;
        bdo:partOf          bdr:MW1NLM7068 ;
        bdo:partTreeIndex   "005" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "'grel pa bzhi sbrag gi bshad lung skabs kyi gsung bshad 'ga' zhig zin bris bkod pa/"@bo-x-ewts .
    
    bdr:O1NLM7068  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7068 .
    
    bdr:TTMW1NLM7068_O1NLM7068_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dpal rdo rje 'jigs byed chen po zab don rim pa gnyis pa'i lam la 'khrid tshul yongs 'dzin bla ma'i gsung bzhin zin bris su bkod pa/"@bo-x-ewts .
    
    bdr:TTMW1NLM7068_O1NLM7068_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bla ma dam pa rnams kyi gzhung man ngag byin rlabs can byang chub lam rim gyi snying po'i nying khu zhim mngar 'o ma ldud pa'i bdud rtsi'i bcud len lam bzang bgrod pa'i 'jug ngogs/"@bo-x-ewts .
    
    bdr:TTMW1NLM7068_O1NLM7068_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gsang 'dus dka' gnad zin bris/"@bo-x-ewts .
    
    bdr:TTMW1NLM7068_O1NLM7068_004_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "byang chub lam gyi rim pa'i sngon du bla ma'i rnal 'byor zhal shes thun mongs ma yin pa kun mkhyen lam bzang*/"@bo-x-ewts .
    
    bdr:TTMW1NLM7068_O1NLM7068_005_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "'grel pa bzhi sbrag gi bshad lung skabs kyi gsung bshad 'ga' zhig zin bris bkod pa/"@bo-x-ewts .
}
