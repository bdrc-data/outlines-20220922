@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM1436 {
    bda:O1NLM1436 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM1436 ;
        adm:gitPath "52/O1NLM1436.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM1436 ;
        adm:logEntry bda:LG0NLMOO1NLM1436_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM1436_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM1436_O1NLM1436_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 318 ;
        bdo:contentLocationInstance bdr:W1NLM1436 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM1436_O1NLM1436_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 510 ;
        bdo:contentLocationInstance bdr:W1NLM1436 ;
        bdo:contentLocationPage 319 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM1436_O1NLM1436_001_001 a bdr:NLMId ;
        rdf:value "M0059354-001" .

    bdr:IDMW1NLM1436_O1NLM1436_002_001 a bdr:NLMId ;
        rdf:value "M0059354-002" .

    bdr:MW1NLM1436_O1NLM1436_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1436_O1NLM1436_001_001 ;
        bdo:colophon "bdud rtsi snying po yan lag brgyad pa gsang ba man ngag gi rgyud las rgyud yongs su gtang ba'i le'u ste nyi shu rtsa bdun pa'o// //bdud rtsi snying po yan lag brgyad pa gsang ba man ngag gi rgyud ces bya ba sangs rgyas sman gyi bla baiTUr+ya 'od kyi rgyal po'i byin rlabs las sprul pa'i ston pa dang drang srong rig pa'i ye shes dang drang srong yid las skyes kyis zhus lan du mdzad pa gso dpyad @#/ /kun gyi rgyal po zhes bya ba yongs su rdzogs so/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM1436_O1NLM1436_001_001 ;
        bdo:contentWidth "23.5" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "30.5" ;
        bdo:hasTitle bdr:TTMW1NLM1436_O1NLM1436_001_001 ;
        bdo:inRootInstance bdr:MW1NLM1436 ;
        bdo:paginationExtentStatement "1b-159a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM1436 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bdud rtsi snying po yan lag brgyad pa gsang ba man ngag gi rgyud ces bya ba sangs rgyas sman gyi bla baiDUr+ya 'od kyi rgyal po'i byin rlabs las sprul pa'i ston pa dang drang srong rig pa'i ye shes dang drang srong yid las skyes kyis zhus lan du mdzad pa gso dpyad kun gyi rgyal po zhes bya ba yongs su/"@bo-x-ewts .

    bdr:MW1NLM1436_O1NLM1436_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1436_O1NLM1436_002_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM1436_O1NLM1436_002_001 ;
        bdo:contentWidth "23.5" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "30.5" ;
        bdo:hasTitle bdr:TTMW1NLM1436_O1NLM1436_002_001 ;
        bdo:inRootInstance bdr:MW1NLM1436 ;
        bdo:paginationExtentStatement "6a-109b inc [*missing page from 1a-5b; 110a *]" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM1436 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bdud rtsi snying po yan lag brgyad pa gsang ba man ngag gi rgyud/"@bo-x-ewts .

    bdr:O1NLM1436 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM1436 .

    bdr:TTMW1NLM1436_O1NLM1436_001_001 a bdo:TitlePageTitle ;
        rdfs:label "bdud rtsi snying po yan lag brgyad pa gsang ba man ngag gi rgyud ces bya ba sangs rgyas sman gyi bla baiDUr+ya 'od kyi rgyal po'i byin rlabs las sprul pa'i ston pa dang drang srong rig pa'i ye shes dang drang srong yid las skyes kyis zhus lan du mdzad pa gso dpyad kun gyi rgyal po zhes bya ba yongs su/"@bo-x-ewts .

    bdr:TTMW1NLM1436_O1NLM1436_002_001 a bdo:TitlePageTitle ;
        rdfs:label "bdud rtsi snying po yan lag brgyad pa gsang ba man ngag gi rgyud/"@bo-x-ewts .

    bdr:MW1NLM1436 bdo:hasPart bdr:MW1NLM1436_O1NLM1436_001,
            bdr:MW1NLM1436_O1NLM1436_002 .
}

