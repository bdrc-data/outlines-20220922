@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM3999 {
    bda:LG0NLMOO1NLM3999_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0O1NLM3999_YCTHBFPMMJF1
        a                   adm:UpdateData ;
        adm:logDate         "2024-07-02T20:18:17.677679Z"^^xsd:dateTime ;
        adm:logMessage      "བཅོས"@en ;
        adm:logWho          bdu:U2089842660 .
    
    bda:O1NLM3999  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM3999 ;
        adm:gitPath         "c9/O1NLM3999.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM3999 ;
        adm:logEntry        bda:LG0NLMOO1NLM3999_IDC001 , bda:LG0O1NLM3999_YCTHBFPMMJF1 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM3999_O1NLM3999_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  156 ;
        bdo:contentLocationEndVolume  1 ;
        bdo:contentLocationInstance  bdr:W1NLM3999 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM3999_O1NLM3999_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  270 ;
        bdo:contentLocationEndVolume  1 ;
        bdo:contentLocationInstance  bdr:W1NLM3999 ;
        bdo:contentLocationPage  157 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM3999_O1NLM3999_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0061917-001" .
    
    bdr:IDMW1NLM3999_O1NLM3999_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0061917-002" .
    
    bdr:MW1NLM3999  bdo:hasPart  bdr:MW1NLM3999_O1NLM3999_001 , bdr:MW1NLM3999_O1NLM3999_002 .
    
    bdr:MW1NLM3999_O1NLM3999_001
        a                   bdo:Instance ;
        skos:prefLabel      "dge slong gi khrims nyis brgya lnga bcu rtsa gsum gyi blang dor phyin ci ma log pa nyams su len tshul gyi bslab bya gnam rtse ldeng mar grags pa/"@bo-x-ewts ;
        bf:identifiedBy     bdr:IDMW1NLM3999_O1NLM3999_001_001 ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM3999_O1NLM3999_001_001 ;
        bdo:contentWidth    "48.5" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "54.5" ;
        bdo:hasTitle        bdr:TTMW1NLM3999_O1NLM3999_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM3999 ;
        bdo:paginationExtentStatement  "1a-78a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM3999 ;
        bdo:partTreeIndex   "01" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 .
    
    bdr:MW1NLM3999_O1NLM3999_002
        a                   bdo:Instance ;
        skos:prefLabel      "byang chub sems dpa'i spyod pa la 'jug pa/"@bo-x-ewts ;
        bf:identifiedBy     bdr:IDMW1NLM3999_O1NLM3999_002_001 ;
        bdo:authorshipStatement  "shAn ti de ba/"@bo-x-ewts ;
        bdo:colophon        "byang chub sems dpa'i spyod pa la 'jug pa slob dpon shAn ta de bas mdzad pa rdzogs so//_rgya gar gyi mkhan po sarba dz+nyA de ba dang/_zhu chen gyi lo tsA ba ban+de dpal brtsegs kyis kha che'i dpe las zhus te gtan la phab pa las/_slad kyis rgya gar gyi mkhan po d+harma shrI b+ha dra dang/_zhu chen gyi lo tsA ba ban+de rin chen bzang po dang/_shAkya blo gros kyis yul dbus kyi 'grel pa dang/_mthun par bcos shing bsgyur te gtan la phab pa'o/_/yang dus phyis rgya gar gyi mkhan po su ma ti ki rti dang/_zhu chen gyi lo tsA ba dge slong blo ldan shes rab kyis dag par bcos shing bsgyur te legs par gtan la phab pa'o/_/oM saM b+ha ra saM b+ha ra b+hi ma na sa ra mahA dza ba hUM phaT s+w+'a hA/_bcom ldan 'das kyis gsungs pa'i chos kyi tshig gcig gi don gang yin de ni chos thams cad kyi yang yin na/_/'on kyang/_bdag ni rang gi las kyi nyes pas shes rab chung zhing blo zhan te/_de lta yin tu zin kyang gang bdag gis/_/bcom ldan 'das kyi gsung rab glegs bam du byas pa bklag par bya ste/_/chos kyi sgra de sems can gang rnams kyi rnal ma du grag par gyur ba de dag de bzhin gshegs pa'i spobs pa thob par gyur cig_//sangs rgyas bstan pa'i snying po zhwa ser bstan/_/'dzin skyong spel la khur 'khyer spos kyi glang/_/brgya phrag dka' yang bstan pa'i rgyal mtshan 'dzin/_/gnam bskos bstan 'dzin lha dbang chos rgyal gyis/_/byang chub spyod la 'jug pa'i dri med gsung/_/chos sbyin rgya cher spel phyir par du bsgrubs/_/dge 'dis mtshon pa'i rnam dkar dge ba'i mthus/_ /'jam dpal snying po'i bstan pa dbyar rnga bzhin/_/phyogs bcu kun tu bar med sgrog pa dang/_/khyad par tshe 'das tshe ring bkra shis kyi/_/sgrib gnyis sbyangs shing tshogs gnyis rab rdzogs nas/_/rnam mkhyen go 'phang myur du thob par shog /_go 'phang dam pa mngon du gyur nas kyang/_/byang chub sems kyi lag pa ring brkyangs nas/_/'gro kun rnam mkhyen 'thob byed 'phags nor bdun/_/'dzad pa med par longs su spyod par shog_//_de ltar bskrun pa'i mthu las byung ba yis/_/'gro la bde stsol stobs ldan chos rgyal gyi/_/chab srid mnga' thang dbyar gyi chu gter ltar/_/rab tu 'phel zhing kun tu rgyas pa ru/_/bskal brgya'i bar du gnas pa'i rgyu ru bsngo/_/yon tan kun gyi gzhi gyur bla ma dang/_/mchog mthun dngos grub stsol ba'i yi dam lha/_/bgegs dpung ma lus 'joms mdzad srungs tshogs kyi/_/bsam don myur du 'grub pa'i bkra shis shog /_//spyod 'jug gi par byang/_gsung rab kun gyi snying po'i chu klung 'bum/_/nyams len gdams par rab 'khyil rgyal mtsho che/_/byang chub spyod par 'jug pa'i gru rdzings par/_/bskrun pa'i dge rtsa kun+da ltar dkar bas/_/lun rtogs rgyal ba'i bstan pa mi nyams shing/_/bdag sogs 'gro kun rgyal sras spyod pa mchog_//zung 'brel nyams len gnad rnams mthar phyin nas/_/sangs rgyas go 'phang bde blag 'thob par shog_//ces pa 'di ni er te ni thu she ye thu dar rgan nang so ye shes skal bzang gis khal kha phyogs su spyod 'jug gi par byang re dgos zhes bskul ba ltar shAkya'i dge slong blo bzang skal bzang rgya mtshos sbyar ba'o//_//"@bo-x-ewts ;
        bdo:conditionGrade  3 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM3999_O1NLM3999_002_001 ;
        bdo:contentWidth    "45" ;
        bdo:dimHeight       "9.5" ;
        bdo:dimWidth        "54" ;
        bdo:hasTitle        bdr:TTMW1NLM3999_O1NLM3999_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM3999 ;
        bdo:paginationExtentStatement  "1a-58a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM3999 ;
        bdo:partTreeIndex   "02" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        bdo:sourcePrinteryStatement  "bkra shis chos 'phel gling/"@bo-x-ewts .
    
    bdr:O1NLM3999  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM3999 .
    
    bdr:TTMW1NLM3999_O1NLM3999_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dge slong gi khrims nyis brgya lnga bcu rtsa gsum gyi blang dor phyin ci ma log pa nyams su len tshul gyi bslab bya gnam rtse ldeng mar grags pa/"@bo-x-ewts .
    
    bdr:TTMW1NLM3999_O1NLM3999_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "byang chub sems dpa'i spyod pa la 'jug pa/"@bo-x-ewts .
}
