@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7123 {
    bda:LG0NLMOO1NLM7123_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7123  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7123 ;
        adm:gitPath         "c9/O1NLM7123.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7123 ;
        adm:logEntry        bda:LG0NLMOO1NLM7123_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7123_O1NLM7123_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  154 ;
        bdo:contentLocationInstance  bdr:W1NLM7123 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7123_O1NLM7123_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  368 ;
        bdo:contentLocationInstance  bdr:W1NLM7123 ;
        bdo:contentLocationPage  155 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7123_O1NLM7123_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0065041-001" .
    
    bdr:IDMW1NLM7123_O1NLM7123_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0065041-002" .
    
    bdr:MW1NLM7123  bdo:hasPart  bdr:MW1NLM7123_O1NLM7123_001 , bdr:MW1NLM7123_O1NLM7123_002 .
    
    bdr:MW1NLM7123_O1NLM7123_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7123_O1NLM7123_001_001 ;
        bdo:conditionGrade  3 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7123_O1NLM7123_001_001 ;
        bdo:contentWidth    "27" ;
        bdo:dimHeight       "9.5" ;
        bdo:dimWidth        "32.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7123_O1NLM7123_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7123 ;
        bdo:paginationExtentStatement  "1a-74a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7123 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "rtsis gzhung chen mo rnams las che long tsam du btus pa'i 'bras rtsis bshad pa 'thor bu pa/"@bo-x-ewts .
    
    bdr:MW1NLM7123_O1NLM7123_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7123_O1NLM7123_002_001 ;
        bdo:conditionGrade  3 ;
        bdo:contentHeight   "9" ;
        bdo:contentLocation  bdr:CLMW1NLM7123_O1NLM7123_002_001 ;
        bdo:contentWidth    "27" ;
        bdo:dimHeight       "9.5" ;
        bdo:dimWidth        "32" ;
        bdo:hasTitle        bdr:TTMW1NLM7123_O1NLM7123_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7123 ;
        bdo:paginationExtentStatement  "1a-107b" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7123 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "'bras rtsis ni/"@bo-x-ewts .
    
    bdr:O1NLM7123  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7123 .
    
    bdr:TTMW1NLM7123_O1NLM7123_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rtsis gzhung chen mo rnams las che long tsam du btus pa'i 'bras rtsis bshad pa 'thor bu pa/"@bo-x-ewts .
    
    bdr:TTMW1NLM7123_O1NLM7123_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "'bras rtsis ni/"@bo-x-ewts .
}
