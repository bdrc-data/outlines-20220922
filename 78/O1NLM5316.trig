@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM5316 {
    bda:O1NLM5316 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM5316 ;
        adm:gitPath "78/O1NLM5316.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM5316 ;
        adm:logEntry bda:LG0NLMOO1NLM5316_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM5316_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM5316_O1NLM5316_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 160 ;
        bdo:contentLocationInstance bdr:W1NLM5316 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5316_O1NLM5316_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 212 ;
        bdo:contentLocationInstance bdr:W1NLM5316 ;
        bdo:contentLocationPage 161 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5316_O1NLM5316_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 220 ;
        bdo:contentLocationInstance bdr:W1NLM5316 ;
        bdo:contentLocationPage 213 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5316_O1NLM5316_004_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 232 ;
        bdo:contentLocationInstance bdr:W1NLM5316 ;
        bdo:contentLocationPage 221 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5316_O1NLM5316_005_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 302 ;
        bdo:contentLocationInstance bdr:W1NLM5316 ;
        bdo:contentLocationPage 233 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM5316_O1NLM5316_001_001 a bdr:NLMId ;
        rdf:value "M0063234-001" .

    bdr:IDMW1NLM5316_O1NLM5316_002_001 a bdr:NLMId ;
        rdf:value "M0063234-002" .

    bdr:IDMW1NLM5316_O1NLM5316_003_001 a bdr:NLMId ;
        rdf:value "M0063234-003" .

    bdr:IDMW1NLM5316_O1NLM5316_004_001 a bdr:NLMId ;
        rdf:value "M0063234-004" .

    bdr:IDMW1NLM5316_O1NLM5316_005_001 a bdr:NLMId ;
        rdf:value "M0063234-005" .

    bdr:MW1NLM5316_O1NLM5316_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5316_O1NLM5316_001_001 ;
        bdo:authorshipStatement "chos kyi rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "dge slong blo bzang 'od zer gyis par du brko ba'o/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM5316_O1NLM5316_001_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "53" ;
        bdo:hasTitle bdr:TTMW1NLM5316_O1NLM5316_001_001 ;
        bdo:inRootInstance bdr:MW1NLM5316 ;
        bdo:paginationExtentStatement "1a-80a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM5316 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "rje btsun chos kyi rgyal mtshan gyis mdzad pa'i drang nges rnam 'byed kyi spyi don rgol ngan tshar gcod rin po che'i 'phreng ba/"@bo-x-ewts .

    bdr:MW1NLM5316_O1NLM5316_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5316_O1NLM5316_002_001 ;
        bdo:authorshipStatement "dbyangs can dga' ba'i blo gros/"@bo-x-ewts ;
        bdo:colophon "ces pa 'di ni dad brtson rnam dpyod yangs pa khe shag thu'i dge slong grags pa chos bzang gis yang yang nan tan che ba'i ngor/ mkhas grub dam pa mang po'i zhabs rdul spyi bo'i rgyan du thob pa'i gyi na pa \\u0f38 dbyangs can dga' ba'i blo gros kyis rang gzhan la phan pa'i bsam pas g.yar khrul du sbyar ba sarbe hi tan+tu// // ces pa 'di yang dka' bcu pa bsam tan gyis bde chen lhun grub gling gi rgyud pa grwa tshang du par du sgrubs pa'o/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM5316_O1NLM5316_002_001 ;
        bdo:contentWidth "47" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "55" ;
        bdo:hasTitle bdr:TTMW1NLM5316_O1NLM5316_002_001 ;
        bdo:inRootInstance bdr:MW1NLM5316 ;
        bdo:paginationExtentStatement "1a-26a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM5316 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dpal gsang ba 'dus pa 'phags lugs dang mthun pa'i sa lam rnam bzhag legs bshad skal bzang 'jug ngogs zhes pa/"@bo-x-ewts .

    bdr:MW1NLM5316_O1NLM5316_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5316_O1NLM5316_003_001 ;
        bdo:authorshipStatement "ngag dbang blo bzang chos ldan/"@bo-x-ewts ;
        bdo:colophon "ces lha mo 'od zer can gyi bsgrub thabs 'di ni don gnyer can 'ga' zhig gis bskul ba'i ngor ban chung gyi ni pa ngag dbang blo bzang chos ldan gyis bsgrub thabs rgya rtsa nas bkol te/ bsgrigs pa'i yi ge pa ni rab 'byams smra ba blo bzang nyi ma'o/ /dge legs 'phel bar gyur cig /mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM5316_O1NLM5316_003_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "53" ;
        bdo:hasTitle bdr:TTMW1NLM5316_O1NLM5316_003_001 ;
        bdo:inRootInstance bdr:MW1NLM5316 ;
        bdo:paginationExtentStatement "1a-4b" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM5316 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "lha mo 'od zer can gyi bsgrub thabs/"@bo-x-ewts .

    bdr:MW1NLM5316_O1NLM5316_004 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5316_O1NLM5316_004_001 ;
        bdo:authorshipStatement "chos ming pa/"@bo-x-ewts ;
        bdo:colophon "ces pa 'di ni btsun gzugs chos ming pas sbyar ba'o// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM5316_O1NLM5316_004_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "53" ;
        bdo:hasTitle bdr:TTMW1NLM5316_O1NLM5316_004_001 ;
        bdo:inRootInstance bdr:MW1NLM5316 ;
        bdo:paginationExtentStatement "1a-6a" ;
        bdo:partIndex 4 ;
        bdo:partOf bdr:MW1NLM5316 ;
        bdo:partTreeIndex "004" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "shes rab snying po'i mchan 'grel mdor bsdus/"@bo-x-ewts .

    bdr:MW1NLM5316_O1NLM5316_005 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5316_O1NLM5316_005_001 ;
        bdo:authorshipStatement "ser stod zur ming pa/"@bo-x-ewts ;
        bdo:colophon "zhes phar phyin gyi ma yig rnying pa'i gzhung gong 'og dang/ spyi mtha' phan tshun tshig zin mi mthun pa'i rigs rnams don la gang gnas zhib dpyad thog /rang bzo'i dri mas ma slad par spyi mtha' gang yang rung ba'i dgongs par gnas nges kyi bsgyur bcos phran bu byas rigs rnams gnas skabs dogs sel gyi tshul du bkod pa 'di bzhin skyabs @#/ /rje btsun mtshangs med yongs 'dzin rin po che nas thugs zhib mdzad thog 'di ltar bgyis 'thud ces gsung gnang ba thob don ltar/ ser stod zur ming bas bgyis pa dge legs 'phel// //bkra shis// zhus dag byed 'dis mtshan bdag gzhan dge tshogs ci so cog dus gsum rgyal dang de sras bcas pa yi/ /rgya chen smon lam mdzad pa kun 'grub cing/ /dam chos 'dzin dang rgyas pa'i rgyu ru sngo/ / se ra rje btsun chos kyi rgyal mtshan gyi yig cha dbu phar gnyis dogs gcod har dzas sang phyag rdor skyabs kyis par du sgrubs te 'dul khrims gtsang srung yid dga' chos 'dzin gling du bzhugs/ / dge'o// mang+ga laM//"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM5316_O1NLM5316_005_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "9.0" ;
        bdo:dimWidth "52.0" ;
        bdo:hasTitle bdr:TTMW1NLM5316_O1NLM5316_005_001 ;
        bdo:inRootInstance bdr:MW1NLM5316 ;
        bdo:paginationExtentStatement "1a-35a" ;
        bdo:partIndex 5 ;
        bdo:partOf bdr:MW1NLM5316 ;
        bdo:partTreeIndex "005" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "skabs dang po sa lam zur rkol bzhi skabs gnyis pa bzhi pa lnga pa brgyad pa'i bar gyi phar phyin yig cha'i dogs gcod/"@bo-x-ewts .

    bdr:O1NLM5316 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM5316 .

    bdr:TTMW1NLM5316_O1NLM5316_001_001 a bdo:TitlePageTitle ;
        rdfs:label "rje btsun chos kyi rgyal mtshan gyis mdzad pa'i drang nges rnam 'byed kyi spyi don rgol ngan tshar gcod rin po che'i 'phreng ba/"@bo-x-ewts .

    bdr:TTMW1NLM5316_O1NLM5316_002_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal gsang ba 'dus pa 'phags lugs dang mthun pa'i sa lam rnam bzhag legs bshad skal bzang 'jug ngogs zhes pa/"@bo-x-ewts .

    bdr:TTMW1NLM5316_O1NLM5316_003_001 a bdo:TitlePageTitle ;
        rdfs:label "lha mo 'od zer can gyi bsgrub thabs/"@bo-x-ewts .

    bdr:TTMW1NLM5316_O1NLM5316_004_001 a bdo:TitlePageTitle ;
        rdfs:label "shes rab snying po'i mchan 'grel mdor bsdus/"@bo-x-ewts .

    bdr:TTMW1NLM5316_O1NLM5316_005_001 a bdo:TitlePageTitle ;
        rdfs:label "skabs dang po sa lam zur rkol bzhi skabs gnyis pa bzhi pa lnga pa brgyad pa'i bar gyi phar phyin yig cha'i dogs gcod/"@bo-x-ewts .

    bdr:MW1NLM5316 bdo:hasPart bdr:MW1NLM5316_O1NLM5316_001,
            bdr:MW1NLM5316_O1NLM5316_002,
            bdr:MW1NLM5316_O1NLM5316_003,
            bdr:MW1NLM5316_O1NLM5316_004,
            bdr:MW1NLM5316_O1NLM5316_005 .
}

