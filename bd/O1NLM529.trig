@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM529 {
    bda:O1NLM529 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM529 ;
        adm:gitPath "bd/O1NLM529.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM529 ;
        adm:logEntry bda:LG0NLMOO1NLM529_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM529_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM529_O1NLM529_001B_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 86 ;
        bdo:contentLocationInstance bdr:W1NLM529 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM529_O1NLM529_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 116 ;
        bdo:contentLocationInstance bdr:W1NLM529 ;
        bdo:contentLocationPage 87 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM529_O1NLM529_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 138 ;
        bdo:contentLocationInstance bdr:W1NLM529 ;
        bdo:contentLocationPage 117 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM529_O1NLM529_004_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 152 ;
        bdo:contentLocationInstance bdr:W1NLM529 ;
        bdo:contentLocationPage 139 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM529_O1NLM529_005_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 180 ;
        bdo:contentLocationInstance bdr:W1NLM529 ;
        bdo:contentLocationPage 153 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM529_O1NLM529_001B_001 a bdr:NLMId ;
        rdf:value "M0058442-001" .

    bdr:IDMW1NLM529_O1NLM529_002_001 a bdr:NLMId ;
        rdf:value "M0058442-002" .

    bdr:IDMW1NLM529_O1NLM529_003_001 a bdr:NLMId ;
        rdf:value "M0058442-003" .

    bdr:IDMW1NLM529_O1NLM529_004_001 a bdr:NLMId ;
        rdf:value "M0058442-004" .

    bdr:IDMW1NLM529_O1NLM529_005_001 a bdr:NLMId ;
        rdf:value "M0058442-005" .

    bdr:MW1NLM529_O1NLM529_001B a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM529_O1NLM529_001B_001 ;
        bdo:authorshipStatement "kun dga' rgya mtsho/"@bo-x-ewts ;
        bdo:colophon "ces pa ni 'bro ba rab 'byams pa kun dga' rgya mtsho zhes bya bas byis pa rnams bod yig glog pa la myur bar slob pa'i thabs yod dam med ces bsam nas shog gu gnyis gsum tsam bsgrigs gam snyam pas bris shing yang nas yang du bsnan par 'dod nas shog gu lnga bcu tsam bri pa tshar la khad du rmi lam legs po zhig dpe 'di dar rgyas su 'gro ba'i ltas byung te byis pa rnams la nges par phan par 'gyur ro snyam pa dang/ 'di ka'i rjes su yang 'di'i nang gi yi ge la brten nas dag yig rgyas pa bsdus pa gang btus nas byed kyang chog par dmigs pa lta bu'i bsam pa byung ba sogs rten 'brel sang po zhig gis rkyen gyis dbang gis byas te/ 'di la rjes 'jug rnams kyis legs par dpyad na rtogs mod kyi/ zhib par mi dpyad cing rang bstod gzhan smod kyis blo rtsing po byed pa rnams kyis rtogs mi nus so/ /gang ltar yin kyang 'jig rten grags 'dod kyis byas pa ming cing bstan 'gro la zur tsam phan nam snyam pas sbyar ro/ /brka shis/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7.5" ;
        bdo:contentLocation bdr:CLMW1NLM529_O1NLM529_001B_001 ;
        bdo:contentWidth "51" ;
        bdo:dimHeight "11.5" ;
        bdo:dimWidth "59" ;
        bdo:hasTitle bdr:TTMW1NLM529_O1NLM529_001B_001 ;
        bdo:inRootInstance bdr:MW1NLM529 ;
        bdo:paginationExtentStatement "1a-43a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM529 ;
        bdo:partTreeIndex "001B" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "ming gi rgya mtsho'i rgyab gnon dag yig chen po skad kyi rgya mtsho 'am skad rigs gsal byed nyi ma chen po/"@bo-x-ewts .

    bdr:MW1NLM529_O1NLM529_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM529_O1NLM529_002_001 ;
        bdo:authorshipStatement "grags pa rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "dpal 'khor lo chen po'i sgrub thabs dngos grub kun gyi @#/ / gter mdzod ces bya ba 'di'ang/ lam 'di la mos pa'i dam pa mang pos ring nas yang yang bskul ba la brten nas nam gru'i rigs las dngos grub brnyes pa'i grub chen dza ba ri pas mdzad pa'i sgrub thabs che chung dang/ gtor ma'i cho ga dang/ dmar chos kyi rgyal pos mdzad pa'i sgrub thabs las kyang legs cha rnams blangs shing/ khyad par du'ang gsung rab mtha' dag la mi 'jigs pa'i spobs pa mchog brnyes nas/ gdul bya mtha' dag smin grol gyi lam la 'god par mdzad pa/ chos kyi rje thams cad mkhyen pa shar brtson kha pa su ma ti kIrti shrI zhal snga nas kyi gsung gi bdud rtsi la brten nas/ 'dul ba 'dzin pa'i ming can grags pa rgyal mtshan gyis/ dgyes byed kyi lo smin drug gi zla ba'i nyer brgyad la grub pa'i gnas sngon gyi skyes bu dam pa'i bsti gnas spangs yang dgon du yi ger bkod pa'o/ /'dis kyang rgyal ba'i bstan pa rin po che sgo thams cad nas phyogs thams cad du dar zhing rgyas la yun ring du gnas pa'i rgyur gyur cig / //sarba mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM529_O1NLM529_002_001 ;
        bdo:contentWidth "47" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "52" ;
        bdo:hasTitle bdr:TTMW1NLM529_O1NLM529_002_001 ;
        bdo:inRootInstance bdr:MW1NLM529 ;
        bdo:paginationExtentStatement "1a-15a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM529 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bcom ldan 'das 'khor lo chen po'i sgrub thabs dngos grub kun gyi gter mdzod/"@bo-x-ewts .

    bdr:MW1NLM529_O1NLM529_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM529_O1NLM529_003_001 ;
        bdo:authorshipStatement "no mong hang gi skye sprul ming pa/"@bo-x-ewts ;
        bdo:colophon "ces pa 'di yangs mtha'i rigs smra'i tha shal \\u0f38no mong hang gi skye sprul ming pas bris pa'o// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM529_O1NLM529_003_001 ;
        bdo:contentWidth "49" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "52" ;
        bdo:hasTitle bdr:TTMW1NLM529_O1NLM529_003_001 ;
        bdo:inRootInstance bdr:MW1NLM529 ;
        bdo:paginationExtentStatement "1a-11b" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM529 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dge 'dun nyi shu'i sdom tshig brjed nges tsha gdung sel ba'i ga bur thigs phreng /"@bo-x-ewts .

    bdr:MW1NLM529_O1NLM529_004 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM529_O1NLM529_004_001 ;
        bdo:authorshipStatement "shA sa na d+h+wa dza/"@bo-x-ewts ;
        bdo:colophon "ces bsam gzugs snyoms 'jug gi rnam bzhag tshigs bcad du bsdebs pa mu tig 'phreng mdzes zhes bya ba 'di ni/ \\u0f38kun mkhyen sku gong 'og gnyis kyi bsam gzugs kyi rnam bzhag gnyis kyi rang lugs kyi don gyi snying po tsam tshigs bcad du bsdebs na blo gros kyi rtsa la shin tu zhan pa rnams kyi rgyun gyi shar sbyang la phan du re zhing/ rang gi dran pa yang gso ba'i phyir du \\u0f38rigs smra shA sa na d+h+wa dzas sug las su bgyis pa'o// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM529_O1NLM529_004_001 ;
        bdo:contentWidth "49" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "52" ;
        bdo:hasTitle bdr:TTMW1NLM529_O1NLM529_004_001 ;
        bdo:inRootInstance bdr:MW1NLM529 ;
        bdo:paginationExtentStatement "1a-7b" ;
        bdo:partIndex 4 ;
        bdo:partOf bdr:MW1NLM529 ;
        bdo:partTreeIndex "004" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bsam gzugs snyoms 'jug gi rnam bzhag tshigs bcad du bsdebs pa mu tig 'phreng mdzes/"@bo-x-ewts .

    bdr:MW1NLM529_O1NLM529_005 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM529_O1NLM529_005_001 ;
        bdo:authorshipStatement "grags pa bshad sgrub/"@bo-x-ewts ;
        bdo:colophon "ces pa 'di dag kyang shAkya'i dge slong grags pa bshad sgrub ces bya bas co ne dgon nas dad pas bsgrigs pa'o/ /'dis kyang bstan pa dang sems can la phan thogs par gyur cig /mang+ga laM// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM529_O1NLM529_005_001 ;
        bdo:contentWidth "47" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "56" ;
        bdo:hasTitle bdr:TTMW1NLM529_O1NLM529_005_001 ;
        bdo:inRootInstance bdr:MW1NLM529 ;
        bdo:paginationExtentStatement "1a-14a" ;
        bdo:partIndex 5 ;
        bdo:partOf bdr:MW1NLM529 ;
        bdo:partTreeIndex "005" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "las rnam par 'byed pa'i mdo sogs dang tshe mtha'i mdo'i don bsdus nas bkod pa/"@bo-x-ewts .

    bdr:O1NLM529 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM529 .

    bdr:TTMW1NLM529_O1NLM529_001B_001 a bdo:TitlePageTitle ;
        rdfs:label "ming gi rgya mtsho'i rgyab gnon dag yig chen po skad kyi rgya mtsho 'am skad rigs gsal byed nyi ma chen po/"@bo-x-ewts .

    bdr:TTMW1NLM529_O1NLM529_002_001 a bdo:TitlePageTitle ;
        rdfs:label "bcom ldan 'das 'khor lo chen po'i sgrub thabs dngos grub kun gyi gter mdzod/"@bo-x-ewts .

    bdr:TTMW1NLM529_O1NLM529_003_001 a bdo:TitlePageTitle ;
        rdfs:label "dge 'dun nyi shu'i sdom tshig brjed nges tsha gdung sel ba'i ga bur thigs phreng*/"@bo-x-ewts .

    bdr:TTMW1NLM529_O1NLM529_004_001 a bdo:TitlePageTitle ;
        rdfs:label "bsam gzugs snyoms 'jug gi rnam bzhag tshigs bcad du bsdebs pa mu tig 'phreng mdzes/"@bo-x-ewts .

    bdr:TTMW1NLM529_O1NLM529_005_001 a bdo:TitlePageTitle ;
        rdfs:label "las rnam par 'byed pa'i mdo sogs dang tshe mtha'i mdo'i don bsdus nas bkod pa/"@bo-x-ewts .

    bdr:MW1NLM529 bdo:hasPart bdr:MW1NLM529_O1NLM529_001B,
            bdr:MW1NLM529_O1NLM529_002,
            bdr:MW1NLM529_O1NLM529_003,
            bdr:MW1NLM529_O1NLM529_004,
            bdr:MW1NLM529_O1NLM529_005 .
}

