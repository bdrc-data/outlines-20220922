@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7 {
    bda:O1NLM7 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM7 ;
        adm:gitPath "b4/O1NLM7.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM7 ;
        adm:logEntry bda:LG0NLMOO1NLM7_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM7_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM7_O1NLM7_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 38 ;
        bdo:contentLocationInstance bdr:W1NLM7 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM7_O1NLM7_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 42 ;
        bdo:contentLocationInstance bdr:W1NLM7 ;
        bdo:contentLocationPage 39 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM7_O1NLM7_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 334 ;
        bdo:contentLocationInstance bdr:W1NLM7 ;
        bdo:contentLocationPage 43 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM7_O1NLM7_001_001 a bdr:NLMId ;
        rdf:value "M0057901-001" .

    bdr:IDMW1NLM7_O1NLM7_002_001 a bdr:NLMId ;
        rdf:value "M0057901-002" .

    bdr:IDMW1NLM7_O1NLM7_003_001 a bdr:NLMId ;
        rdf:value "M0057901-003" .

    bdr:MW1NLM7_O1NLM7_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM7_O1NLM7_001_001 ;
        bdo:authorshipStatement "lcang skya rol pa'i rdo rje/"@bo-x-ewts ;
        bdo:colophon "ces dpal 'khor lo sdom pa lo dril gnyis kyi bsnyen pa byed tshul gsal bar bshad pa mkha' 'gro'i gsang mdzod bdud rtsi'i bum bzang zhes bya ba 'di yang/ lam rim pa gnyis kyi gnad la sgrub pa snying por byed pa'i yi dam brtan po dang ldan pa har chen dka' bcu brtson 'grus kyis nan tan chen pos bskul ba don yod par bya ba'i phyir tu rigs brgya'i khyab bdag rdo rje 'chang khri chen \\u0f38ngag dbang mchog ldan pa'i zhal snga nas dang/ /rje grub pa'i dbang phyug blo bzang chos 'dzin pa'i zhal snga nas sogs dam pa du ma'i bka' drin las chos 'di'i dbang dang rgyud bshad man ngag sogs yongs su rdzogs par thob pa dge ldan rigs sngags 'chang ba mang thos \\u0f38lcang skya rol pa'i rdo rjes rgyal khab chen po'i byang phyogs dkar @#/ /phyogs la dga' ba'i klu chen gnas pa'i chu tshan khar sbyar ba'i yi ge pa ni lam 'di la mos shing lhag bsam rnam par dag pa'i dpyod ldan dge legs nam mkhas pa gyis pa 'dis kyang 'khor lo sdom pa'i lam bzang phyogs thams cad du rgyas par gyur cig /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM7_O1NLM7_001_001 ;
        bdo:contentWidth "35" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "42" ;
        bdo:hasTitle bdr:TTMW1NLM7_O1NLM7_001_001 ;
        bdo:inRootInstance bdr:MW1NLM7 ;
        bdo:paginationExtentStatement "1a-19a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM7 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dpal 'khor lo sdom pa lo dril gnyis kyi bsnyen pa byed tshul gsal bar bshad pa mkha' 'gro'i gsang mdzod bdud rtsi'i bum bzang /"@bo-x-ewts .

    bdr:MW1NLM7_O1NLM7_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM7_O1NLM7_002_001 ;
        bdo:authorshipStatement "blo bzang dpal ldan ye shes/"@bo-x-ewts ;
        bdo:colophon "zhes pa 'di ni paN chen nA ro pa'i man ngag blo bzang rgyal ba gnyis pa'i man ngag dben sa snyan brgyud bzhin bris pa'o/ /zhes dpal 'khor lo sdom pa'i rnal 'byor dang 'brel ba'i rdo rje'i bzlas pa dang bum pa can 'di ni spi lig thu chos rje nges legs lhun grub kyis bskul ba ltar/ lam 'di'i r+hal 'byor ba \\u0f38shAkya'i dge slong blo bzang dpal ldan ye shes kyis sbyar ba'o/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM7_O1NLM7_002_001 ;
        bdo:contentWidth "35" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "42" ;
        bdo:inRootInstance bdr:MW1NLM7 ;
        bdo:incipit "bla ma dang dpal 'khor lo sdom pa'i lha tshogs la phyag 'tshal lo/"@bo-x-ewts ;
        bdo:paginationExtentStatement "1a-2b" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM7 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 .

    bdr:MW1NLM7_O1NLM7_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM7_O1NLM7_003_001 ;
        bdo:authorshipStatement "blo bzang phun tshogs/"@bo-x-ewts ;
        bdo:colophon "ces bla ma mchod pa'i 'khrid yig snyan brgyud kyi man ngag zhal shes kyis brgyan pa 'di ni/ mkhas btsun bzang gsum dang mkhyen brtse nus gsum la sogs pa'i yon tan mtha' dag la mnga' brnyes shing/ rigs dang dkyil 'khor rgya mtsho'i khyon la dbang bsgyur ba'i paN+Di ta mer kan mkhan po dka' chen blo bzang phun tshogs dpal bzang po'i zhal snga nas/ me rta lor dge 'dun 'dus pa rgya mtshor bla mchod myong khrid du stsal skabs dang blos gang lcogs zin bris su btab pa 'di yang/ rje rang nyid kyis kyang 'di lta bu zin thor bkod nus na legs snyam tu gsungs pa dang/ chos grogs 'ga' zhig gis kyang shog bu sogs kyi rgyu sbyar te ci nas kyang 'bri dgos zhes nan cher 'byung ba dang/ rang nyid kyang bla ma dam pa'i gsung 'di lta bu yi ger bkod de sngas mgor bzhag na yang skal ba bzang snyam pa dang/ 'di 'dra'i bya ba byas pa te yang bdag ltas bus dal 'byor don ldan du byas pa'i mchog yin snyam du bsams te/ mgon de'i slob ma'i tha shal bskal bzang min can zhig gis bris te/ bla ma rang nyid kyi gzigs lam tu phebs te dag par bgyis pa'o// //'dis kyang md0 sngags yongs su rdzogs pa'i bstan pa rin po che 'phel rgyas su 'gro ba dang/ sems can thams cad bshes gnyen dam pa'i mgon gyis rjes su 'dzin pa'i rgyur gyur cig / //sarba mang+ga laM/ /@#/ oM swa sti/ rigs kun khyab bdag rdo rje 'chang dbang gi /thugs kyi bcud bstus bla mchod khrid yig 'di/ par du bzhengs pa'i dge ba la brten nas/ ma gur spyi dang khyad par rgyu sbyor ba/ /'brel thogs gnyen 'brel ltos bcas thams cad kyis/ /tshe rams kun tu dge ba'i bshes gnyen rnams/ /rjes bzung mdo sngags nyams len mthar phyin nas/ /stobs bcu mnga' ba'i go 'phang myur thob @#/ /shog /ces pa 'di ni me rta lor bla khrid stsal skabs tho yon dka' bcu tshul khrims dang gnyer ba dge slong phun tshogs rdo rje sogs kyis rgyu rkyen bsgrubs te par du bzhengs pa'i smon tshig 'di lta bu zhig dgos zhes bskul ba ltar/ \\u0f38paN+Di ta med kan mkhan po dkan po dka' chen blo bzang phun tshogs kyis sbyar ba'o// // bkra shis pa dang dge legs 'phel par gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM7_O1NLM7_003_001 ;
        bdo:contentWidth "33" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "41" ;
        bdo:hasTitle bdr:TTMW1NLM7_O1NLM7_003_001 ;
        bdo:inRootInstance bdr:MW1NLM7 ;
        bdo:paginationExtentStatement "1a-146a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM7 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bla ma mchod pa'i nyams khrid zab mo stsal skabs kyi zin bris zhal shes man ngag gis brgyan pa/"@bo-x-ewts .

    bdr:O1NLM7 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM7 .

    bdr:TTMW1NLM7_O1NLM7_001_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal 'khor lo sdom pa lo dril gnyis kyi bsnyen pa byed tshul gsal bar bshad pa mkha' 'gro'i gsang mdzod bdud rtsi'i bum bzang*/"@bo-x-ewts .

    bdr:TTMW1NLM7_O1NLM7_003_001 a bdo:TitlePageTitle ;
        rdfs:label "bla ma mchod pa'i nyams khrid zab mo stsal skabs kyi zin bris zhal shes man ngag gis brgyan pa/"@bo-x-ewts .

    bdr:MW1NLM7 bdo:hasPart bdr:MW1NLM7_O1NLM7_001,
            bdr:MW1NLM7_O1NLM7_002,
            bdr:MW1NLM7_O1NLM7_003 .
}

