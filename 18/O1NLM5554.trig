@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM5554 {
    bda:LG0NLMOO1NLM5554_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0O1NLM5554_XNOOPRYC52ED
        a                   adm:UpdateData ;
        adm:logDate         "2024-08-08T11:42:45.689372Z"^^xsd:dateTime ;
        adm:logMessage      "reviewed and corrected"@bo ;
        adm:logWho          bdu:U0ES1788534075 .
    
    bda:O1NLM5554  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM5554 ;
        adm:gitPath         "18/O1NLM5554.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM5554 ;
        adm:logEntry        bda:LG0NLMOO1NLM5554_IDC001 , bda:LG0O1NLM5554_XNOOPRYC52ED ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM5554_O1NLM5554_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  196 ;
        bdo:contentLocationEndVolume  1 ;
        bdo:contentLocationInstance  bdr:W1NLM5554 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM5554_O1NLM5554_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  204 ;
        bdo:contentLocationEndVolume  1 ;
        bdo:contentLocationInstance  bdr:W1NLM5554 ;
        bdo:contentLocationPage  197 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM5554_O1NLM5554_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  220 ;
        bdo:contentLocationEndVolume  1 ;
        bdo:contentLocationInstance  bdr:W1NLM5554 ;
        bdo:contentLocationPage  205 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM5554_O1NLM5554_004_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  244 ;
        bdo:contentLocationEndVolume  1 ;
        bdo:contentLocationInstance  bdr:W1NLM5554 ;
        bdo:contentLocationPage  221 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM5554_O1NLM5554_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0063472-001" .
    
    bdr:IDMW1NLM5554_O1NLM5554_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0063472-002" .
    
    bdr:IDMW1NLM5554_O1NLM5554_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0063472-003" .
    
    bdr:IDMW1NLM5554_O1NLM5554_004_001
        a                   bdr:NLMId ;
        rdf:value           "M0063472-004" .
    
    bdr:IDMW1NLM5554_O1NLM5554_005_001
        a                   bdr:NLMId ;
        rdf:value           "M0063472-005" .
    
    bdr:IDMW1NLM5554_O1NLM5554_006_001
        a                   bdr:NLMId ;
        rdf:value           "M0063472-006" .
    
    bdr:MW1NLM5554  bdo:hasPart  bdr:MW1NLM5554_O1NLM5554_001 , bdr:MW1NLM5554_O1NLM5554_002 , bdr:MW1NLM5554_O1NLM5554_003 , bdr:MW1NLM5554_O1NLM5554_004 , bdr:MW1NLM5554_O1NLM5554_005 , bdr:MW1NLM5554_O1NLM5554_006 .
    
    bdr:MW1NLM5554_O1NLM5554_001
        a                   bdo:Instance ;
        skos:prefLabel      "bcom ldan 'das dpal 'khor lo sdom pa lha lnga'i dkyil 'khor du skal ldan gyi slob ma rnams bcug nas smin byed kyi dbang bskur tshul gyi bshad sbyor bde chen bdud rtsi'i rgya mtsho/"@bo-x-ewts ;
        bf:identifiedBy     bdr:IDMW1NLM5554_O1NLM5554_001_001 ;
        bdo:authorshipStatement  "dam tshig rdo rje/"@bo-x-ewts ;
        bdo:colophon        "ces byin rlabs kyi gter chen po bcom ldan 'dab dpal 'khor lo sdom pa lha lnga'i dkyil 'khor du skal ldan gyi slob ma rnams bcug nas smin byed kyi dbang bskur tshul gyi bshad sbyor bde chen bdud rtsi'i rgya mtsho zhes bya ba 'di ni/_don gnyer can gyi slob bu 'ga' zhig gis/_lugs 'di'i bdag 'jug gi dmigs rim gsal bar thon pa'i dbang TI ka zhig dgos tshul mol bar brten/_deng sang lha 'di la yi dam du byed pa mang zhing /_dkyil 'khor sgrub mchod bdag 'jug sogs kyi lag len kyang dar so che bar snang bas/_tshul 'di lta bu zhig byung na bdag 'jug gi dmigs rim sogs kyang zhal la 'thon zhing/_slad nas dbang rgyun spel mkhan dag la'ang phan che bar bsams nas/_rje bdag nyid chen po'i gsung dkyil chog rin chen bang mdzod dang mkhas grub rje'i bde chen rol mtsho gnyis kyi dgongs don rgyal dbang thams cad mkhyen pa bsod nams rgya mtsho'i dkyil @#/_/chog nyid gzhir bzung/_mtshams sbyor gyi bshad pa phal cher gong smos kyi dbang bshad rnams las ci rigs par btus shing/_rigs dang dkyil 'khor kun gyi phyab bdag rdo rje 'chang dngos kun mkhyen dkon mchog bstan pa'i sgron me dpal bzang po la sogs pa'i bla ma dam pa rnams kyi phyag bzhes gsung rgyun gyis nye bar brgyan nas/_rgyas bsdus 'tsham zhing 'khyer bde ba'i tshul du/_mkhas shing grub pa'i bla ma dam pa du ma'i bka' drin las/_'di'i skor gyi dbang dang gdams pa mang du thob pa'i rgyal khams pa/[ \\u0f38]dam tshig rdo rjes dpal ldan brag ri'i dben gnas su sbyar ba'i yi ge pa ni/_dpyod ldan las kaya rdo rje chos mdzad dge slong bstan 'dzin bzang po dang/_mchod las pa dge tshul blo bzang chos ldan zung gis bgyis pa 'dis kyang rgyal ba'i bstan pa'i snying po khams gsum chos kyi rgyal po tsong kha pa chen po'i mdo sngags bshad sgrub kyi ring lugs dri ma med pa 'di ag dus kyi mthar yang mi nub cing/_phyogs dus kun tu dar zhing rgyas la yun ring du gnas par gyur cig /mahA su kha sid+d+hi m+me pra yatsh+ts+han+du//_//"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM5554_O1NLM5554_001_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "10" ;
        bdo:dimWidth        "54.5" ;
        bdo:hasTitle        bdr:TTMW1NLM5554_O1NLM5554_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM5554 ;
        bdo:paginationExtentStatement  "1a-98a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM5554 ;
        bdo:partTreeIndex   "01" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 .
    
    bdr:MW1NLM5554_O1NLM5554_002
        a                   bdo:Instance ;
        skos:prefLabel      "gdung shugs drag pos smre sngags gsol 'debs bya tshul/"@bo-x-ewts ;
        bf:identifiedBy     bdr:IDMW1NLM5554_O1NLM5554_002_001 ;
        bdo:authorshipStatement  "pa Tu sid+d+hi/"@bo-x-ewts ;
        bdo:colophon        "zhes pa 'di ni dpyod ldan dge slong mgon po bkra shis dang spar dpon tshe dpag gnyis kyi 'dod mos ltar \\u0f38pa Tu sid+d+his bris pa skal mnyam la phan thogs par gyur cig /mang+ga la/_/"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM5554_O1NLM5554_002_001 ;
        bdo:contentWidth    "46" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "48.5" ;
        bdo:hasTitle        bdr:TTMW1NLM5554_O1NLM5554_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM5554 ;
        bdo:paginationExtentStatement  "1a-4a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM5554 ;
        bdo:partTreeIndex   "02" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 .
    
    bdr:MW1NLM5554_O1NLM5554_003
        a                   bdo:Instance ;
        skos:prefLabel      "mi 'khrugs pa'i cho ga"@bo-x-ewts ;
        bf:identifiedBy     bdr:IDMW1NLM5554_O1NLM5554_003_001 ;
        bdo:authorshipStatement  "blo bzang rta dbyangs/"@bo-x-ewts ;
        bdo:colophon        "ces rgyal ba mi 'khrugs pa la mdo chog gi lugs bzhin mchod cing gsol ba btab pa'i rim pa mngon dga' zhing du bgrod pa'i 'phrul gyi gru gzings zhes bya ba 'di ni/_chos mthun grogs bshes mkhyen ldan dka' bcu bde spyid dpal gyis gzhan phan gyi lhag bsam dkar bas lha dbang d+r+g+ya sgor gyi nyen dang bcas te bskul ma byung ba bzhin/_rang nyid kyang mos 'dun gyis mtshams sbyar te/[ \\u0f38]shAkya'i btsun gzugs blo bzang rta dbyangs ming pas hal ha dbus ru'i klung chen tho'u la'i nub 'gram gyi char gsar bsgrigs byas so/_/'dis kyang 'brel thogs skye 'gro rnams dang mo dang dga'i zhing du mi bskyod rgyal ba'i mdun sar phan chun gshes bzhin du mjal ba'i rgyu byed par gyur cig /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM5554_O1NLM5554_003_001 ;
        bdo:contentWidth    "47" ;
        bdo:dimHeight       "9.5" ;
        bdo:dimWidth        "55" ;
        bdo:hasTitle        bdr:TTMW1NLM5554_O1NLM5554_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM5554 ;
        bdo:paginationExtentStatement  "1a-8a" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM5554 ;
        bdo:partTreeIndex   "03" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 .
    
    bdr:MW1NLM5554_O1NLM5554_004
        a                   bdo:Instance ;
        skos:prefLabel      "dpal 'khor lo sdom pa nag po pa'i sa chog sta gon 'dod pa 'jo ba'i ngag 'don 'khrig chags su bkod pa/"@bo-x-ewts ;
        bf:identifiedBy     bdr:IDMW1NLM5554_O1NLM5554_004_001 ;
        bdo:colophon        "zhes 'dod don la gsol ba btab/_gzhan rnams zhi ba'i skabs dang 'dra 'di'i thams kyis khyad par/_dbyibs zla gam/_rgyar khru gang/_mu ngan rgya'i bcu gnyis cha/_'khyer drug cha/_de la pad+ma'am mda'i 'phreng bas bskor/_dbus kyis mtshan ma pad ma dmar po bya bar gsungs/_yaM shing gats+had sogs dbang spyi 'dre ltar ro/_/de ltar bde mchog gis sgo nas dbang gi sbyin sregs bya tshul 'di yang grub chen dA ri ka pas mdzad pa'i sbyin sregs kyi dgongs pa ltar nyer 'kho kha gsal du gar dbang pas bris pa'o/_/dbang gis 'gugs pa yin nad migs gsal gyi khyad par mang du yid gsungs pas gzhan las shes par b+b+yos shig /dbang gi spel tshig ni/_rig pa 'dzin pa gar gyi dbang po bdag cag dpon slob rnams kyi dbang du chos grub pa'i mthun rkyen du gyur ba'i mi dang mi min zas nor longs spyod mchog thun dngos grub thams ad wa shaM ku ru hoH/"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM5554_O1NLM5554_004_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "10" ;
        bdo:dimWidth        "60" ;
        bdo:hasTitle        bdr:TTMW1NLM5554_O1NLM5554_004_001 ;
        bdo:inRootInstance  bdr:MW1NLM5554 ;
        bdo:paginationExtentStatement  "1a-11a" ;
        bdo:partIndex       4 ;
        bdo:partOf          bdr:MW1NLM5554 ;
        bdo:partTreeIndex   "04" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 .
    
    bdr:MW1NLM5554_O1NLM5554_005
        a                   bdo:Instance ;
        skos:prefLabel      "dpal 'khor lo sdom pa nag po pa'i sa chog sta gon 'dod pa 'jo ba'i ngag 'don 'khrig chags su bkod pa/"@bo-x-ewts ;
        bf:identifiedBy     bdr:IDMW1NLM5554_O1NLM5554_005_001 ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "10" ;
        bdo:dimWidth        "60" ;
        bdo:hasTitle        bdr:TTMW1NLM5554_O1NLM5554_005_001 ;
        bdo:inRootInstance  bdr:MW1NLM5554 ;
        bdo:note            bdr:NTMW1NLM5554_O1NLM5554_005CL ;
        bdo:paginationExtentStatement  "1a-8b inc [*missing page from 9a*]" ;
        bdo:partIndex       5 ;
        bdo:partOf          bdr:MW1NLM5554 ;
        bdo:partTreeIndex   "05" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 .
    
    bdr:MW1NLM5554_O1NLM5554_006
        a                   bdo:Instance ;
        skos:prefLabel      "bde mchog gis sgo nas zhi ba'i sbyin sregs bya tshul sogs ni/"@bo-x-ewts ;
        bf:identifiedBy     bdr:IDMW1NLM5554_O1NLM5554_006_001 ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentWidth    "47.5" ;
        bdo:dimHeight       "10" ;
        bdo:dimWidth        "60" ;
        bdo:hasTitle        bdr:TTMW1NLM5554_O1NLM5554_006_001 ;
        bdo:inRootInstance  bdr:MW1NLM5554 ;
        bdo:note            bdr:NTMW1NLM5554_O1NLM5554_006CL ;
        bdo:paginationExtentStatement  "9a-11a" ;
        bdo:partIndex       6 ;
        bdo:partOf          bdr:MW1NLM5554 ;
        bdo:partTreeIndex   "06" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 .
    
    bdr:NTMW1NLM5554_O1NLM5554_005CL
        a                   bdo:Note ;
        bdo:noteText        "Title present in the catalog but not digitized"@en .
    
    bdr:NTMW1NLM5554_O1NLM5554_006CL
        a                   bdo:Note ;
        bdo:noteText        "Title present in the catalog but not digitized"@en .
    
    bdr:O1NLM5554  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM5554 .
    
    bdr:TTMW1NLM5554_O1NLM5554_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bcom ldan 'das dpal 'khor lo sdom pa lha lnga'i dkyil 'khor du skal ldan gyi slob ma rnams bcug nas smin byed kyi dbang bskur tshul gyi bshad sbyor bde chen bdud rtsi'i rgya mtsho/"@bo-x-ewts .
    
    bdr:TTMW1NLM5554_O1NLM5554_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gdung shugs drag pos smre sngags gsol 'debs bya tshul/"@bo-x-ewts .
    
    bdr:TTMW1NLM5554_O1NLM5554_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "mi 'khrugs pa'i cho ga"@bo-x-ewts .
    
    bdr:TTMW1NLM5554_O1NLM5554_004_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dpal 'khor lo sdom pa nag po pa'i sa chog sta gon 'dod pa 'jo ba'i ngag 'don 'khrig chags su bkod pa/"@bo-x-ewts .
    
    bdr:TTMW1NLM5554_O1NLM5554_005_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dpal 'khor lo sdom pa nag po pa'i sa chog sta gon 'dod pa 'jo ba'i ngag 'don 'khrig chags su bkod pa/"@bo-x-ewts .
    
    bdr:TTMW1NLM5554_O1NLM5554_006_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bde mchog gis sgo nas zhi ba'i sbyin sregs bya tshul sogs ni/"@bo-x-ewts .
}
