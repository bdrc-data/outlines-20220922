@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7509 {
    bda:LG0NLMOO1NLM7509_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7509  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7509 ;
        adm:gitPath         "f3/O1NLM7509.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7509 ;
        adm:logEntry        bda:LG0NLMOO1NLM7509_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7509_O1NLM7509_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  237 ;
        bdo:contentLocationInstance  bdr:W1NLM7509 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7509_O1NLM7509_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  251 ;
        bdo:contentLocationInstance  bdr:W1NLM7509 ;
        bdo:contentLocationPage  238 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7509_O1NLM7509_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  259 ;
        bdo:contentLocationInstance  bdr:W1NLM7509 ;
        bdo:contentLocationPage  252 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7509_O1NLM7509_004_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  285 ;
        bdo:contentLocationInstance  bdr:W1NLM7509 ;
        bdo:contentLocationPage  260 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7509_O1NLM7509_005_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  306 ;
        bdo:contentLocationInstance  bdr:W1NLM7509 ;
        bdo:contentLocationPage  286 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7509_O1NLM7509_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0065427-001" .
    
    bdr:IDMW1NLM7509_O1NLM7509_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0065427-002" .
    
    bdr:IDMW1NLM7509_O1NLM7509_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0065427-003" .
    
    bdr:IDMW1NLM7509_O1NLM7509_004_001
        a                   bdr:NLMId ;
        rdf:value           "M0065427-004" .
    
    bdr:IDMW1NLM7509_O1NLM7509_005_001
        a                   bdr:NLMId ;
        rdf:value           "M0065427-005" .
    
    bdr:MW1NLM7509  bdo:hasPart  bdr:MW1NLM7509_O1NLM7509_001 , bdr:MW1NLM7509_O1NLM7509_002 , bdr:MW1NLM7509_O1NLM7509_003 , bdr:MW1NLM7509_O1NLM7509_004 , bdr:MW1NLM7509_O1NLM7509_005 .
    
    bdr:MW1NLM7509_O1NLM7509_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7509_O1NLM7509_001_001 ;
        bdo:authorshipStatement  "rje blo bzang grags pa/"@bo-x-ewts ;
        bdo:colophon        "ces mgon po klu sgrub kyis ngo bo nyid med pa'i tshul dang/ bdag nyid chen po thogs med kyis rnam par rig pa tsam gyi tshul gyi shing rta chen po'i srol gnyis phye nas/ gsung rab kyi drang ba dang nges pa'i don rnam par phye ba gsal bar byed pa @#/ /legs par bshad pa'i snying po zhes bya ba 'di ni/ shAkya'i dge slong mang du thos pa rigs pa smra ba shar tsong kha pa blo bzang grags pa'i dpal gyis sbyar ba'i yi ge pa ni dge sbyong sdom brtson bsod nams blo gros so// //mang+ga laM/ /swa sti/ rgyal bstan pad tshal phan bde'i dri bsung can/ bshad sgrub 'dab rgyas rnam grol sbrang rtsi'i bcud/ /'gro kun spyod phyir sku 'bum byams pa gling/ /chos sde che nas chos sbyin char 'di spel/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7509_O1NLM7509_001_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "53.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7509_O1NLM7509_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7509 ;
        bdo:paginationExtentStatement  "1a-124a inc [*missing page from 20a-25b*]" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7509 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "drang ba dang nges pa'i don rnam par phye ba'i bstan bcos legs bshad snying po/"@bo-x-ewts .
    
    bdr:MW1NLM7509_O1NLM7509_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7509_O1NLM7509_002_001 ;
        bdo:authorshipStatement  "rat+na d+h+wa dza/"@bo-x-ewts ;
        bdo:colophon        "zhes rdo rje 'jigs byed dpa' bo gcig pa'i bdag 'jug gi chog bsgrigs mdor bsdus pa 'di ni bsam sgang ba blo bzang ngag gi dpa' bo dang/ rgyal ba bskal bzang rgya mtsho/ lcang skya rol pa'i rdo rje/ kun mkhyen sku phreng gnyis pa'i sras kyi thu bo rnam gnyis sogs kyi gsung la gzhi byas/ yab sras gong ma rnams kyi gsung rgyun gyis brgyan te chag med byed pa rnams la mkho ba'i bsam pas \\u0f38rat+na d+h+wa dzas bris pa'o/ /mdo sngags chos kyi skyed tshal dga' ldan chos 'khor gling nas par du bkod//"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM7509_O1NLM7509_002_001 ;
        bdo:contentWidth    "37.5" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "44.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7509_O1NLM7509_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7509 ;
        bdo:paginationExtentStatement  "1a-7b" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7509 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "rdo rje 'jigs byed dpa' bo gcig pa'i bdag 'jug gi chog bsgrigs mdor bsdus/"@bo-x-ewts .
    
    bdr:MW1NLM7509_O1NLM7509_003
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7509_O1NLM7509_003_001 ;
        bdo:authorshipStatement  "bsod nams rgya mtsho/"@bo-x-ewts ;
        bdo:colophon        "'di yang bsod nams rgya mtshos bgyis pa'o// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM7509_O1NLM7509_003_001 ;
        bdo:contentWidth    "37.5" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "44.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7509_O1NLM7509_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM7509 ;
        bdo:paginationExtentStatement  "1a-4b" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM7509 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "bsag sbyang gi gnad bsdus pa'i yig chung gi 'dod bsgrigs/"@bo-x-ewts .
    
    bdr:MW1NLM7509_O1NLM7509_004
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7509_O1NLM7509_004_001 ;
        bdo:authorshipStatement  "blo bzang bskal bzang rgya mtsho/"@bo-x-ewts ;
        bdo:colophon        "de ltar thugs rje chen po rgyal ba rgya mtsho'i sgrub thabs dngos grub gter mdzod ces bya ba 'di ni bkra shis sgoms pa'i gra rigs rab 'byams pa blo bzang phun tshogs dang/ zab yangs 'khyil ba'i gra rigs dge slong bskal bzang bstan dar gnyis nas dgos zhes bskul ngor nyams len byed 'dod can gzan la'ang phan de bcas shAkya'i dge slong blo bzang bskal bzang rgya mtshos gru 'dzin gnyis pa'i gzhal med khang gi gzims chung nyi 'od 'khyil bar sbyar ba'o//"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7509_O1NLM7509_004_001 ;
        bdo:contentWidth    "45.5" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "53" ;
        bdo:hasTitle        bdr:TTMW1NLM7509_O1NLM7509_004_001 ;
        bdo:inRootInstance  bdr:MW1NLM7509 ;
        bdo:paginationExtentStatement  "1a-13b" ;
        bdo:partIndex       4 ;
        bdo:partOf          bdr:MW1NLM7509 ;
        bdo:partTreeIndex   "004" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "thugs rje chen po rgya mtsho'i sgrub thabs dngos grub gter mdzod/"@bo-x-ewts .
    
    bdr:MW1NLM7509_O1NLM7509_005
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7509_O1NLM7509_005_001 ;
        bdo:authorshipStatement  "blo bzang bkra shis/"@bo-x-ewts ;
        bdo:colophon        "ces spyi dang bye brag gi mtha' dpyod ngo mtshar dga' ston zhes bya ba 'di ni/ dka' bcu blo bzang bkra shis kyis bris pa'o/ /'dis kyang bstan pa rin po che phyogs kun tu rgyas par byed nus par gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7509_O1NLM7509_005_001 ;
        bdo:contentWidth    "43.5" ;
        bdo:dimHeight       "9.5" ;
        bdo:dimWidth        "52" ;
        bdo:hasTitle        bdr:TTMW1NLM7509_O1NLM7509_005_001 ;
        bdo:inRootInstance  bdr:MW1NLM7509 ;
        bdo:paginationExtentStatement  "1a-14a inc [*missing page from 11a-13b*]" ;
        bdo:partIndex       5 ;
        bdo:partOf          bdr:MW1NLM7509 ;
        bdo:partTreeIndex   "005" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "spyi dang bye brag gi mtha' dpyod ngo mtshar dga' ston/"@bo-x-ewts .
    
    bdr:O1NLM7509  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7509 .
    
    bdr:TTMW1NLM7509_O1NLM7509_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "drang ba dang nges pa'i don rnam par phye ba'i bstan bcos legs bshad snying po/"@bo-x-ewts .
    
    bdr:TTMW1NLM7509_O1NLM7509_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rdo rje 'jigs byed dpa' bo gcig pa'i bdag 'jug gi chog bsgrigs mdor bsdus/"@bo-x-ewts .
    
    bdr:TTMW1NLM7509_O1NLM7509_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bsag sbyang gi gnad bsdus pa'i yig chung gi 'dod bsgrigs/"@bo-x-ewts .
    
    bdr:TTMW1NLM7509_O1NLM7509_004_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "thugs rje chen po rgya mtsho'i sgrub thabs dngos grub gter mdzod/"@bo-x-ewts .
    
    bdr:TTMW1NLM7509_O1NLM7509_005_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "spyi dang bye brag gi mtha' dpyod ngo mtshar dga' ston/"@bo-x-ewts .
}
