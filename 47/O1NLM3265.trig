@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM3265 {
    bda:O1NLM3265 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM3265 ;
        adm:gitPath "47/O1NLM3265.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM3265 ;
        adm:logEntry bda:LG0NLMOO1NLM3265_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM3265_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM3265_O1NLM3265_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 106 ;
        bdo:contentLocationInstance bdr:W1NLM3265 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM3265_O1NLM3265_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 192 ;
        bdo:contentLocationInstance bdr:W1NLM3265 ;
        bdo:contentLocationPage 107 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM3265_O1NLM3265_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 242 ;
        bdo:contentLocationInstance bdr:W1NLM3265 ;
        bdo:contentLocationPage 193 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM3265_O1NLM3265_001_001 a bdr:NLMId ;
        rdf:value "M0061183-001" .

    bdr:IDMW1NLM3265_O1NLM3265_002_001 a bdr:NLMId ;
        rdf:value "M0061183-002" .

    bdr:IDMW1NLM3265_O1NLM3265_003_001 a bdr:NLMId ;
        rdf:value "M0061183-003" .

    bdr:IDMW1NLM3265_O1NLM3265_004_001 a bdr:NLMId ;
        rdf:value "M0061183-004" .

    bdr:MW1NLM3265_O1NLM3265_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM3265_O1NLM3265_001_001 ;
        bdo:authorshipStatement "dge 'dun grub/"@bo-x-ewts ;
        bdo:colophon "dbu ma la 'jug pa'i bstan bcos kyi dgongs pa rab tu gsal ba'i me long 'di ni/ chos kyi rgyal po shar btsong kha pa chen po'i zhabs rdul spyi bos len pa/ dbu ma chen po'i rnal 'byor pa dge 'dun grub dpal bzang pos/ thun mong gi lo tha skar gyi zla ba'i yar ngo'i tshe ? la/ dpal snar thang gi btsug lag khang chen por sbyar ba'o/ /'dis kyang sangs rgyas kyi bstan pa rin po che sgo thams cad nas phyogs thams cad du dar zhing rgyas la yun ring du gnas par gyur cig /dge'o/ /mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM3265_O1NLM3265_001_001 ;
        bdo:contentWidth "47" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "53" ;
        bdo:hasTitle bdr:TTMW1NLM3265_O1NLM3265_001_001 ;
        bdo:inRootInstance bdr:MW1NLM3265 ;
        bdo:paginationExtentStatement "1a-53a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM3265 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dbu ma la 'jug pa'i bstan bcos kyi dgongs pa rab tu gsal ba'i me long /"@bo-x-ewts .

    bdr:MW1NLM3265_O1NLM3265_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM3265_O1NLM3265_002_001 ;
        bdo:authorshipStatement "shes rab rgya mtsho/"@bo-x-ewts ;
        bdo:colophon "ces dpal gsang ba 'dus pa'i rdzogs rim rim lnga'i dmar khrid kyi brjed byang mi bskyod zhal gyi bdud rtsi zhes bya ba 'di yang/ rim gnyis kyi gdams pa'i bang mdzod 'dzin pa zla bral smra ba'i dbang phyug \\u0f38rje btsun thams cad mkhyen pa dkon mchog rgyal mtshan dpal bzang po'i bka' drin las 'di'i nyams khrid thob pa'i skabs brjed tho cung zad bris pa la/ phyis su bla ma rdo rje 'chang yab sras rnams kyis de bzhin yi ger bkod chog pa'i bka'i gnang ba spyi bor stsal ba'i byin rlabs kho na la brten nas/ dge slong shes rab rgya mtshos zin tho snga phyi rnams phyogs bsdebs su byas pa la rang gi blo skyon gyis spags pa dang/ zhal shes rnams gsal bar bkod pa sogs kyis lam la srib par mi 'gyur zhing/ tshe rabs thams cad du bla ma lhag pa'i lhas brtse bas rjes su bzung ste/ dpa' bo dang rnal 'byor ma'i dbang phyug bye ba brgya phrag gang nas gshegs pa'i lam bzang 'di dang skad cig tsam yang mi 'bral zhing/ bstan pa'i snying po dri ma med pa bshad sgrub zung gis sgo nas phyogs dus kun tu rgyas par byed nus par gyur cig / //oM swa sti/ chos srid 'byor ba'i mdzod dang dge bcu ldan/ /dga' ldan rab rgyas zhes ba'i rgyud grwa cher/ /kun mkhyen 'jam dbyangs bzhad pa yab sras kyi/ /thugs rje chos sbyin 'dzad med rgyun 'di spel// //mang+ga laM// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM3265_O1NLM3265_002_001 ;
        bdo:contentWidth "47" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "53" ;
        bdo:hasTitle bdr:TTMW1NLM3265_O1NLM3265_002_001 ;
        bdo:inRootInstance bdr:MW1NLM3265 ;
        bdo:paginationExtentStatement "1a-43a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM3265 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 3 ;
        skos:prefLabel "dpal gsang ba 'dus pa'i rdzogs rim rim lnga'i dmar khrid kyi brjed byang mi bskyod zhal gyi bdud rtsi/"@bo-x-ewts .

    bdr:MW1NLM3265_O1NLM3265_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM3265_O1NLM3265_003_001 ;
        bdo:authorshipStatement "wa gin+d+ra pa Tu sid+d+hi/"@bo-x-ewts ;
        bdo:colophon "ces rgyal chen rnam sras kyi gtor chog dngos grub bang mdzod kyi zur 'debs rgyas pa'i gtor sgrub bya tshul dang bcas pa gsal bar bkod pa rgyal chen dgyes pa'i dga' ston zhes bya ba 'di ni/ bstan 'dzin dam pa'i skyes mchog il ku sang hu thog thu no yon no mang han rin chen rdo rje'i gsung gis bskul ba ltar/ mkhan ming 'dzin pa wa gIn+d+ra pa Tu sid+d+his khu re chen mor bsdebs pa dad brtson rnam dpyod yangs pa dge slong ngag dbang 'chi med kyis bris pa'o/ 'dis kyang rgyal chen dgyes nas bstan pa bstan 'dzin yongs la mthun rkyen 'dod dgu'i char chen rgyun chad med par phabs par gyur cig / sarba mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM3265_O1NLM3265_003_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "53.5" ;
        bdo:hasTitle bdr:TTMW1NLM3265_O1NLM3265_003_001 ;
        bdo:inRootInstance bdr:MW1NLM3265 ;
        bdo:paginationExtentStatement "1a-25a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM3265 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "khu re chen mo/"@bo-x-ewts ;
        skos:prefLabel "rgyal chen rnam sras kyi gtor chog dngos grub bang mdzod kyi zur 'debs rgyas pa'i gtor sgrub bya tshul dang bcas pa gsal bar bkod pa rgyal chen dgyes pa'i dga' ston/"@bo-x-ewts .

    bdr:MW1NLM3265_O1NLM3265_004 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM3265_O1NLM3265_004_001 ;
        bdo:authorshipStatement "wa gin+d+ra pa Tu sid+d+hi/"@bo-x-ewts ;
        bdo:colophon "ces rgyal chen rnam sras kyi gtor chog dngos grub bang mdzod kyi zur 'debs rgyas pa'i gtor sgrub bya tshul dang bcas pa gsal bar bkod pa rgyal chen dgyes pa'i dga' ston zhes bya ba 'di ni/ bstan 'dzin dam pa'i skyes mchog il ku sang hu thog thu no yon no mang han rin chen rdo rje'i gsung gis bskul ba ltar/ mkhan ming 'dzin pa wa gIn+d+ra pa Tu sid+d+his khu re chen mor bsdebs pa dad brtson rnam dpyod yangs pa dge slong ngag dbang 'chi med kyis bris pa'o/ 'dis kyang rgyal chen dgyes nas bstan pa bstan 'dzin yongs la mthun rkyen 'dod dgu'i char chen rgyun chad med par phabs par gyur cig / sarba mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "53.5" ;
        bdo:hasTitle bdr:TTMW1NLM3265_O1NLM3265_004_001 ;
        bdo:inRootInstance bdr:MW1NLM3265 ;
        bdo:note bdr:NTMW1NLM3265_O1NLM3265_004CL ;
        bdo:paginationExtentStatement "1a-25a" ;
        bdo:partIndex 4 ;
        bdo:partOf bdr:MW1NLM3265 ;
        bdo:partTreeIndex "004" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "khu re chen mo/"@bo-x-ewts ;
        skos:prefLabel "rgyal chen rnam sras kyi gtor chog dngos grub bang mdzod kyi zur 'debs rgyas pa'i gtor sgrub bya tshul dang bcas pa gsal bar bkod pa rgyal chen dgyes pa'i dga' ston/"@bo-x-ewts .

    bdr:NTMW1NLM3265_O1NLM3265_004CL a bdo:Note ;
        bdo:noteText "Title present in the catalog but not digitized"@en .

    bdr:O1NLM3265 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM3265 .

    bdr:TTMW1NLM3265_O1NLM3265_001_001 a bdo:TitlePageTitle ;
        rdfs:label "dbu ma la 'jug pa'i bstan bcos kyi dgongs pa rab tu gsal ba'i me long*/"@bo-x-ewts .

    bdr:TTMW1NLM3265_O1NLM3265_002_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal gsang ba 'dus pa'i rdzogs rim rim lnga'i dmar khrid kyi brjed byang mi bskyod zhal gyi bdud rtsi/"@bo-x-ewts .

    bdr:TTMW1NLM3265_O1NLM3265_003_001 a bdo:TitlePageTitle ;
        rdfs:label "rgyal chen rnam sras kyi gtor chog dngos grub bang mdzod kyi zur 'debs rgyas pa'i gtor sgrub bya tshul dang bcas pa gsal bar bkod pa rgyal chen dgyes pa'i dga' ston/"@bo-x-ewts .

    bdr:TTMW1NLM3265_O1NLM3265_004_001 a bdo:TitlePageTitle ;
        rdfs:label "rgyal chen rnam sras kyi gtor chog dngos grub bang mdzod kyi zur 'debs rgyas pa'i gtor sgrub bya tshul dang bcas pa gsal bar bkod pa rgyal chen dgyes pa'i dga' ston/"@bo-x-ewts .

    bdr:MW1NLM3265 bdo:hasPart bdr:MW1NLM3265_O1NLM3265_001,
            bdr:MW1NLM3265_O1NLM3265_002,
            bdr:MW1NLM3265_O1NLM3265_003,
            bdr:MW1NLM3265_O1NLM3265_004 .
}

