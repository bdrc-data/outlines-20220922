@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM1298 {
    bda:O1NLM1298 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM1298 ;
        adm:gitPath "73/O1NLM1298.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM1298 ;
        adm:logEntry bda:LG0NLMOO1NLM1298_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM1298_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM1298_O1NLM1298_001B_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 126 ;
        bdo:contentLocationInstance bdr:W1NLM1298 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM1298_O1NLM1298_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 182 ;
        bdo:contentLocationInstance bdr:W1NLM1298 ;
        bdo:contentLocationPage 127 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM1298_O1NLM1298_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 240 ;
        bdo:contentLocationInstance bdr:W1NLM1298 ;
        bdo:contentLocationPage 183 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM1298_O1NLM1298_001B_001 a bdr:NLMId ;
        rdf:value "M0059216-001" .

    bdr:IDMW1NLM1298_O1NLM1298_002_001 a bdr:NLMId ;
        rdf:value "M0059216-002" .

    bdr:IDMW1NLM1298_O1NLM1298_003_001 a bdr:NLMId ;
        rdf:value "M0059216-003" .

    bdr:MW1NLM1298_O1NLM1298_001B a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1298_O1NLM1298_001B_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM1298_O1NLM1298_001B_001 ;
        bdo:contentWidth "47.5" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "55" ;
        bdo:hasTitle bdr:TTMW1NLM1298_O1NLM1298_001B_001 ;
        bdo:inRootInstance bdr:MW1NLM1298 ;
        bdo:paginationExtentStatement "303a-365a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM1298 ;
        bdo:partTreeIndex "001B" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "byang chub lam rim che ba las/ lhag mthong gi skabs kyi dbu phyogs so/"@bo-x-ewts .

    bdr:MW1NLM1298_O1NLM1298_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1298_O1NLM1298_002_001 ;
        bdo:authorshipStatement "ngag dbang/"@bo-x-ewts ;
        bdo:colophon "zhes pa 'di ni ngag dbang pas rang gi nyer mkho'i brjed byang du bris pa'o/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM1298_O1NLM1298_002_001 ;
        bdo:contentWidth "44" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "52.5" ;
        bdo:hasTitle bdr:TTMW1NLM1298_O1NLM1298_002_001 ;
        bdo:inRootInstance bdr:MW1NLM1298 ;
        bdo:paginationExtentStatement "1a-28a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM1298 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 3 ;
        skos:prefLabel "theg chen gso sbyong gi sdom pa'i 'bogs pa dang len tshul stobs ldan can zhes khyu mchog"@bo-x-ewts .

    bdr:MW1NLM1298_O1NLM1298_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1298_O1NLM1298_003_001 ;
        bdo:authorshipStatement "ngag dbang rdo rje/"@bo-x-ewts ;
        bdo:colophon "de ltar lam rim chen mor dga' po mngal 'jug gi mdo dang/ sa sde dang/ mdzod nas bshad pa'i skye shi bar do'i snang ba 'char tshul rnams bkod pa dang/ paN chen blo bzang chos kyi rgyal mtshan dang 'jam dbyangs bzhad pa dkon mchog 'jigs med dbang pos mdzad pa'i do'i ngo sprod dang/ lcang skya rol pa'i rdo rjes mdzad pa'i bde mchog gi lho sgo'i cho gar 'byung ba'i bar do'i ngo sprod dang/ rje'i dngos slob chos rje bsod nams rgyal mtshan gyis mdzad pa'i chos drug gi 'khrid yig tu skye 'chi bar do'i rnam bzhag mdo sngags gnyis nas gsungs tshul dang/ byin rlabs can gyi bka' rgyud gong ma'i man ngag las byung ba phyogs gcig tu bkod pa rnams las btus te go bde la yid 'khul ba'i lugs su bsdebs pa @#/ /'khrul snang dbyings grol zhes bya ba 'di ni/ \\u0f38rin can bla ma mchog gi bzhed pa sgong phyir \\u0f38btsun gzugs ngag dbang rdo rjes bgyis pa'o/ /dge legs 'phel/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM1298_O1NLM1298_003_001 ;
        bdo:contentWidth "43.5" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "52" ;
        bdo:hasTitle bdr:TTMW1NLM1298_O1NLM1298_003_001 ;
        bdo:inRootInstance bdr:MW1NLM1298 ;
        bdo:paginationExtentStatement "1a-29a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM1298 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 3 ;
        skos:prefLabel "tshe'i mthar nye bar phan pa'i man ngag 'khrul snang dbyings grol/"@bo-x-ewts .

    bdr:O1NLM1298 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM1298 .

    bdr:TTMW1NLM1298_O1NLM1298_001B_001 a bdo:TitlePageTitle ;
        rdfs:label "byang chub lam rim che ba las/ lhag mthong gi skabs kyi dbu phyogs so/"@bo-x-ewts .

    bdr:TTMW1NLM1298_O1NLM1298_002_001 a bdo:TitlePageTitle ;
        rdfs:label "theg chen gso sbyong gi sdom pa'i 'bogs pa dang len tshul stobs ldan can zhes khyu mchog"@bo-x-ewts .

    bdr:TTMW1NLM1298_O1NLM1298_003_001 a bdo:TitlePageTitle ;
        rdfs:label "tshe'i mthar nye bar phan pa'i man ngag 'khrul snang dbyings grol/"@bo-x-ewts .

    bdr:MW1NLM1298 bdo:hasPart bdr:MW1NLM1298_O1NLM1298_001B,
            bdr:MW1NLM1298_O1NLM1298_002,
            bdr:MW1NLM1298_O1NLM1298_003 .
}

