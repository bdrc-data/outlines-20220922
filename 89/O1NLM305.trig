@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM305 {
    bda:O1NLM305 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM305 ;
        adm:gitPath "89/O1NLM305.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM305 ;
        adm:logEntry bda:LG0NLMOO1NLM305_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM305_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM305_O1NLM305_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 186 ;
        bdo:contentLocationInstance bdr:W1NLM305 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM305_O1NLM305_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 370 ;
        bdo:contentLocationInstance bdr:W1NLM305 ;
        bdo:contentLocationPage 187 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM305_O1NLM305_001_001 a bdr:NLMId ;
        rdf:value "M0058218-001" .

    bdr:IDMW1NLM305_O1NLM305_002_001 a bdr:NLMId ;
        rdf:value "M0058218-002" .

    bdr:MW1NLM305_O1NLM305_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM305_O1NLM305_001_001 ;
        bdo:authorshipStatement "blo bzang chos kyi rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "ces rgyud thams cad rgyal po dpal gsang ba 'dus pa'i rim pa dang po'i rnam bshad dngos grub kyi rgya mtsho'i snying po zhes bya ba 'di ni rang gi slob ma mdo sngags rab 'byams smra ba'i dge ba'i bshes gnyen mang po'i blo gros kyi padmo'i kha 'byed pa'i phyir/ rdo rje 'chang dbang bla ma dam pa rnams kyi zhabs rdul spyi bos len pa chos sla smra blo bzang chos kyi rgyal mtshan gyis/ chos grwa chen po bkra shis lhun po'i gzi rgyal mtshan mthon por sbyar ba'o// //dpal ldan rgyud rgyal gsang ba 'dus pa yi/ rim pa dang po'i rnam bshad dngos grub kyi/ rgya mtsho'i snying po'i mchog 'di u lu gi'i/ /'gyur med bde chen gling du par du bsgrub/ thub bstan 'phel ba'i bsam sbyor dyed pa po'i/ /thub bstan byang chub 'khor bcas dus kun tu/ /thub dbang bstan pa'i snying khu nyams su len/ /thub dbang rdo rje 'chang la reg 'gyur cig / /tshul 'dir 'bad pa'i dag pas 'bral thogs kun/ kun rtog gzhon rlung bcas pa/ @#/ /rab bcom zhing/ /zhing khams kun gyi 'gro ba myur du 'dren/ /'dren mchog rdo rje 'chang la dgod par shog / //ces pa 'di ni bsngags ram pa thub bstan byang chub sogs do dom byed pa po rnams kyis bskyed rim dngos grub rgya mtsho'i snying po par du bsgrubs nas/ par byang smon tshig 'di lta bu zhig dgos zhes bskul ba'i ngor/ dka' bcu rin chen rdo rjes sug bris su bgyis pa'o/ /dge'o/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM305_O1NLM305_001_001 ;
        bdo:contentWidth "47" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "55" ;
        bdo:hasTitle bdr:TTMW1NLM305_O1NLM305_001_001 ;
        bdo:inRootInstance bdr:MW1NLM305 ;
        bdo:paginationExtentStatement "1a-93a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM305 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 3 ;
        skos:prefLabel "rgyud thams cad kyi rgyal po dpal gsang ba 'dus pa'i bskyed rim gyi rnam bshad dngos grub kyi rgya mtsho'i snying po/"@bo-x-ewts .

    bdr:MW1NLM305_O1NLM305_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM305_O1NLM305_002_001 ;
        bdo:authorshipStatement "blo bzang chos kyi rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "zhes rgyud thams cad kyi rgyal po dpal gsang ba 'dus pa'i gdams ngag rim pa lnga gsal ba'i sgron me'i @#/ /snying po'i gnad kun bsdus pa zab don gsal ba'i nyi ma zhes bya ba 'di yang/ rang gzhan la phan pa'i bsam pas/ shAkya'i dge slong 'dus pa'i rnal 'byor spyod pa/ blo bzang chos kyi rgyal mtshan gyis sbyar ba'o/ /mang+ga laM/ /oM swa sti/ dpar dpon sngags ram bzod pa 'jigs med gnyis/ gnyis med rgyud sogs dkon pa'i dpe cha mang/ mang la mi zad rgya cher spel ba'i thabs/ thabs shes zung stan gsang rdzogs spar u lu ki'i 'gyur med bde chen gling du spar bsgrubs dges/ rnam kun mchog ldan dus gsang bde 'jigs sogs/ rnam mang rgyud sde'i snying po rim gnyis kyi/ rnam dag lam bzang 'grod pa mthar phyin nas/ rnam 'dren rdo rje 'chang dbang thob gyur cig / /sbyong mdzad gsum ldan bla ma'i bka' drin las/ /sbyangs gzhi gsum po skye shi bar do sogs/ sbyong byed sku gsum lam 'khyer bskyed rdzogs kyis/ sbyangs 'bras sku gsum myur du thob par shog / /dang po yi ni dang po la/ dbyangs yig i zhugs bzang po yi/ mig la mtsho yi dang po brtsegs/ /i bkod ming can sug bris bgyis/ /@#/ /'dis kyang bstan pa spyi dang khyad par du/ rgyal ba'i dbang po blo bzang grags pa yi/ bstan pa rin chen phyogs dus thams cad du/ dar zhing rgyas la yun ring gnas gyur cig / //mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7.2" ;
        bdo:contentLocation bdr:CLMW1NLM305_O1NLM305_002_001 ;
        bdo:contentWidth "47" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "55" ;
        bdo:hasTitle bdr:TTMW1NLM305_O1NLM305_002_001 ;
        bdo:inRootInstance bdr:MW1NLM305 ;
        bdo:paginationExtentStatement "1a-92a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM305 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "rgyud thams cad kyi rgyal po dpal gsang ba 'dus pa'i gdams ngag rim lnga gsal sgron gyi snying po'i gnad kun bsdus pa zab don gsal ba'i nyi ma/"@bo-x-ewts .

    bdr:O1NLM305 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM305 .

    bdr:TTMW1NLM305_O1NLM305_001_001 a bdo:TitlePageTitle ;
        rdfs:label "rgyud thams cad kyi rgyal po dpal gsang ba 'dus pa'i bskyed rim gyi rnam bshad dngos grub kyi rgya mtsho'i snying po/"@bo-x-ewts .

    bdr:TTMW1NLM305_O1NLM305_002_001 a bdo:TitlePageTitle ;
        rdfs:label "rgyud thams cad kyi rgyal po dpal gsang ba 'dus pa'i gdams ngag rim lnga gsal sgron gyi snying po'i gnad kun bsdus pa zab don gsal ba'i nyi ma/"@bo-x-ewts .

    bdr:MW1NLM305 bdo:hasPart bdr:MW1NLM305_O1NLM305_001,
            bdr:MW1NLM305_O1NLM305_002 .
}

