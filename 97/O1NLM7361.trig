@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7361 {
    bda:LG0NLMOO1NLM7361_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7361  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7361 ;
        adm:gitPath         "97/O1NLM7361.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7361 ;
        adm:logEntry        bda:LG0NLMOO1NLM7361_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7361_O1NLM7361_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  138 ;
        bdo:contentLocationInstance  bdr:W1NLM7361 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7361_O1NLM7361_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  180 ;
        bdo:contentLocationInstance  bdr:W1NLM7361 ;
        bdo:contentLocationPage  139 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7361_O1NLM7361_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  224 ;
        bdo:contentLocationInstance  bdr:W1NLM7361 ;
        bdo:contentLocationPage  181 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7361_O1NLM7361_004_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  262 ;
        bdo:contentLocationInstance  bdr:W1NLM7361 ;
        bdo:contentLocationPage  225 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7361_O1NLM7361_005_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  288 ;
        bdo:contentLocationInstance  bdr:W1NLM7361 ;
        bdo:contentLocationPage  263 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7361_O1NLM7361_006_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  314 ;
        bdo:contentLocationInstance  bdr:W1NLM7361 ;
        bdo:contentLocationPage  289 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7361_O1NLM7361_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0065279-001" .
    
    bdr:IDMW1NLM7361_O1NLM7361_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0065279-002" .
    
    bdr:IDMW1NLM7361_O1NLM7361_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0065279-003" .
    
    bdr:IDMW1NLM7361_O1NLM7361_004_001
        a                   bdr:NLMId ;
        rdf:value           "M0065279-004" .
    
    bdr:IDMW1NLM7361_O1NLM7361_005_001
        a                   bdr:NLMId ;
        rdf:value           "M0065279-005" .
    
    bdr:IDMW1NLM7361_O1NLM7361_006_001
        a                   bdr:NLMId ;
        rdf:value           "M0065279-006" .
    
    bdr:MW1NLM7361  bdo:hasPart  bdr:MW1NLM7361_O1NLM7361_001 , bdr:MW1NLM7361_O1NLM7361_002 , bdr:MW1NLM7361_O1NLM7361_003 , bdr:MW1NLM7361_O1NLM7361_004 , bdr:MW1NLM7361_O1NLM7361_005 , bdr:MW1NLM7361_O1NLM7361_006 .
    
    bdr:MW1NLM7361_O1NLM7361_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7361_O1NLM7361_001_001 ;
        bdo:authorshipStatement  "su ma ti many+dzu g+ho ShA/"@bo-x-ewts ;
        bdo:colophon        "ces pa 'di ni dad stobs blo gros kyi nor ldan er te ni phyag mdzod pa blo bzang bkra shis nas rgyu yon sbyor te rab stod kyi blo rtags hu re chen mor spar du bskrun pa'i tshe spar byang dgos zhes chos grwa g.yas pa'i las sna rnams kyis bskul ngor/ \\u0f38gong ma'i lung gis no min han du bsngags pa \\u0f38yongs 'dzin mkhan po su ma ti many+dzu g+ho ShAs sbyar ba'i yi ge pa ni rab 'byams pa ngag dbang thub bstan no/ /'dis kyang rgyal bstan yun ring du gnas pa'i rgyur gyur cig /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7361_O1NLM7361_001_001 ;
        bdo:contentWidth    "50" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "61.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7361_O1NLM7361_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7361 ;
        bdo:paginationExtentStatement  "1a-67a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7361 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "blo rigs kyi rnam bzhag rigs pa'i gter mdzod/"@bo-x-ewts .
    
    bdr:MW1NLM7361_O1NLM7361_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7361_O1NLM7361_002_001 ;
        bdo:colophon        "ces gtan tshigs rigs pa'i rnams bzhag gsal bar bshad pa yongs 'dus nags tshal zhes bya ba 'di ni dgon lung byams gling gi mkhan po u cu mu chi na lha rams pa bskal bzang lha dbang gis u cu mu chin dbang gi lha khang chen mo theg chen chos 'khor gling du sbyar ba'i yi ge pa ni dge slong blo bzang snyan grags so/ / 'dis kyang sangs rgyas kyi bstan pa rin po che phyogs dus gnas skabs thams cad du dar zhing rgyas la yun ring du gnas par gyur gyur cig /bkra shis par gyur cig /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "8.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7361_O1NLM7361_002_001 ;
        bdo:contentWidth    "55" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "61.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7361_O1NLM7361_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7361 ;
        bdo:paginationExtentStatement  "1a-21b" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7361 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "gtan tshigs rigs pa'i rnam bzhag gsal bar bshad pa yongs 'dus nags tshal/"@bo-x-ewts .
    
    bdr:MW1NLM7361_O1NLM7361_003
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7361_O1NLM7361_003_001 ;
        bdo:authorshipStatement  "dkon mchog chos 'phel/"@bo-x-ewts ;
        bdo:colophon        "zhes pa 'di ni rigs pa smra ba yongs kyi tha shal dge slong dkon mchog chos 'phel gyis/ \\u0f38skyabs mgon mchog gi sprul pa'i sku rin po che la mchod pa'i sprin du spros pa'o// //'di yang dga' ldan chos 'khor gling nas par du grug //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7361_O1NLM7361_003_001 ;
        bdo:contentWidth    "55.4" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "61.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7361_O1NLM7361_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM7361 ;
        bdo:paginationExtentStatement  "1a-22a" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM7361 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "rtags rigs kyi rnam bzhag nyung gsal legs bshad gser gyi phreng mdzes kyi mchan/"@bo-x-ewts .
    
    bdr:MW1NLM7361_O1NLM7361_004
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7361_O1NLM7361_004_001 ;
        bdo:authorshipStatement  "rat+na d+hArma wa ti'i ming can/"@bo-x-ewts ;
        bdo:colophon        "zhes kun mkhyen chen po 'jam dbyangs bzhad pa'i rdo rjes mdzad pa'i blo rigs gi rnam bzhag legs bshad 'phreng mdzes kyi nang du drangs pi tshad ma mdo dang rnam 'grel sogs kyi lung phal cher gyi don zhib tu bkral te dka' gnad kyi skor 'ga'' zhig la dogs gcod dang bcas te smra ba 'di yi byed pa po ni/ dus gsum rgyal ba'i spyi gzugs/ \\u0f38mchog gi sprul ba'i sku blo bzang thub bstan 'jigs med rgya mtsho'i zhabs rdul spyi bor len pa rigs pa smra ba yongs kyi tha shal btsun pa'i gzugs brnyan/ rat+na d+hArma wa ti'i ming can bdag lags so/ /'di yang kun mkhyen chen po 'jam dbyangs bzhad pa'i sde'i/ /thugs rjes la brten bkra shis 'khyil gling gi /lho rgyud mdo sngags chos s+s+g+ra sgrogs pa'i tshal/ /dga' ldan chos 'khor gling nas par du grub/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "8" ;
        bdo:contentLocation  bdr:CLMW1NLM7361_O1NLM7361_004_001 ;
        bdo:contentWidth    "55.5" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "61.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7361_O1NLM7361_004_001 ;
        bdo:inRootInstance  bdr:MW1NLM7361 ;
        bdo:paginationExtentStatement  "1a-19a" ;
        bdo:partIndex       4 ;
        bdo:partOf          bdr:MW1NLM7361 ;
        bdo:partTreeIndex   "004" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "kun mkhyen 'jam dbyangs bzhad pa'i rdo rjes mdzad pa'i blo rig gi rnam bzhag nyung gsal legs bshad gser gyi 'phreng mdzes kyi mchan 'grel gser gyi lde mig"@bo-x-ewts .
    
    bdr:MW1NLM7361_O1NLM7361_005
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7361_O1NLM7361_005_001 ;
        bdo:authorshipStatement  "blo bzang ye shes/"@bo-x-ewts ;
        bdo:colophon        "tshul @#/ /'di ni chos gra chen po bkra shis lhun po'i rgyud pa grwa tshang gi 'don rgyun gzhir byas te/ shAkya'i btsun pa blo bzang ye shes kyis dam pa gong ma rnams kyis mdzad pa'i gsang bde 'jigs gsum gyi sbyin sreg sogs sbyin sreg mang po la legs par brtags nas zhuns dag bgyis pa'o// //mang+ga laM//"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7361_O1NLM7361_005_001 ;
        bdo:contentWidth    "49.5" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "61.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7361_O1NLM7361_005_001 ;
        bdo:inRootInstance  bdr:MW1NLM7361 ;
        bdo:paginationExtentStatement  "1a-13a" ;
        bdo:partIndex       5 ;
        bdo:partOf          bdr:MW1NLM7361 ;
        bdo:partTreeIndex   "005" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "dpal rdo rje 'jigs byed kyi zhi rgyas kyi sbyin sreg gi ngag 'don/"@bo-x-ewts .
    
    bdr:MW1NLM7361_O1NLM7361_006
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7361_O1NLM7361_006_001 ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7361_O1NLM7361_006_001 ;
        bdo:contentWidth    "58" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "62" ;
        bdo:hasTitle        bdr:TTMW1NLM7361_O1NLM7361_006_001 ;
        bdo:inRootInstance  bdr:MW1NLM7361 ;
        bdo:paginationExtentStatement  "1a-13a" ;
        bdo:partIndex       6 ;
        bdo:partOf          bdr:MW1NLM7361 ;
        bdo:partTreeIndex   "006" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "dpal ldan stag tshang rab bstod pa'i gzhan sel rgol dan tshang gcod gnam lcags thog 'bebs/"@bo-x-ewts .
    
    bdr:O1NLM7361  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7361 .
    
    bdr:TTMW1NLM7361_O1NLM7361_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "blo rigs kyi rnam bzhag rigs pa'i gter mdzod/"@bo-x-ewts .
    
    bdr:TTMW1NLM7361_O1NLM7361_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gtan tshigs rigs pa'i rnam bzhag gsal bar bshad pa yongs 'dus nags tshal/"@bo-x-ewts .
    
    bdr:TTMW1NLM7361_O1NLM7361_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rtags rigs kyi rnam bzhag nyung gsal legs bshad gser gyi phreng mdzes kyi mchan/"@bo-x-ewts .
    
    bdr:TTMW1NLM7361_O1NLM7361_004_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "kun mkhyen 'jam dbyangs bzhad pa'i rdo rjes mdzad pa'i blo rig gi rnam bzhag nyung gsal legs bshad gser gyi 'phreng mdzes kyi mchan 'grel gser gyi lde mig"@bo-x-ewts .
    
    bdr:TTMW1NLM7361_O1NLM7361_005_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dpal rdo rje 'jigs byed kyi zhi rgyas kyi sbyin sreg gi ngag 'don/"@bo-x-ewts .
    
    bdr:TTMW1NLM7361_O1NLM7361_006_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dpal ldan stag tshang rab bstod pa'i gzhan sel rgol dan tshang gcod gnam lcags thog 'bebs/"@bo-x-ewts .
}
