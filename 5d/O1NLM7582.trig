@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7582 {
    bda:LG0NLMOO1NLM7582_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7582  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7582 ;
        adm:gitPath         "5d/O1NLM7582.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7582 ;
        adm:logEntry        bda:LG0NLMOO1NLM7582_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7582_O1NLM7582_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  154 ;
        bdo:contentLocationInstance  bdr:W1NLM7582 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7582_O1NLM7582_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  238 ;
        bdo:contentLocationInstance  bdr:W1NLM7582 ;
        bdo:contentLocationPage  155 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7582_O1NLM7582_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  288 ;
        bdo:contentLocationInstance  bdr:W1NLM7582 ;
        bdo:contentLocationPage  239 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7582_O1NLM7582_004_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  294 ;
        bdo:contentLocationInstance  bdr:W1NLM7582 ;
        bdo:contentLocationPage  289 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7582_O1NLM7582_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0065500-001" .
    
    bdr:IDMW1NLM7582_O1NLM7582_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0065500-002" .
    
    bdr:IDMW1NLM7582_O1NLM7582_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0065500-003" .
    
    bdr:IDMW1NLM7582_O1NLM7582_004_001
        a                   bdr:NLMId ;
        rdf:value           "M0065500-004" .
    
    bdr:MW1NLM7582  bdo:hasPart  bdr:MW1NLM7582_O1NLM7582_001 , bdr:MW1NLM7582_O1NLM7582_002 , bdr:MW1NLM7582_O1NLM7582_003 , bdr:MW1NLM7582_O1NLM7582_004 .
    
    bdr:MW1NLM7582_O1NLM7582_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7582_O1NLM7582_001_001 ;
        bdo:colophon        "oM swa sti/ rgyal bstan pad tshal phan bde'i dri gsung can/ / bshad sgrub 'dab rgyas rnam grol sbrang rtsi'i bcud/ /'gro kun spyod phyir sku 'bum byams pa gling/ /chos sde che nas chos sbyin char 'di spel// //sarba mang+ga laM// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM7582_O1NLM7582_001_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "53" ;
        bdo:hasTitle        bdr:TTMW1NLM7582_O1NLM7582_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7582 ;
        bdo:paginationExtentStatement  "1a-77a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7582 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "'grel chen dri med 'od kyi dbang gi le'u'i 'grel bshad gyi stod cha mdor bsdus gsum pa yan chad/"@bo-x-ewts .
    
    bdr:MW1NLM7582_O1NLM7582_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7582_O1NLM7582_002_001 ;
        bdo:colophon        "zhes pa 'di ni kun mkhyen 'jam dbyangs bzhad pa'i rdo rjes mdzad pa lags so// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM7582_O1NLM7582_002_001 ;
        bdo:contentWidth    "44.5" ;
        bdo:dimHeight       "9.5" ;
        bdo:dimWidth        "52.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7582_O1NLM7582_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7582 ;
        bdo:paginationExtentStatement  "1a-42b" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7582 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "drang nges rnam 'byed kyi mtha' dpyod mdor bsdus blo gsal mgrin rgyan/"@bo-x-ewts .
    
    bdr:MW1NLM7582_O1NLM7582_003
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7582_O1NLM7582_003_001 ;
        bdo:authorshipStatement  "blo bzang tshul khrims rgya mtsho/"@bo-x-ewts ;
        bdo:colophon        "ces sngon 'gro khrid chen bzhi'i dmigs rim 'di ni/ rang nyid la rab 'byams gzhung lugs rgya mtsho chen por thos bsam gyi sgro 'dogs chod cing/ sgom pa'i nyams rtogs kyi klong brdol ba'i dbang gis 'chad rtsod rtsom pa la thogs pa med pa'i yon tan gyi tshogs til 'bru tsam yang med pas chos 'chad pa dang rtsom pa sogs ga la 'os mod/ sangs rgyas dngos dang khyad par med pa'i gsum ldan rtsa ba'i bla ma grub dbang rin po che sogs dam pa du ma'i zhabs rdul spyi bor brten pa las/ mdo sngags kyi man ngag zab rgyas kyi bka' drin cung zad mnos pa la skyabs 'cha' ste/ skal ldan gyi slob ma bcu phrag lnga brgal par sngon 'gro bzhi'i khrid lung phul ba'i rjes su/ /rab 'byams gzhung lugs kyi dgongs don la blo mig gsal ba ja rod zhabs drung 'jam dbyangs tshe ldan dang/ gso ba rig pa'i man ngag la nang byan chu de pa u cu mu chin shog gi rab 'byams pa 'jam dbyangs blo bzang sogs slob ma du mas lha rdzas dang/ rin po che gnyis pa srang nyi shu rtsa lnga sogs skyes bzang po dang bcas khrid chen bzhi'i dmigs rim gsal zhing go bde ba zhig dgos zhes bskul ngor/ sku 'bum mkhan zur gser tog no mon khan blo bzang tshul khrims rgya mtsho'am/ /ming gzhan mi pham tshangs sras dgyes pa'i rdo rjes rang lo bcu phrag gsum dang nyag ma drug tu slebs pa'i khrums zla ba'i tshes bzang po nas 'go brtsams te/ tha skar zla ba'i rgyal ba dang po'i nyin mjug grub par bgyis te/ sku 'bum gser tog bla brang bde chen bsam gtan gling gi gzims chung bkra shis g.yang 'khyil gyi yang rtser sbyar ba'o// //tshul 'di khal kha se chen rgyal khag gi /bstan pa'i sbyin bdag chen po tA cang jung/ /se chen pE li tshe ring bsam grub dang/ /g.yung drung rdo rje yid bzhin 'khor lo bcas/ /mthun rkyen sbyar te dpar du bsgrubs pa'i dges/ / skye ba'i phreng bar dal 'byor rten bzang thob/ /drin can bla ma mchog dang mi 'bral zhing/ /sa lam yon tan yongs su mthar phyin shog / //mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM7582_O1NLM7582_003_001 ;
        bdo:contentWidth    "44" ;
        bdo:dimHeight       "8" ;
        bdo:dimWidth        "51.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7582_O1NLM7582_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM7582 ;
        bdo:paginationExtentStatement  "1a-25b" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM7582 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  2 ;
        skos:prefLabel      "sngon 'gro khrid chen bzhi las/ bla ma'i rnal 'byor gyi dmigs rim bshad pa dngos grub sbrang char 'jo ba'i byin rlabs chu 'dzin dbang po/"@bo-x-ewts .
    
    bdr:MW1NLM7582_O1NLM7582_004
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7582_O1NLM7582_004_001 ;
        bdo:authorshipStatement  "rgyal sras ming can/"@bo-x-ewts ;
        bdo:colophon        "ces pa 'di ni rgyal sras ming can gyis rang gi 'dod mos ltar sug bris su bgyis pa dza yan+tu// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7582_O1NLM7582_004_001 ;
        bdo:contentWidth    "45" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "49.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7582_O1NLM7582_004_001 ;
        bdo:inRootInstance  bdr:MW1NLM7582 ;
        bdo:paginationExtentStatement  "1a-3a" ;
        bdo:partIndex       4 ;
        bdo:partOf          bdr:MW1NLM7582 ;
        bdo:partTreeIndex   "004" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "bsangs mchod 'dod dgur 'khyil ba'i dga' ston/"@bo-x-ewts .
    
    bdr:O1NLM7582  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7582 .
    
    bdr:TTMW1NLM7582_O1NLM7582_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "'grel chen dri med 'od kyi dbang gi le'u'i 'grel bshad gyi stod cha mdor bsdus gsum pa yan chad/"@bo-x-ewts .
    
    bdr:TTMW1NLM7582_O1NLM7582_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "drang nges rnam 'byed kyi mtha' dpyod mdor bsdus blo gsal mgrin rgyan/"@bo-x-ewts .
    
    bdr:TTMW1NLM7582_O1NLM7582_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "sngon 'gro khrid chen bzhi las/ bla ma'i rnal 'byor gyi dmigs rim bshad pa dngos grub sbrang char 'jo ba'i byin rlabs chu 'dzin dbang po/"@bo-x-ewts .
    
    bdr:TTMW1NLM7582_O1NLM7582_004_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bsangs mchod 'dod dgur 'khyil ba'i dga' ston/"@bo-x-ewts .
}
