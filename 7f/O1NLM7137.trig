@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7137 {
    bda:LG0NLMOO1NLM7137_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7137  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7137 ;
        adm:gitPath         "7f/O1NLM7137.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7137 ;
        adm:logEntry        bda:LG0NLMOO1NLM7137_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7137_O1NLM7137_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  210 ;
        bdo:contentLocationInstance  bdr:W1NLM7137 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7137_O1NLM7137_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  215 ;
        bdo:contentLocationInstance  bdr:W1NLM7137 ;
        bdo:contentLocationPage  211 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7137_O1NLM7137_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  288 ;
        bdo:contentLocationInstance  bdr:W1NLM7137 ;
        bdo:contentLocationPage  215 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7137_O1NLM7137_004_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  362 ;
        bdo:contentLocationInstance  bdr:W1NLM7137 ;
        bdo:contentLocationPage  289 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7137_O1NLM7137_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0065055-001" .
    
    bdr:IDMW1NLM7137_O1NLM7137_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0065055-002" .
    
    bdr:IDMW1NLM7137_O1NLM7137_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0065055-003" .
    
    bdr:IDMW1NLM7137_O1NLM7137_004_001
        a                   bdr:NLMId ;
        rdf:value           "M0065055-004" .
    
    bdr:MW1NLM7137  bdo:hasPart  bdr:MW1NLM7137_O1NLM7137_001 , bdr:MW1NLM7137_O1NLM7137_002 , bdr:MW1NLM7137_O1NLM7137_003 , bdr:MW1NLM7137_O1NLM7137_004 .
    
    bdr:MW1NLM7137_O1NLM7137_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7137_O1NLM7137_001_001 ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7137_O1NLM7137_001_001 ;
        bdo:contentWidth    "47" ;
        bdo:dimHeight       "9.5" ;
        bdo:dimWidth        "53.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7137_O1NLM7137_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7137 ;
        bdo:paginationExtentStatement  "1a-105a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7137 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "rje btsun chos kyi rgyal mtshan dpal bzang po'i gsung ji lta ba bzhin yi ger bkod pa skabs brgyad pa'i spyi don/"@bo-x-ewts .
    
    bdr:MW1NLM7137_O1NLM7137_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7137_O1NLM7137_002_001 ;
        bdo:conditionGrade  3 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7137_O1NLM7137_002_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "57" ;
        bdo:hasTitle        bdr:TTMW1NLM7137_O1NLM7137_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7137 ;
        bdo:paginationExtentStatement  "1a-3a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7137 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  3 ;
        skos:prefLabel      "bod kyi brda'i bstan bcos sum cu pa zhes bya ba'i rnam bshad pa kun tu bzang po'i dgongs pa rab tu gsal bar byed pa'i rgyan/"@bo-x-ewts .
    
    bdr:MW1NLM7137_O1NLM7137_003
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7137_O1NLM7137_003_001 ;
        bdo:authorshipStatement  "dkon mchog 'jigs med dbang po/"@bo-x-ewts ;
        bdo:colophon        "ces rig gnas rgya mtsho'i ba mthar son pa'i paN+Dita chen po rin chen don grub kyis mdzad pa'i sum cu pa'i dgongs 'grel 'di/ dgongs 'grel gzhan las khyad par du 'phags shing blo skyed shin tu che bar brten/ chos grwa chen po bkra shis 'khyil du dpar du sgrub pa'i dpar byang du rig gnas smra ba btsun pa/ \\u0f38 dkon mchog 'jigs med dbang pos sbyar ba'o// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7137_O1NLM7137_003_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "57" ;
        bdo:hasTitle        bdr:TTMW1NLM7137_O1NLM7137_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM7137 ;
        bdo:paginationExtentStatement  "3a-39a" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM7137 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  3 ;
        skos:prefLabel      "lung ston pa sum cu pa/"@bo-x-ewts .
    
    bdr:MW1NLM7137_O1NLM7137_004
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7137_O1NLM7137_004_001 ;
        bdo:conditionGrade  3 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7137_O1NLM7137_004_001 ;
        bdo:contentWidth    "38" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "37" ;
        bdo:hasTitle        bdr:TTMW1NLM7137_O1NLM7137_004_001 ;
        bdo:inRootInstance  bdr:MW1NLM7137 ;
        bdo:paginationExtentStatement  "1a-37a" ;
        bdo:partIndex       4 ;
        bdo:partOf          bdr:MW1NLM7137 ;
        bdo:partTreeIndex   "004" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "bod kyi brda'i bstan bcos legs par bshad pa rin po che'i za ma tog bkod pa/"@bo-x-ewts .
    
    bdr:O1NLM7137  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7137 .
    
    bdr:TTMW1NLM7137_O1NLM7137_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rje btsun chos kyi rgyal mtshan dpal bzang po'i gsung ji lta ba bzhin yi ger bkod pa skabs brgyad pa'i spyi don/"@bo-x-ewts .
    
    bdr:TTMW1NLM7137_O1NLM7137_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bod kyi brda'i bstan bcos sum cu pa zhes bya ba'i rnam bshad pa kun tu bzang po'i dgongs pa rab tu gsal bar byed pa'i rgyan/"@bo-x-ewts .
    
    bdr:TTMW1NLM7137_O1NLM7137_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "lung ston pa sum cu pa/"@bo-x-ewts .
    
    bdr:TTMW1NLM7137_O1NLM7137_004_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bod kyi brda'i bstan bcos legs par bshad pa rin po che'i za ma tog bkod pa/"@bo-x-ewts .
}
