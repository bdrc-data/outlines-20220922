@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM5160 {
    bda:O1NLM5160 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM5160 ;
        adm:gitPath "bb/O1NLM5160.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM5160 ;
        adm:logEntry bda:LG0NLMOO1NLM5160_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM5160_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM5160_O1NLM5160_001B_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 212 ;
        bdo:contentLocationInstance bdr:W1NLM5160 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5160_O1NLM5160_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 220 ;
        bdo:contentLocationInstance bdr:W1NLM5160 ;
        bdo:contentLocationPage 213 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5160_O1NLM5160_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 312 ;
        bdo:contentLocationInstance bdr:W1NLM5160 ;
        bdo:contentLocationPage 221 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM5160_O1NLM5160_001B_001 a bdr:NLMId ;
        rdf:value "M0063078-001" .

    bdr:IDMW1NLM5160_O1NLM5160_002_001 a bdr:NLMId ;
        rdf:value "M0063078-002" .

    bdr:IDMW1NLM5160_O1NLM5160_003_001 a bdr:NLMId ;
        rdf:value "M0063078-003" .

    bdr:MW1NLM5160_O1NLM5160_001B a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5160_O1NLM5160_001B_001 ;
        bdo:authorshipStatement "ngag dbang rdo rje/"@bo-x-ewts ;
        bdo:colophon "zhes pa 'di ni/ skyabs mgon sku phreng lnga pa/ blo bzang tshul khrims 'jigs med bstan pa'i rgyal mtshan dpal bzang pos lung kha'i pho brang du lha mchog 'di bsnyen sgrub la thugs nyams bzhes mdzad skabs/ mkha' spyod dbang mo'i zhal bzang mngon du gyur nas thun mong dang thun mong ma yin pa'i zab rgyas kyi gdams pa mang po stsal bar brten/ grub pa'i gnas mchog de nyid du dus mtshungs kyi lo re bzhin gyi sgrub mchod kyi rgyun 'dzugs skabs kyi/ de nyid kyi gsung srol ltar/ chos rje ngag dbang rdo rjes ngag 'don gyi rim par sgrigs pa yin lags/ /'di'i dge slong ye shes smon lam gyis mthun rkyen byas nas 'bar du bsgrub// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM5160_O1NLM5160_001B_001 ;
        bdo:contentWidth "16.5" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "22" ;
        bdo:hasTitle bdr:TTMW1NLM5160_O1NLM5160_001B_001 ;
        bdo:inRootInstance bdr:MW1NLM5160 ;
        bdo:paginationExtentStatement "1a-115a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM5160 ;
        bdo:partTreeIndex "001B" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "rje btsun nA ro mkha' spyod ma'i sin+d+hu ra'i dkyil 'khor sgrub cing mchod pa dang bdag nyid 'jug cing dbang blang ba'i cho ga cha lag dang bcas pa/"@bo-x-ewts .

    bdr:MW1NLM5160_O1NLM5160_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5160_O1NLM5160_002_001 ;
        bdo:authorshipStatement "blo bzang chos kyi rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "zhes dpal rdo rje 'jigs byed kyi bum bskyed ngag 'don gyi rim par dril bar 'di yang dge slong blo bzang chos kyi rgyal mtshan gyis sbyar ba'o// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM5160_O1NLM5160_002_001 ;
        bdo:contentWidth "16.5" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "22" ;
        bdo:hasTitle bdr:TTMW1NLM5160_O1NLM5160_002_001 ;
        bdo:inRootInstance bdr:MW1NLM5160 ;
        bdo:paginationExtentStatement "1a-4b" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM5160 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dpal rdo rje 'jigs byed kyi bum bskyed/"@bo-x-ewts .

    bdr:MW1NLM5160_O1NLM5160_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5160_O1NLM5160_003_001 ;
        bdo:colophon "ces dpal rdo rje 'jigs byed chen po'i dkyil 'khor bsgrub cing mchod bdag 'jug blang ba'i tshul las/ bsgrub mchod bya ba'i tshul yongs rdzogs bstan pa'i mnga' bdag /rdo rje 'chang ngag dbang blo bzang chos ldan dpal bzang pos/ khams gsum chos kyi rgyal @#/ /po shar tsong kha pa blo bzang grags pa'i dpal gyis mdzad pa'i dkyil cho ga rin chen 'phreng ba nas bshad pa bzhin za ma tog gi lhan thabs su ..."@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM5160_O1NLM5160_003_001 ;
        bdo:contentWidth "16.5" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "22" ;
        bdo:hasTitle bdr:TTMW1NLM5160_O1NLM5160_003_001 ;
        bdo:inRootInstance bdr:MW1NLM5160 ;
        bdo:paginationExtentStatement "1a-47a inc [*missing page from 47a*]" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM5160 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "'jigs byed kyi dbang chog"@bo-x-ewts .

    bdr:O1NLM5160 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM5160 .

    bdr:TTMW1NLM5160_O1NLM5160_001B_001 a bdo:TitlePageTitle ;
        rdfs:label "rje btsun nA ro mkha' spyod ma'i sin+d+hu ra'i dkyil 'khor sgrub cing mchod pa dang bdag nyid 'jug cing dbang blang ba'i cho ga cha lag dang bcas pa/"@bo-x-ewts .

    bdr:TTMW1NLM5160_O1NLM5160_002_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal rdo rje 'jigs byed kyi bum bskyed/"@bo-x-ewts .

    bdr:TTMW1NLM5160_O1NLM5160_003_001 a bdo:TitlePageTitle ;
        rdfs:label "'jigs byed kyi dbang chog"@bo-x-ewts .

    bdr:MW1NLM5160 bdo:hasPart bdr:MW1NLM5160_O1NLM5160_001B,
            bdr:MW1NLM5160_O1NLM5160_002,
            bdr:MW1NLM5160_O1NLM5160_003 .
}

