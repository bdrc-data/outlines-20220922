@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM333 {
    bda:O1NLM333 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM333 ;
        adm:gitPath "b3/O1NLM333.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM333 ;
        adm:logEntry bda:LG0NLMOO1NLM333_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusWithdrawn .

    bda:LG0NLMOO1NLM333_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM333_O1NLM333_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 462 ;
        bdo:contentLocationInstance bdr:W1NLM333 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM333_O1NLM333_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 798 ;
        bdo:contentLocationInstance bdr:W1NLM333 ;
        bdo:contentLocationPage 463 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM333_O1NLM333_001_001 a bdr:NLMId ;
        rdf:value "M0058246-001" .

    bdr:IDMW1NLM333_O1NLM333_002_001 a bdr:NLMId ;
        rdf:value "M0058246-002" .

    bdr:MW1NLM333_O1NLM333_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM333_O1NLM333_001_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "9" ;
        bdo:contentLocation bdr:CLMW1NLM333_O1NLM333_001_001 ;
        bdo:contentWidth "30" ;
        bdo:dimHeight "11" ;
        bdo:dimWidth "35.5" ;
        bdo:hasTitle bdr:TTMW1NLM333_O1NLM333_001_001 ;
        bdo:inRootInstance bdr:MW1NLM333 ;
        bdo:paginationExtentStatement "1a-231a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM333 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "rje rgyal po chen po ki si nas bi kra mi ji ta'i khri la bzhugs pa'i phyir du shing mi'i dang po mahA kA la nas bzung ste shing mi so gnyis pa'i bar du phan tsh+hun smra ba'i lo rgyus/"@bo-x-ewts .

    bdr:MW1NLM333_O1NLM333_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM333_O1NLM333_002_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "8.5" ;
        bdo:contentLocation bdr:CLMW1NLM333_O1NLM333_002_001 ;
        bdo:contentWidth "30" ;
        bdo:dimHeight "11.5" ;
        bdo:dimWidth "36" ;
        bdo:hasTitle bdr:TTMW1NLM333_O1NLM333_002_001 ;
        bdo:inRootInstance bdr:MW1NLM333 ;
        bdo:paginationExtentStatement "1a-168b" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM333 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "rje rgyal po chen po ki si nas bi kra mi ji ta'i khri la bzhugs pa'i phyir du shing mi'i dang po mahA kA la nas bzung ste shing mi so gnyis pa'i bar du phan tsh+hun smra ba'i lo rgyus/"@bo-x-ewts .

    bdr:O1NLM333 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM333 .

    bdr:TTMW1NLM333_O1NLM333_001_001 a bdo:TitlePageTitle ;
        rdfs:label "rje rgyal po chen po ki si nas bi kra mi ji ta'i khri la bzhugs pa'i phyir du shing mi'i dang po mahA kA la nas bzung ste shing mi so gnyis pa'i bar du phan tsh+hun smra ba'i lo rgyus/"@bo-x-ewts .

    bdr:TTMW1NLM333_O1NLM333_002_001 a bdo:TitlePageTitle ;
        rdfs:label "rje rgyal po chen po ki si nas bi kra mi ji ta'i khri la bzhugs pa'i phyir du shing mi'i dang po mahA kA la nas bzung ste shing mi so gnyis pa'i bar du phan tsh+hun smra ba'i lo rgyus/"@bo-x-ewts .

    bdr:MW1NLM333 bdo:hasPart bdr:MW1NLM333_O1NLM333_001,
            bdr:MW1NLM333_O1NLM333_002 .
}

