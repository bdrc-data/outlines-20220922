@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM5181 {
    bda:O1NLM5181 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM5181 ;
        adm:gitPath "6d/O1NLM5181.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM5181 ;
        adm:logEntry bda:LG0NLMOO1NLM5181_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM5181_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM5181_O1NLM5181_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 172 ;
        bdo:contentLocationInstance bdr:W1NLM5181 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5181_O1NLM5181_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 288 ;
        bdo:contentLocationInstance bdr:W1NLM5181 ;
        bdo:contentLocationPage 173 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM5181_O1NLM5181_001_001 a bdr:NLMId ;
        rdf:value "M0063099-001" .

    bdr:IDMW1NLM5181_O1NLM5181_002_001 a bdr:NLMId ;
        rdf:value "M0063099-002" .

    bdr:MW1NLM5181_O1NLM5181_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5181_O1NLM5181_001_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM5181_O1NLM5181_001_001 ;
        bdo:contentWidth "38" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "44.5" ;
        bdo:hasTitle bdr:TTMW1NLM5181_O1NLM5181_001_001 ;
        bdo:inRootInstance bdr:MW1NLM5181 ;
        bdo:paginationExtentStatement "1a-86b" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM5181 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "rgyud kyi rgyal po dpal gsang ba 'dus pa'i rim gnyis rgyud 'grel dang bcas pa bshad thabs kyi gsung rgyun man ngag rdo lag brgyud ma/"@bo-x-ewts .

    bdr:MW1NLM5181_O1NLM5181_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5181_O1NLM5181_002_001 ;
        bdo:colophon "zhes dpal rdo rje 'jigs byed kyi zab lam rim pa rdzogs rim sku gsum them skas zhes bya ba 'di ni/ bla ma dam pa rnams kyi bka' drin gyis/ /lugs 'dir thos bsam gyis blo cung zad 'dris shing bsgos pa ltar zlos pa/ gu shri chos rjes blo bzang lhun grub dang/ mng gzhan sgog pa'i rdo rje zhes bya bas/ dben dam pa rgyal ba blo bzang don grub kyis mdzad pa'i rdzogs rim dang/ rje lha btsun rin chen rgya mtsho blo gros dpal bzang pos mdzad pa'i rdzogs rim dang/ paN chen blo bzang chos rgyan gyis mdzad pa'i rdzogs rim dang/ mkhas mchog blo ldan dbang pos mdzad pa'i rdzogs rim dang/ rjes 'jam dbyangs bzhad pa rin po ches mdzad pa'i rdzogs rim dang/ thang sag la dngos grub rgya mtshos mdzad pa'i rdzogs rim dang/ lcang skya rin po che ngag dbang blo bzang chos ldan gyis mdzad pa'i rdzogs rim dang/ mkhas mchog d+harmA ka rtis mdzad pa'i rdzogs rim dang/ mkhas dbang legs pa don grub kyis mdzad pa'i rdzogs @#/ /rim dang/ rjes ngag dbang byams pas mdzad pa'i rdzogs rim dang/ bla ma rin po ches lam mchog blo bzang bstan 'dzin gyis mdzad pa'i rdzogs rim dang/ mkhas pa ye shes bskal bzang gis mdzad pa'i rdzogs rim dang! gzhan yang mkhas grub rin po che'i yig chung nyer gcig dang/ paN chen rin po che'i rim lnga'i rnam bshad dang/ rdo rje 'chang sum pa mkhan chen gyis mdzad pa'i rgyud ste rgya mtsho'i gru gzings dang/ mkham mchog bsod grags kyis 'dzug pa'i 'dus pa'i rdzogs rim dang/ mkhas pa thang sag pa rin po ches mdzad pa'i 'dus pa'i rdzogs rim dang/ re khrod pa ngag dbang byams pas mdzad pa'i bde mchog rdzogs rim sogs la blta rtog zhib par byas te rigs pas kyang yang yang dpyad pa la brten nas/ /blo gros kyi snang ba cung zheg rnyeng tshe/ rje 'jam dbyangs bzhad pa rin po che'i rdzogs rim la gzhir byas shing/ gong du smos pa'i legs bshad de dag gis legs par brgyan te sbyar ba'i yi ge pa ni chos la blo mig rgyas pa gu shrI chos rgya mtsho dang/ rnam dpyod shin tu yangs pa'i gu shri ngag dbang dar rgyas gnyis so/ /'dis kyang rgyal ba tsong kha pa'i bstan pa rin po che dar zhing rgyas la yun ring du gnas par gyur cig /dza yan+tu/ sarba mang+ga laM// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7.5" ;
        bdo:contentLocation bdr:CLMW1NLM5181_O1NLM5181_002_001 ;
        bdo:contentWidth "39" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "44.5" ;
        bdo:hasTitle bdr:TTMW1NLM5181_O1NLM5181_002_001 ;
        bdo:inRootInstance bdr:MW1NLM5181 ;
        bdo:paginationExtentStatement "1a-58a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM5181 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dpal rdo rje 'jigs byed kyi zab lam rim pa gnyis pa rdzogs rim sku gsum them skas/"@bo-x-ewts .

    bdr:O1NLM5181 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM5181 .

    bdr:TTMW1NLM5181_O1NLM5181_001_001 a bdo:TitlePageTitle ;
        rdfs:label "rgyud kyi rgyal po dpal gsang ba 'dus pa'i rim gnyis rgyud 'grel dang bcas pa bshad thabs kyi gsung rgyun man ngag rdo lag brgyud ma/"@bo-x-ewts .

    bdr:TTMW1NLM5181_O1NLM5181_002_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal rdo rje 'jigs byed kyi zab lam rim pa gnyis pa rdzogs rim sku gsum them skas/"@bo-x-ewts .

    bdr:MW1NLM5181 bdo:hasPart bdr:MW1NLM5181_O1NLM5181_001,
            bdr:MW1NLM5181_O1NLM5181_002 .
}

