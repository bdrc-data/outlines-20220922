@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM2002 {
    bda:O1NLM2002 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM2002 ;
        adm:gitPath "56/O1NLM2002.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM2002 ;
        adm:logEntry bda:LG0NLMOO1NLM2002_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM2002_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM2002_O1NLM2002_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 124 ;
        bdo:contentLocationInstance bdr:W1NLM2002 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM2002_O1NLM2002_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 218 ;
        bdo:contentLocationInstance bdr:W1NLM2002 ;
        bdo:contentLocationPage 125 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM2002_O1NLM2002_001_001 a bdr:NLMId ;
        rdf:value "M0059920-001" .

    bdr:IDMW1NLM2002_O1NLM2002_002_001 a bdr:NLMId ;
        rdf:value "M0059920-002" .

    bdr:MW1NLM2002_O1NLM2002_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM2002_O1NLM2002_001_001 ;
        bdo:authorshipStatement "shAkya thub pa/"@bo-x-ewts ;
        bdo:colophon "rgya gar gyi mkhan po dzi na mi dra dang/ dA na shI la dang/ shI len dra bo d+hi dang/ zhu chen gyi lo ts+ts+ha ba ban+de ye shes sdes bsgyur cing zhus te skad gsang bcad kyis kyang bcos nas gtan la phab pa'o// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM2002_O1NLM2002_001_001 ;
        bdo:contentWidth "17.5" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "22" ;
        bdo:hasTitle bdr:TTMW1NLM2002_O1NLM2002_001_001 ;
        bdo:inRootInstance bdr:MW1NLM2002 ;
        bdo:paginationExtentStatement "1a-62a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM2002 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "'phags pa de bzhin gshegs pa bdun gyi sngon gyi smon lam gyi khyad par rgyas pa zhes bya ba theg pa chen po'i mdo brgyad brgya ba/"@bo-x-ewts .

    bdr:MW1NLM2002_O1NLM2002_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM2002_O1NLM2002_002_001 ;
        bdo:authorshipStatement "byams pa mgon po/"@bo-x-ewts ;
        bdo:colophon "shes rab kyi pha rol tu phyin pa'i man ngag gi bstan bcos mngon par rtogs pa'i rgyan ces bya ba/ /rje btsun byams pa mgon pos mdzad pa rdzogs so/ /\\u0f38 rgya gar gyi paN+Di ta go mi 'chi med dang/ lo ts+ts+ha ba dge slong blo ldan shes rab kyis bsgyur cing zhus te legs par gtan la phab pa'o/ /\\u0f38 'di ltar gzhung mchog bris dang bar bsgrubs pa'i/ /dge ba 'dis mtshon skye 'phags ji snyed dge /bsdoms pa'i mthu las phyogs rnams thams cad du/ /lam bzang 'di nyid dar zhing rgyas phyir bsngo/ /de yi mthu las @#/ /mkha' mnyam mar gyur 'gro/ /theg mchog 'khor lo bzhi ldan rten thob nas/ /lung rtogs dam pa'i chos tshul kun bzang ste/ /bla med rdzogs pa'i byang chub myur thob shog /bla ma mchog gsum byin rlabs thugs rje dang/ /bdag cag lhag bsam dad mos chos dbyings mthus/ /gcig tu tshogs pa'i rten 'brel bden stobs kyis/ /don gnyis lhun gyis 'grub pa'i bkra shis shog / sarba mang+ga lam b+ha wan+tu// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5" ;
        bdo:contentLocation bdr:CLMW1NLM2002_O1NLM2002_002_001 ;
        bdo:contentWidth "18" ;
        bdo:dimHeight "7" ;
        bdo:dimWidth "22" ;
        bdo:hasTitle bdr:TTMW1NLM2002_O1NLM2002_002_001 ;
        bdo:inRootInstance bdr:MW1NLM2002 ;
        bdo:paginationExtentStatement "1a-47a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM2002 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "shes rab kyi pha rol tu phyin pa'i man ngag gi bstan bcos mngon par rtogs pa'i rgyan/"@bo-x-ewts .

    bdr:O1NLM2002 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM2002 .

    bdr:TTMW1NLM2002_O1NLM2002_001_001 a bdo:TitlePageTitle ;
        rdfs:label "'phags pa de bzhin gshegs pa bdun gyi sngon gyi smon lam gyi khyad par rgyas pa zhes bya ba theg pa chen po'i mdo brgyad brgya ba/"@bo-x-ewts .

    bdr:TTMW1NLM2002_O1NLM2002_002_001 a bdo:TitlePageTitle ;
        rdfs:label "shes rab kyi pha rol tu phyin pa'i man ngag gi bstan bcos mngon par rtogs pa'i rgyan/"@bo-x-ewts .

    bdr:MW1NLM2002 bdo:hasPart bdr:MW1NLM2002_O1NLM2002_001,
            bdr:MW1NLM2002_O1NLM2002_002 .
}

