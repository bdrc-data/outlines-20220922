@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM6305 {
    bda:O1NLM6305 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM6305 ;
        adm:gitPath "03/O1NLM6305.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM6305 ;
        adm:logEntry bda:LG0NLMOO1NLM6305_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM6305_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-09-21T17:14:22.784883"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM6305_O1NLM6305_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 154 ;
        bdo:contentLocationInstance bdr:W1NLM6305 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM6305_O1NLM6305_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 194 ;
        bdo:contentLocationInstance bdr:W1NLM6305 ;
        bdo:contentLocationPage 155 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM6305_O1NLM6305_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 250 ;
        bdo:contentLocationInstance bdr:W1NLM6305 ;
        bdo:contentLocationPage 195 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM6305_O1NLM6305_001_001 a bdr:NLMId ;
        rdf:value "M0064223-001" .

    bdr:IDMW1NLM6305_O1NLM6305_002_001 a bdr:NLMId ;
        rdf:value "M0064223-002" .

    bdr:IDMW1NLM6305_O1NLM6305_003_001 a bdr:NLMId ;
        rdf:value "M0064223-003" .

    bdr:MW1NLM6305_O1NLM6305_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM6305_O1NLM6305_001_001 ;
        bdo:authorshipStatement "bzhad pa'i rdo rje/"@bo-x-ewts ;
        bdo:colophon "ces bya ba 'di ni \\u0f38 btsun gzugs rab 'byams ming 'dzin dge slong ye shes rgya mtsho'am ming gzhan tshangs sras bzhad pa'i rdo rjes nag 'gros su bkod pa'o/ /'dis kyang rgyal ba'i bstan pa yul dus gnas skabs thams cad du dar zhing rgyas par gyur cig / //oM swa sti/ rmad byung bshad sgrub bstan pa'i 'byung gnas mchog /'gyur med bkra shis dpal ldan bsod nams zhing/ /bstan 'gror phan bde'i bsil ldan chos lding nas/ /mthun rkyen legs par sbyar te par du bsgrubs/ /'di la rgyu rkyen sbyar ba'i yon bdag dang/ /zhu dag do dam mdzad pa'i dge bshes rnams/ /dge 'dis thos bsam sgom pa mthar phyin te/ /sku gsum go 'phang myur du thob gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM6305_O1NLM6305_001_001 ;
        bdo:contentWidth "46.5" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "51" ;
        bdo:hasTitle bdr:TTMW1NLM6305_O1NLM6305_001_001 ;
        bdo:inRootInstance bdr:MW1NLM6305 ;
        bdo:paginationExtentStatement "1a-77b" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM6305 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "blo rigs kyi spyi don mdor bsdus dogs dpyod dang bcas pa'i rnam gzhag lung rigs rin chen gter mdzod/"@bo-x-ewts .

    bdr:MW1NLM6305_O1NLM6305_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM6305_O1NLM6305_002_001 ;
        bdo:authorshipStatement "bzhad pa'i rdo rje/"@bo-x-ewts ;
        bdo:colophon "zhes bya 'di ni \\u0f38 btsun gzugs rab 'byams ming 'dzin dge slong ye shes rgya mtsho'am \\u0f38 ming gzhan tshangs sras bzhad pa'i rdo rje'am \\u0f38 'jam dbyangs dgyis pa'i lang tsho zhes grags pas dam pa gong ma'i gzhung tshad ldan du ma bsdud nas gang legs pa'i cha rnams phyogs gcig tu bkod pa 'dis kyang rgyal ba'i bstan pa rin po che phyogs dus gnas skabs thams cad du dar zhing rgyas par gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM6305_O1NLM6305_002_001 ;
        bdo:contentWidth "46.5" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "51" ;
        bdo:hasTitle bdr:TTMW1NLM6305_O1NLM6305_002_001 ;
        bdo:inRootInstance bdr:MW1NLM6305 ;
        bdo:paginationExtentStatement "1a-20b" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM6305 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "sgrub ngag gi rnam gzhag lung rigs rin chen gter mdzod/"@bo-x-ewts .

    bdr:MW1NLM6305_O1NLM6305_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM6305_O1NLM6305_003_001 ;
        bdo:authorshipStatement "kun dga' rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "tshad ma rigs pa'i gter zhes bya ba'i bstan bcos 'di ni/ yul byang phyogs kyi rgyud du byung ba/ sgra dang/ tshad ma dang/ sde brgyad dang/ snyan ngag dang/ tshig gi rgyan dang/ ming gi mngon brjod dang/ tha snyad kyi gtsug lag rnams legs par shes pas/ 'chang ba dang/ rtsom pa dang/ rtsod  pa'i tshul la spobs pa dge ba can/ kun las btus dang/ sde bdun phyin ci ma log par khong du chud cing/ lung dang rigs pa dang/ man ngag gi tshul mtha' dag la blo'i snang ba thob pa/ shAkya'i dge slong kun dga' rgyal mtshan dpal bzang po zhes bya bas sde ba sbyor sbangs nas go bde bar bkrol te/ dpal sa skya'i gtsug lag khang du sbyar ba'o/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM6305_O1NLM6305_003_001 ;
        bdo:contentWidth "46.5" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "51" ;
        bdo:hasTitle bdr:TTMW1NLM6305_O1NLM6305_003_001 ;
        bdo:inRootInstance bdr:MW1NLM6305 ;
        bdo:paginationExtentStatement "1a-28a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM6305 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 3 ;
        skos:prefLabel "phyogs las rnam rgyal chen po 'jam mgon sa skya paN+Di ta kun dga' rgyal mtshan dpal bzang pos mdzad pa'i tshad ma'i rigs pa'i gter/"@bo-x-ewts .

    bdr:O1NLM6305 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM6305 .

    bdr:TTMW1NLM6305_O1NLM6305_001_001 a bdo:TitlePageTitle ;
        rdfs:label "blo rigs kyi spyi don mdor bsdus dogs dpyod dang bcas pa'i rnam gzhag lung rigs rin chen gter mdzod/"@bo-x-ewts .

    bdr:TTMW1NLM6305_O1NLM6305_002_001 a bdo:TitlePageTitle ;
        rdfs:label "sgrub ngag gi rnam gzhag lung rigs rin chen gter mdzod/"@bo-x-ewts .

    bdr:TTMW1NLM6305_O1NLM6305_003_001 a bdo:TitlePageTitle ;
        rdfs:label "phyogs las rnam rgyal chen po 'jam mgon sa skya paN+Di ta kun dga' rgyal mtshan dpal bzang pos mdzad pa'i tshad ma'i rigs pa'i gter/"@bo-x-ewts .

    bdr:MW1NLM6305 bdo:hasPart bdr:MW1NLM6305_O1NLM6305_001,
            bdr:MW1NLM6305_O1NLM6305_002,
            bdr:MW1NLM6305_O1NLM6305_003 .
}

