@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM6745 {
    bda:LG0NLMOO1NLM6745_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM6745  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM6745 ;
        adm:gitPath         "81/O1NLM6745.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM6745 ;
        adm:logEntry        bda:LG0NLMOO1NLM6745_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM6745_O1NLM6745_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  150 ;
        bdo:contentLocationInstance  bdr:W1NLM6745 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM6745_O1NLM6745_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  252 ;
        bdo:contentLocationInstance  bdr:W1NLM6745 ;
        bdo:contentLocationPage  151 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM6745_O1NLM6745_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0064663-001" .
    
    bdr:IDMW1NLM6745_O1NLM6745_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0064663-002" .
    
    bdr:MW1NLM6745  bdo:hasPart  bdr:MW1NLM6745_O1NLM6745_001 , bdr:MW1NLM6745_O1NLM6745_002 .
    
    bdr:MW1NLM6745_O1NLM6745_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM6745_O1NLM6745_001_001 ;
        bdo:colophon        "ces sman bla'i mdo chog yid /bzhin dbang rgyal gyi 'don cha zur du bkol nas spar du grub pa'i kha byang lta bu zhig dgos zhes tA bla ma dar han em chi 'jam dpal dbyangs rgyal mtshan gyis bskul ba bzhin/ / //\\u0f38lcang skya rol pa'i rdo rjes sbyar ba dge legs su gyur cig / bkra shis par gyur cig / sarba mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade  3 ;
        bdo:contentHeight   "8" ;
        bdo:contentLocation  bdr:CLMW1NLM6745_O1NLM6745_001_001 ;
        bdo:contentWidth    "26.5" ;
        bdo:dimHeight       "11" ;
        bdo:dimWidth        "35.5" ;
        bdo:hasTitle        bdr:TTMW1NLM6745_O1NLM6745_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM6745 ;
        bdo:paginationExtentStatement  "1a-75a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM6745 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "sman bla'i mdo chog 'don zur du bkol ba yid bzhin dbang rgyal/"@bo-x-ewts .
    
    bdr:MW1NLM6745_O1NLM6745_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM6745_O1NLM6745_002_001 ;
        bdo:conditionGrade  3 ;
        bdo:contentHeight   "9" ;
        bdo:contentLocation  bdr:CLMW1NLM6745_O1NLM6745_002_001 ;
        bdo:contentWidth    "30" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "35" ;
        bdo:hasTitle        bdr:TTMW1NLM6745_O1NLM6745_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM6745 ;
        bdo:paginationExtentStatement  "1a-51a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM6745 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  3 ;
        skos:prefLabel      "sman bla'i mdo chog 'don zur du bkol ba yid bzhin dbang rgyal/"@bo-x-ewts .
    
    bdr:O1NLM6745  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM6745 .
    
    bdr:TTMW1NLM6745_O1NLM6745_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "sman bla'i mdo chog 'don zur du bkol ba yid bzhin dbang rgyal/"@bo-x-ewts .
    
    bdr:TTMW1NLM6745_O1NLM6745_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "sman bla'i mdo chog 'don zur du bkol ba yid bzhin dbang rgyal/"@bo-x-ewts .
}
