@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM18 {
    bda:O1NLM18 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM18 ;
        adm:gitPath "86/O1NLM18.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM18 ;
        adm:logEntry bda:LG0NLMOO1NLM18_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM18_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM18_O1NLM18_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 9 ;
        bdo:contentLocationInstance bdr:W1NLM18 ;
        bdo:contentLocationPage 2 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM18_O1NLM18_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 83 ;
        bdo:contentLocationInstance bdr:W1NLM18 ;
        bdo:contentLocationPage 10 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM18_O1NLM18_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 168 ;
        bdo:contentLocationInstance bdr:W1NLM18 ;
        bdo:contentLocationPage 84 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM18_O1NLM18_001_001 a bdr:NLMId ;
        rdf:value "M0057912-001" .

    bdr:IDMW1NLM18_O1NLM18_002_001 a bdr:NLMId ;
        rdf:value "M0057912-002" .

    bdr:IDMW1NLM18_O1NLM18_003_001 a bdr:NLMId ;
        rdf:value "M0057912-003" .

    bdr:MW1NLM18_O1NLM18_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM18_O1NLM18_001_001 ;
        bdo:authorshipStatement "blo bzang 'phrin las/"@bo-x-ewts ;
        bdo:colophon "ces khyab bdag rdo rje 'chang dbang snyegs ma'i dus su ngur smrig 'dzin pa'i zlos gar bsgyur ba rdo rje 'chang chen po dkon mchog rgyal mtshan dpal bzang po'i gsol 'debs thugs rje myur 'jug ces bya ba 'di ni rje nyid kyi sras mchog bstan pa'i gsal khyer mkhan chen chos kyi rgyal po tho bzang bstan 'dzin rgyal mtshan dpal bzang po'i lta gsal sgram zho bzhi'i gnad rten dang bcas bkas bskal ba dang nyid kyang rje bla ma la'i bstod pa ni rdo rje 'chang la bskal brgyar bstod ba las kyang ban yon che bar gsungs ba'i don yid la bsams te rdo rje 'chang chen po de nyid kyi slob pa'i tha shal dza ya paN+Di ta sku blo bzang 'phrin las kyis sug bres su bgyis pa 'i dpal ldan bla ma dam pas chang bas thams cad du rjes su 'dzin par gyur cig// /'dis kyang rgyal ba'i bstan pa rin po che phyogs dus kun tu dar zhing rgyas la yun ring du gnas par gyur cig// /bkra shis bar dge ba dge 'phel gyur cig/"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM18_O1NLM18_001_001 ;
        bdo:contentWidth "31.5" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "36" ;
        bdo:hasTitle bdr:TTMW1NLM18_O1NLM18_001_001 ;
        bdo:inRootInstance bdr:MW1NLM18 ;
        bdo:paginationExtentStatement "1a-4b" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM18 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 2 ;
        skos:prefLabel "gsol 'debs thugs rje myur 'jug bcas bya ba/"@bo-x-ewts .

    bdr:MW1NLM18_O1NLM18_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM18_O1NLM18_002_001 ;
        bdo:authorshipStatement "blo bzang grags pa/"@bo-x-ewts ;
        bdo:colophon "ces bcom ldan 'das rdo rje 'jigs byed lha bcu gsum ma'i sgrub pa'i thabs/ rin po che'i za ma tog ces bya ba 'di bla ma chen dam pa mang du thos pa mkhan chen mkho bor ngag dbang grags pas shar rgyal mo dad nas bskul ba'i don du/ shin rje'i gshed kyi rnal 'byor ba shar tsong kha pa blo bzang grags pa'i dpal gyi sbyar ba'o// yi ge pa ni rdo rje rin chen rnams pas rin po che'i za ma tog du gsungs pa rnam mchang ba dpal tha sha dang bsres pa'o/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM18_O1NLM18_002_001 ;
        bdo:contentWidth "31.5" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "36" ;
        bdo:hasTitle bdr:TTMW1NLM18_O1NLM18_002_001 ;
        bdo:inRootInstance bdr:MW1NLM18 ;
        bdo:paginationExtentStatement "1a-37a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM18 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 2 ;
        skos:prefLabel "dpal rdo rje 'jigs byed lha bcu gsum ma'i sgrub pa'i thabs rin po che'i za ma tog"@bo-x-ewts .

    bdr:MW1NLM18_O1NLM18_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM18_O1NLM18_003_001 ;
        bdo:colophon "zhes pa 'di ni rgyal dbang thams cad mkhyen pa snyan ngags dam pa yig du sbyar ba'o// //dge ba 'phel 'gyur cig/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM18_O1NLM18_003_001 ;
        bdo:contentWidth "31.5" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "36" ;
        bdo:hasTitle bdr:TTMW1NLM18_O1NLM18_003_001 ;
        bdo:inRootInstance bdr:MW1NLM18 ;
        bdo:paginationExtentStatement "1a-42a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM18 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 2 ;
        skos:prefLabel "'dod khams dbang phyug ma dmag zor rgyal mo'i sgrub thabs gtor chog"@bo-x-ewts .

    bdr:O1NLM18 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM18 .

    bdr:TTMW1NLM18_O1NLM18_001_001 a bdo:TitlePageTitle ;
        rdfs:label "gsol 'debs thugs rje myur 'jug bcas bya ba/"@bo-x-ewts .

    bdr:TTMW1NLM18_O1NLM18_002_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal rdo rje 'jigs byed lha bcu gsum ma'i sgrub pa'i thabs rin po che'i za ma tog"@bo-x-ewts .

    bdr:TTMW1NLM18_O1NLM18_003_001 a bdo:TitlePageTitle ;
        rdfs:label "'dod khams dbang phyug ma dmag zor rgyal mo'i sgrub thabs gtor chog"@bo-x-ewts .

    bdr:MW1NLM18 bdo:hasPart bdr:MW1NLM18_O1NLM18_001,
            bdr:MW1NLM18_O1NLM18_002,
            bdr:MW1NLM18_O1NLM18_003 .
}

