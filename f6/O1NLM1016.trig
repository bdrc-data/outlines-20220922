@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM1016 {
    bda:O1NLM1016 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM1016 ;
        adm:gitPath "f6/O1NLM1016.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM1016 ;
        adm:logEntry bda:LG0NLMOO1NLM1016_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM1016_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM1016_O1NLM1016_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 538 ;
        bdo:contentLocationInstance bdr:W1NLM1016 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM1016_O1NLM1016_001B_001 a bdr:NLMId ;
        rdf:value "M0058930-001" .

    bdr:IDMW1NLM1016_O1NLM1016_001_001 a bdr:NLMId ;
        rdf:value "M0058929-001" .

    bdr:MW1NLM1016_O1NLM1016_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1016_O1NLM1016_001_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7.5" ;
        bdo:contentLocation bdr:CLMW1NLM1016_O1NLM1016_001_001 ;
        bdo:contentWidth "50" ;
        bdo:dimHeight "11.5" ;
        bdo:dimWidth "64" ;
        bdo:hasTitle bdr:TTMW1NLM1016_O1NLM1016_001_001 ;
        bdo:inRootInstance bdr:MW1NLM1016 ;
        bdo:paginationExtentStatement "1a-268a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM1016 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "tshad ma rnam 'grel gyi le'u dang po'i mtha' dpyod blo gsal mgul rgyan skal bzang 'jug ngog zhes bya ba yongs 'dzin ngag gi dbang po'i gsung /"@bo-x-ewts .

    bdr:MW1NLM1016_O1NLM1016_001B a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1016_O1NLM1016_001B_001 ;
        bdo:authorshipStatement "ngag dbang blo bzang chos ldan/"@bo-x-ewts ;
        bdo:colophon "zhes pa @#/ /'di ni khyab bdag rdo rje 'chang dang dbyer ma mchis pa'i rje btsun dam pa ngag dbang 'phrin las lhun grub pa'i zhal snga nas kyis blo ldan don gnyer can gyi slob bu'i tshogs mang po la byang chub lam gyi rim pa'i nyams khrid zab mo 'phags pa 'jig rten dbang phyug ngur smrig 'dzin pa'i gar gyi rnam par rol pa gangs can 'gro ba'i mgon po ngag dbang blo bzang rgya mtsho 'jigs med go cha thub bstan lang tsho'i sde phyogs thams cad las rnam par rgyal ba'i gsung gi nying lu 'jam dpal zhal lung rje bdag nyid chen po'i byang chub lam rim chung bas brgyan te lam rim las/ blo'i mthu chung na lung 'dren dor nas skabs su gang babs pa'i don gyi ngo bo tsam bskyang bar bya'o/ /zhes gsungs pa ltar zhug po rnams kyi blo dang 'tshams par lung 'dren sogs kyi spros pa dor nas don gyi ngo bo'i dmigs rim rgyas par bstsal pa'i tshe zin bris su 'bebs par gsung gi gnang ba thob pa la brten nas gyi na ba ngag dbang blo bzang chos ldan gyis bris pa 'dis bdag sogs sems can thams cad lam 'di la brten nas myur du rdzogs pa'i sangs rgyas kyi go 'phang thob par gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentWidth "47.5" ;
        bdo:dimHeight "11" ;
        bdo:dimWidth "55" ;
        bdo:hasTitle bdr:TTMW1NLM1016_O1NLM1016_001B_001 ;
        bdo:inRootInstance bdr:MW1NLM1016 ;
        bdo:note bdr:NTMW1NLM1016_O1NLM1016_001BCL ;
        bdo:paginationExtentStatement "1a-49a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM1016 ;
        bdo:partTreeIndex "001B" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "lam rim kyi khrid kyi zin bris/"@bo-x-ewts .

    bdr:NTMW1NLM1016_O1NLM1016_001BCL a bdo:Note ;
        bdo:noteText "Title present in the catalog but not digitized"@en .

    bdr:O1NLM1016 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM1016 .

    bdr:TTMW1NLM1016_O1NLM1016_001B_001 a bdo:TitlePageTitle ;
        rdfs:label "lam rim kyi khrid kyi zin bris/"@bo-x-ewts .

    bdr:TTMW1NLM1016_O1NLM1016_001_001 a bdo:TitlePageTitle ;
        rdfs:label "tshad ma rnam 'grel gyi le'u dang po'i mtha' dpyod blo gsal mgul rgyan skal bzang 'jug ngog zhes bya ba yongs 'dzin ngag gi dbang po'i gsung*/"@bo-x-ewts .

    bdr:MW1NLM1016 bdo:hasPart bdr:MW1NLM1016_O1NLM1016_001,
            bdr:MW1NLM1016_O1NLM1016_001B .
}

