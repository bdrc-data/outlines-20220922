@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM63 {
    bda:O1NLM63 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM63 ;
        adm:gitPath "13/O1NLM63.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM63 ;
        adm:logEntry bda:LG0NLMOO1NLM63_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM63_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM63_O1NLM63_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 70 ;
        bdo:contentLocationInstance bdr:W1NLM63 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM63_O1NLM63_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 254 ;
        bdo:contentLocationInstance bdr:W1NLM63 ;
        bdo:contentLocationPage 71 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM63_O1NLM63_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 266 ;
        bdo:contentLocationInstance bdr:W1NLM63 ;
        bdo:contentLocationPage 255 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM63_O1NLM63_004_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 298 ;
        bdo:contentLocationInstance bdr:W1NLM63 ;
        bdo:contentLocationPage 267 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM63_O1NLM63_001_001 a bdr:NLMId ;
        rdf:value "M0057976-001" .

    bdr:IDMW1NLM63_O1NLM63_002_001 a bdr:NLMId ;
        rdf:value "M0057976-002" .

    bdr:IDMW1NLM63_O1NLM63_003_001 a bdr:NLMId ;
        rdf:value "M0057976-003" .

    bdr:IDMW1NLM63_O1NLM63_004_001 a bdr:NLMId ;
        rdf:value "M0057976-004" .

    bdr:MW1NLM63_O1NLM63_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM63_O1NLM63_001_001 ;
        bdo:authorshipStatement "blo bzang bstan 'dzin rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "zhes pa 'di ni dad ldan gyi dge slong kun dga' rgyal mtshan pas lam rim gyi don go sla zhing tshig nyung don tshang gcig nges par dgos zhes dad pa'i me tog dang bcas te bskul ba'i ngor/ rgyal ba yab sras gnyis kyi zhabs rdul spyi bor len cing gsung gi bdud rtsi myong ba'i lha'i btsun pa \\u0f38blo bzang bstan 'dzin rgyal mtshan gyis sbyar ba'i dge bas rgyal ba gnyis pa'i bstan pa dar zhing rgyas la 'gro ba thams cad kyi nyon mongs @#/ /pa'i gsos sman du gyur cig// //phan bde'i 'byung gnas rgyal bstan yongs 'dus ljon/ /dab rgyas 'gro kun thar mchog 'bras bzang la/ /spyod phyir tshe 'phel gling gi chos grwa cher/ /chos sbyin 'dzad med dal 'gro'i rgyun 'di spel/ /sarba mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM63_O1NLM63_001_001 ;
        bdo:contentWidth "17" ;
        bdo:dimHeight "7" ;
        bdo:dimWidth "20" ;
        bdo:hasTitle bdr:TTMW1NLM63_O1NLM63_001_001 ;
        bdo:inRootInstance bdr:MW1NLM63 ;
        bdo:paginationExtentStatement "1a-35a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM63 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "mkhan chen chos kyi rgyal po'i gsung 'bum las byang chub lam rim gyi 'khrid blo yi mun sel/"@bo-x-ewts .

    bdr:MW1NLM63_O1NLM63_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM63_O1NLM63_002_001 ;
        bdo:colophon "par 'di 'bras spung gling khams tshan du bzhugs/ / mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM63_O1NLM63_002_001 ;
        bdo:contentWidth "18.5" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "22" ;
        bdo:hasTitle bdr:TTMW1NLM63_O1NLM63_002_001 ;
        bdo:inRootInstance bdr:MW1NLM63 ;
        bdo:paginationExtentStatement "1a-92b inc [*missing page from 3a-3b, 65a-66b*]" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM63 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 3 ;
        bdo:sourcePrinteryStatement "bras spung gling/"@bo-x-ewts ;
        skos:prefLabel "bde gshegs bdun gyi mchod pa'i cho ga sgrigs yid bzhin dbang rgyal/"@bo-x-ewts .

    bdr:MW1NLM63_O1NLM63_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM63_O1NLM63_003_001 ;
        bdo:conditionGrade 2 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM63_O1NLM63_003_001 ;
        bdo:contentWidth "21.5" ;
        bdo:dimHeight "7.5" ;
        bdo:dimWidth "23" ;
        bdo:hasTitle bdr:TTMW1NLM63_O1NLM63_003_001 ;
        bdo:inRootInstance bdr:MW1NLM63 ;
        bdo:paginationExtentStatement "1a-4b" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM63 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bstan 'bar ma/"@bo-x-ewts .

    bdr:MW1NLM63_O1NLM63_004 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM63_O1NLM63_004_001 ;
        bdo:authorshipStatement "dkon mchog bstan pa'i sgron me/"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM63_O1NLM63_004_001 ;
        bdo:contentWidth "18" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "22.5" ;
        bdo:hasTitle bdr:TTMW1NLM63_O1NLM63_004_001 ;
        bdo:inRootInstance bdr:MW1NLM63 ;
        bdo:paginationExtentStatement "3a-18a inc [*missing page from 1a-2b*]" ;
        bdo:partIndex 4 ;
        bdo:partOf bdr:MW1NLM63 ;
        bdo:partTreeIndex "004" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dga' ldan lha brgya mar grags pa'i bla ma'i rnal 'byor gyi gzhung bsrangs te mdo sngags cha tshad bar nyams su len pa'i tshul dga' ldan zhing du bgrod pa'i nor bu'i them skas/"@bo-x-ewts .

    bdr:O1NLM63 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM63 .

    bdr:TTMW1NLM63_O1NLM63_001_001 a bdo:TitlePageTitle ;
        rdfs:label "mkhan chen chos kyi rgyal po'i gsung 'bum las byang chub lam rim gyi 'khrid blo yi mun sel/"@bo-x-ewts .

    bdr:TTMW1NLM63_O1NLM63_002_001 a bdo:TitlePageTitle ;
        rdfs:label "bde gshegs bdun gyi mchod pa'i cho ga sgrigs yid bzhin dbang rgyal/"@bo-x-ewts .

    bdr:TTMW1NLM63_O1NLM63_003_001 a bdo:TitlePageTitle ;
        rdfs:label "bstan 'bar ma/"@bo-x-ewts .

    bdr:TTMW1NLM63_O1NLM63_004_001 a bdo:TitlePageTitle ;
        rdfs:label "dga' ldan lha brgya mar grags pa'i bla ma'i rnal 'byor gyi gzhung bsrangs te mdo sngags cha tshad bar nyams su len pa'i tshul dga' ldan zhing du bgrod pa'i nor bu'i them skas/"@bo-x-ewts .

    bdr:MW1NLM63 bdo:hasPart bdr:MW1NLM63_O1NLM63_001,
            bdr:MW1NLM63_O1NLM63_002,
            bdr:MW1NLM63_O1NLM63_003,
            bdr:MW1NLM63_O1NLM63_004 .
}

