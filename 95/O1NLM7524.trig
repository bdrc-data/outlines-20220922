@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7524 {
    bda:LG0NLMOO1NLM7524_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7524  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7524 ;
        adm:gitPath         "95/O1NLM7524.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7524 ;
        adm:logEntry        bda:LG0NLMOO1NLM7524_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7524_O1NLM7524_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  212 ;
        bdo:contentLocationInstance  bdr:W1NLM7524 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7524_O1NLM7524_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  310 ;
        bdo:contentLocationInstance  bdr:W1NLM7524 ;
        bdo:contentLocationPage  213 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7524_O1NLM7524_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  334 ;
        bdo:contentLocationInstance  bdr:W1NLM7524 ;
        bdo:contentLocationPage  311 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7524_O1NLM7524_004_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  338 ;
        bdo:contentLocationInstance  bdr:W1NLM7524 ;
        bdo:contentLocationPage  335 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7524_O1NLM7524_005_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  342 ;
        bdo:contentLocationInstance  bdr:W1NLM7524 ;
        bdo:contentLocationPage  339 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7524_O1NLM7524_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0065442-001" .
    
    bdr:IDMW1NLM7524_O1NLM7524_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0065442-002" .
    
    bdr:IDMW1NLM7524_O1NLM7524_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0065442-003" .
    
    bdr:IDMW1NLM7524_O1NLM7524_004_001
        a                   bdr:NLMId ;
        rdf:value           "M0065442-004" .
    
    bdr:IDMW1NLM7524_O1NLM7524_005_001
        a                   bdr:NLMId ;
        rdf:value           "M0065442-005" .
    
    bdr:MW1NLM7524  bdo:hasPart  bdr:MW1NLM7524_O1NLM7524_001 , bdr:MW1NLM7524_O1NLM7524_002 , bdr:MW1NLM7524_O1NLM7524_003 , bdr:MW1NLM7524_O1NLM7524_004 , bdr:MW1NLM7524_O1NLM7524_005 .
    
    bdr:MW1NLM7524_O1NLM7524_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7524_O1NLM7524_001_001 ;
        bdo:authorshipStatement  "jam dbyangs bzhad pa'i rdo rje/"@bo-x-ewts ;
        bdo:colophon        "blo smon legs par dkar ba'i lhag bsam gyis/ /bzang po'i bstan dang 'jal ba'i dus 'di ru/ /snyin pa'i legs bshad mchog la rtag rol zhing/ /grags pa'i ring lugs 'dzin pa'i mchog gyur cig /'dis kyng thub pa'i lung rtogs kyi bstan pa rin po che spyi dang khyad par rgyal ba 'jam dpal snying po'i mdo sngags kyi bstan pa dang/ kun mkhyen bla ma 'jam dbyangs bzhad pa'i rdo rje'i bshad sgrub kyi bstan pa mi nyams par phyogs kun tu dar zhing rgyas par 'gyur zhing 'gro ba'i don kyang byed nus par gyur cig /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7524_O1NLM7524_001_001 ;
        bdo:contentWidth    "55" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "61" ;
        bdo:hasTitle        bdr:TTMW1NLM7524_O1NLM7524_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7524 ;
        bdo:paginationExtentStatement  "1a-106a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7524 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "shes rab kyi pha rol tu phyin pa'i man ngag gi bstan bcos mngon par rtogs pa'i rgyan gyi skabs dang po'i mtha' dpyod las gcig tu bral gyi rnam bzhag legs par bshad pa yang dag blo gros mig 'byed/"@bo-x-ewts .
    
    bdr:MW1NLM7524_O1NLM7524_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7524_O1NLM7524_002_001 ;
        bdo:colophon        "oM swa sti/ rgyal bstan pad tshal phan bde'i dri bsung can/ /bshad sgrub 'dab rgyas rnam grol sbrang rtsi'i bcud/ /'gro kun spyod phyir sku 'bum byams pa gling/ /chos sde che nas chos sbyin char 'di spel/ /sarba mang+ga laM// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7524_O1NLM7524_002_001 ;
        bdo:contentWidth    "46" ;
        bdo:dimHeight       "9.5" ;
        bdo:dimWidth        "54.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7524_O1NLM7524_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7524 ;
        bdo:paginationExtentStatement  "1a-49a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7524 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "rim lnga'i gsal byed mkhas grub rjes snga rting zin bris su stsal ba rnams phyogs gcig tu sdebs pa/"@bo-x-ewts .
    
    bdr:MW1NLM7524_O1NLM7524_003
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7524_O1NLM7524_003_001 ;
        bdo:authorshipStatement  "bshad sgrub ming can/"@bo-x-ewts ;
        bdo:colophon        "ces 'dul ba rgya mtsho'i snying po'i don gyi sdom pa'i mtha' gcod ces bya ba 'di yang shAkya'i dge slong bshad sgrub ming can gyis bgyis pa ste/ yi ge pa ni rnam dpyod ldan pa'i sngags ram pa kun dga' chos 'byor ro/ /mang+ga laM/ /dge legs 'phel lo/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM7524_O1NLM7524_003_001 ;
        bdo:contentWidth    "49" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "54" ;
        bdo:hasTitle        bdr:TTMW1NLM7524_O1NLM7524_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM7524 ;
        bdo:paginationExtentStatement  "1a-12a" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM7524 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "'dul ba rgya mtsho'i snying po'i don gyi sdom pa'i mtha' gcod gser gyi spu gri/"@bo-x-ewts .
    
    bdr:MW1NLM7524_O1NLM7524_004
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7524_O1NLM7524_004_001 ;
        bdo:authorshipStatement  "dge 'dun grub pa/"@bo-x-ewts ;
        bdo:colophon        "ces pa 'di ni thams cad mkhyen pa chen po dge 'dun grub pa dpal bzang pos gdung dbyangs su mdzad pa'o/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM7524_O1NLM7524_004_001 ;
        bdo:contentWidth    "45" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "55" ;
        bdo:hasTitle        bdr:TTMW1NLM7524_O1NLM7524_004_001 ;
        bdo:inRootInstance  bdr:MW1NLM7524 ;
        bdo:paginationExtentStatement  "1a-2b" ;
        bdo:partIndex       4 ;
        bdo:partOf          bdr:MW1NLM7524 ;
        bdo:partTreeIndex   "004" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "shar gangs rim dang bde chen lhun grub ma gnyis/"@bo-x-ewts .
    
    bdr:MW1NLM7524_O1NLM7524_005
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7524_O1NLM7524_005_001 ;
        bdo:authorshipStatement  "rje blo bzang grags pa/"@bo-x-ewts ;
        bdo:colophon        "zhes rje btsun byambs mgon po la bstod pa'i tshigs su bcad pa 'di ni rgyal khams pa blo bzang grags pa'i dpal gyis chos grwa chen po skyor mo lung du sbyar ba dge legs su gyur cig / ces rje btsun byams mgon bstod pa yid bzhin nor bu zhes bya ba 'di'ang shA ka btsun bshad sgrub ming can gyis dad pa kho nas sbyar ba'o/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM7524_O1NLM7524_005_001 ;
        bdo:contentWidth    "45.5" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "55" ;
        bdo:hasTitle        bdr:TTMW1NLM7524_O1NLM7524_005_001 ;
        bdo:inRootInstance  bdr:MW1NLM7524 ;
        bdo:paginationExtentStatement  "1a-2b" ;
        bdo:partIndex       5 ;
        bdo:partOf          bdr:MW1NLM7524 ;
        bdo:partTreeIndex   "005" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "rje btsun byams bstod zung gcig"@bo-x-ewts .
    
    bdr:O1NLM7524  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7524 .
    
    bdr:TTMW1NLM7524_O1NLM7524_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "shes rab kyi pha rol tu phyin pa'i man ngag gi bstan bcos mngon par rtogs pa'i rgyan gyi skabs dang po'i mtha' dpyod las gcig tu bral gyi rnam bzhag legs par bshad pa yang dag blo gros mig 'byed/"@bo-x-ewts .
    
    bdr:TTMW1NLM7524_O1NLM7524_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rim lnga'i gsal byed mkhas grub rjes snga rting zin bris su stsal ba rnams phyogs gcig tu sdebs pa/"@bo-x-ewts .
    
    bdr:TTMW1NLM7524_O1NLM7524_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "'dul ba rgya mtsho'i snying po'i don gyi sdom pa'i mtha' gcod gser gyi spu gri/"@bo-x-ewts .
    
    bdr:TTMW1NLM7524_O1NLM7524_004_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "shar gangs rim dang bde chen lhun grub ma gnyis/"@bo-x-ewts .
    
    bdr:TTMW1NLM7524_O1NLM7524_005_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rje btsun byams bstod zung gcig"@bo-x-ewts .
}
