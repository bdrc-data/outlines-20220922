@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM590 {
    bda:O1NLM590 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM590 ;
        adm:gitPath "ea/O1NLM590.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM590 ;
        adm:logEntry bda:LG0NLMOO1NLM590_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM590_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM590_O1NLM590_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 6 ;
        bdo:contentLocationInstance bdr:W1NLM590 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM590_O1NLM590_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 108 ;
        bdo:contentLocationInstance bdr:W1NLM590 ;
        bdo:contentLocationPage 7 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM590_O1NLM590_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 114 ;
        bdo:contentLocationInstance bdr:W1NLM590 ;
        bdo:contentLocationPage 109 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM590_O1NLM590_001_001 a bdr:NLMId ;
        rdf:value "M0058503-001" .

    bdr:IDMW1NLM590_O1NLM590_002_001 a bdr:NLMId ;
        rdf:value "M0058503-002" .

    bdr:IDMW1NLM590_O1NLM590_003_001 a bdr:NLMId ;
        rdf:value "M0058503-003" .

    bdr:MW1NLM590_O1NLM590_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM590_O1NLM590_001_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM590_O1NLM590_001_001 ;
        bdo:contentWidth "25" ;
        bdo:dimHeight "7.5" ;
        bdo:dimWidth "32.5" ;
        bdo:hasTitle bdr:TTMW1NLM590_O1NLM590_001_001 ;
        bdo:inRootInstance bdr:MW1NLM590 ;
        bdo:paginationExtentStatement "1a-3b" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM590 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "smyung bar gnas pa'i cho ga'i bla brgyud gsol 'debs/"@bo-x-ewts .

    bdr:MW1NLM590_O1NLM590_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM590_O1NLM590_002_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM590_O1NLM590_002_001 ;
        bdo:contentWidth "25" ;
        bdo:dimHeight "7.5" ;
        bdo:dimWidth "32.5" ;
        bdo:hasTitle bdr:TTMW1NLM590_O1NLM590_002_001 ;
        bdo:inRootInstance bdr:MW1NLM590 ;
        bdo:paginationExtentStatement "1a-51a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM590 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "'phags pa thugs rje chen po zhal bcu gcig pa dpal mo'i lugs kyi sgrub thabs smyung bar gnas pa'i cho ga dang bcas pa phan bde'i snang ba gsar dngom/"@bo-x-ewts .

    bdr:MW1NLM590_O1NLM590_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM590_O1NLM590_003_001 ;
        bdo:authorshipStatement "su ma ti ma ni pra dz+nya/"@bo-x-ewts ;
        bdo:colophon "ces myur mdzad ye shes kyi mgon po phyag drug pa la gtor ma 'bul tshul bsdus pa bsam 'phel yid bzhin nor bu zhes bya ba 'di ni rnam dpyod spobs pa phul du byung ba'i er te ni shrI kun dga' don grub ces bya ba 'di lta bu bsdus pa zhig dgos zhes bskul ba la brten nas ye shes kyi mgon po phyag drug pa la yid ches pa'i dad pa dang ldan pa/ shAkya'i btsun pa su ma ti ma ni pra dz+nyas 'phral du bgyis pa'o/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM590_O1NLM590_003_001 ;
        bdo:contentWidth "25" ;
        bdo:dimHeight "7.5" ;
        bdo:dimWidth "32.5" ;
        bdo:hasTitle bdr:TTMW1NLM590_O1NLM590_003_001 ;
        bdo:inRootInstance bdr:MW1NLM590 ;
        bdo:paginationExtentStatement "1a-3b" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM590 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "myur mdzad ye shes kyi mgon po phyag drug pa la gtor ma 'bul ba'i tshul/"@bo-x-ewts .

    bdr:O1NLM590 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM590 .

    bdr:TTMW1NLM590_O1NLM590_001_001 a bdo:TitlePageTitle ;
        rdfs:label "smyung bar gnas pa'i cho ga'i bla brgyud gsol 'debs/"@bo-x-ewts .

    bdr:TTMW1NLM590_O1NLM590_002_001 a bdo:TitlePageTitle ;
        rdfs:label "'phags pa thugs rje chen po zhal bcu gcig pa dpal mo'i lugs kyi sgrub thabs smyung bar gnas pa'i cho ga dang bcas pa phan bde'i snang ba gsar dngom/"@bo-x-ewts .

    bdr:TTMW1NLM590_O1NLM590_003_001 a bdo:TitlePageTitle ;
        rdfs:label "myur mdzad ye shes kyi mgon po phyag drug pa la gtor ma 'bul ba'i tshul/"@bo-x-ewts .

    bdr:MW1NLM590 bdo:hasPart bdr:MW1NLM590_O1NLM590_001,
            bdr:MW1NLM590_O1NLM590_002,
            bdr:MW1NLM590_O1NLM590_003 .
}

