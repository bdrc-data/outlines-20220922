@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM1180 {
    bda:O1NLM1180 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM1180 ;
        adm:gitPath "ee/O1NLM1180.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM1180 ;
        adm:logEntry bda:LG0NLMOO1NLM1180_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM1180_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM1180_O1NLM1180_001B_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 56 ;
        bdo:contentLocationInstance bdr:W1NLM1180 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM1180_O1NLM1180_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 166 ;
        bdo:contentLocationInstance bdr:W1NLM1180 ;
        bdo:contentLocationPage 57 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM1180_O1NLM1180_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 274 ;
        bdo:contentLocationInstance bdr:W1NLM1180 ;
        bdo:contentLocationPage 167 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM1180_O1NLM1180_004_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 332 ;
        bdo:contentLocationInstance bdr:W1NLM1180 ;
        bdo:contentLocationPage 275 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM1180_O1NLM1180_001B_001 a bdr:NLMId ;
        rdf:value "M0059093-001" .

    bdr:IDMW1NLM1180_O1NLM1180_002_001 a bdr:NLMId ;
        rdf:value "M0059093-002" .

    bdr:IDMW1NLM1180_O1NLM1180_003_001 a bdr:NLMId ;
        rdf:value "M0059093-003" .

    bdr:IDMW1NLM1180_O1NLM1180_004_001 a bdr:NLMId ;
        rdf:value "M0059093-004" .

    bdr:MW1NLM1180_O1NLM1180_001B a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1180_O1NLM1180_001B_001 ;
        bdo:authorshipStatement "slob dpon dbyig gnyen/"@bo-x-ewts ;
        bdo:colophon "!om swa sti/ chos kun 'byung gnas khu rE chen mo ru/ /chos mngon gzhung 'di spar bskrun 'dis mtshon dges/ /chos tshul rnam kun 'phel zhing 'brel thogs sogs/ /chos srid dpal myong sangs rgyas myur thob shog/ //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM1180_O1NLM1180_001B_001 ;
        bdo:contentWidth "54" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "61" ;
        bdo:hasTitle bdr:TTMW1NLM1180_O1NLM1180_001B_001 ;
        bdo:inRootInstance bdr:MW1NLM1180 ;
        bdo:paginationExtentStatement "1a-28a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM1180 ;
        bdo:partTreeIndex "001B" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "khu rE chen mo/"@bo-x-ewts ;
        skos:prefLabel "chos mngon pa'i mdzod 'grel slob dpon dbyig gnyen rang gis mdzad pa las gnas dang po khams bstan pa/"@bo-x-ewts .

    bdr:MW1NLM1180_O1NLM1180_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1180_O1NLM1180_002_001 ;
        bdo:authorshipStatement "slob dpon dbyig gnyen/"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM1180_O1NLM1180_002_001 ;
        bdo:contentWidth "53.5" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "61.5" ;
        bdo:hasTitle bdr:TTMW1NLM1180_O1NLM1180_002_001 ;
        bdo:inRootInstance bdr:MW1NLM1180 ;
        bdo:paginationExtentStatement "1a-55a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM1180 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "chos mngon pa'i mdzod 'grel slob dpon dbyig gnyen rang gis mdzad pa las gnas gnyis pa dbang po bstan pa/"@bo-x-ewts .

    bdr:MW1NLM1180_O1NLM1180_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1180_O1NLM1180_003_001 ;
        bdo:authorshipStatement "slob dpon dbyig gnyen/"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7.5" ;
        bdo:contentLocation bdr:CLMW1NLM1180_O1NLM1180_003_001 ;
        bdo:contentWidth "54" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "61" ;
        bdo:hasTitle bdr:TTMW1NLM1180_O1NLM1180_003_001 ;
        bdo:inRootInstance bdr:MW1NLM1180 ;
        bdo:paginationExtentStatement "1a-54a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM1180 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "chos mngon pa'i mdzod 'grel slob dpon dbyig gnyen rang gis mdzad pa las gnas bzhi pa las bstan pa/"@bo-x-ewts .

    bdr:MW1NLM1180_O1NLM1180_004 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1180_O1NLM1180_004_001 ;
        bdo:authorshipStatement "slob dpon dbyig gnyen/"@bo-x-ewts ;
        bdo:colophon "chos mngon pa'i mdzod kyi tshig le'ur byas pa/ slob dpon shAkya'i dge slong dbyig gnyen gyis mdzad pa rdzogs so// //rgya gar kyi mkhan po dzi na mi tra dang/ zhu chen gyi lo tso ba ban+de dpal brtsegs kyis bsgyur cing zhus te gtan la phab pa'o// //chos sbyin rgya cher spel phyir dga' ldan phun tshogs gling du par du bsgrubs// sarba dirga u ta dza yan+tu// //"@bo-x-ewts ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "8" ;
        bdo:contentLocation bdr:CLMW1NLM1180_O1NLM1180_004_001 ;
        bdo:contentWidth "53" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "61.5" ;
        bdo:hasTitle bdr:TTMW1NLM1180_O1NLM1180_004_001 ;
        bdo:inRootInstance bdr:MW1NLM1180 ;
        bdo:paginationExtentStatement "1a-29a" ;
        bdo:partIndex 4 ;
        bdo:partOf bdr:MW1NLM1180 ;
        bdo:partTreeIndex "004" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "dga' ldan phun tshogs gling/"@bo-x-ewts ;
        skos:prefLabel "chos mngon pa mdzod/"@bo-x-ewts .

    bdr:O1NLM1180 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM1180 .

    bdr:TTMW1NLM1180_O1NLM1180_001B_001 a bdo:TitlePageTitle ;
        rdfs:label "chos mngon pa'i mdzod 'grel slob dpon dbyig gnyen rang gis mdzad pa las gnas dang po khams bstan pa/"@bo-x-ewts .

    bdr:TTMW1NLM1180_O1NLM1180_002_001 a bdo:TitlePageTitle ;
        rdfs:label "chos mngon pa'i mdzod 'grel slob dpon dbyig gnyen rang gis mdzad pa las gnas gnyis pa dbang po bstan pa/"@bo-x-ewts .

    bdr:TTMW1NLM1180_O1NLM1180_003_001 a bdo:TitlePageTitle ;
        rdfs:label "chos mngon pa'i mdzod 'grel slob dpon dbyig gnyen rang gis mdzad pa las gnas bzhi pa las bstan pa/"@bo-x-ewts .

    bdr:TTMW1NLM1180_O1NLM1180_004_001 a bdo:TitlePageTitle ;
        rdfs:label "chos mngon pa mdzod/"@bo-x-ewts .

    bdr:MW1NLM1180 bdo:hasPart bdr:MW1NLM1180_O1NLM1180_001B,
            bdr:MW1NLM1180_O1NLM1180_002,
            bdr:MW1NLM1180_O1NLM1180_003,
            bdr:MW1NLM1180_O1NLM1180_004 .
}

