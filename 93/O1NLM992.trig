@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM992 {
    bda:O1NLM992 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM992 ;
        adm:gitPath "93/O1NLM992.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM992 ;
        adm:logEntry bda:LG0NLMOO1NLM992_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM992_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM992_O1NLM992_001B_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 10 ;
        bdo:contentLocationInstance bdr:W1NLM992 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM992_O1NLM992_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 116 ;
        bdo:contentLocationInstance bdr:W1NLM992 ;
        bdo:contentLocationPage 11 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM992_O1NLM992_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 162 ;
        bdo:contentLocationInstance bdr:W1NLM992 ;
        bdo:contentLocationPage 117 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM992_O1NLM992_004_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 190 ;
        bdo:contentLocationInstance bdr:W1NLM992 ;
        bdo:contentLocationPage 163 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM992_O1NLM992_001B_001 a bdr:NLMId ;
        rdf:value "M0058905-001" .

    bdr:IDMW1NLM992_O1NLM992_002_001 a bdr:NLMId ;
        rdf:value "M0058905-002" .

    bdr:IDMW1NLM992_O1NLM992_003_001 a bdr:NLMId ;
        rdf:value "M0058905-003" .

    bdr:IDMW1NLM992_O1NLM992_004_001 a bdr:NLMId ;
        rdf:value "M0058905-004" .

    bdr:MW1NLM992_O1NLM992_001B a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM992_O1NLM992_001B_001 ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "8" ;
        bdo:contentLocation bdr:CLMW1NLM992_O1NLM992_001B_001 ;
        bdo:contentWidth "26" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "33" ;
        bdo:hasTitle bdr:TTMW1NLM992_O1NLM992_001B_001 ;
        bdo:inRootInstance bdr:MW1NLM992 ;
        bdo:paginationExtentStatement "1a-5b" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM992 ;
        bdo:partTreeIndex "001B" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "phyag na rdo rje'i grib sel ma/"@bo-x-ewts .

    bdr:MW1NLM992_O1NLM992_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM992_O1NLM992_002_001 ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM992_O1NLM992_002_001 ;
        bdo:contentWidth "27.5" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "32.5" ;
        bdo:hasTitle bdr:TTMW1NLM992_O1NLM992_002_001 ;
        bdo:inRootInstance bdr:MW1NLM992 ;
        bdo:paginationExtentStatement "1b-53a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM992 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bla ma dang mgon po 'jam pa'i dbyangs la phyag 'tshal lo/ /dus ngan snyigs ma'i mtsho brdol nas/ /sems can las ngan sna tshogs bsags/ /nad gdon gnod pa sna tshogs 'byung/ /de rnams sel phyir rgyal ba yis/ /thabs ni sna tshogs gsung pa rnams/ /'gro la phan phyir 'dir bsdus nas/ /blo dman rnams la bshad par bya/ /de la 'dir sems can rnams la phan par bya ba'i phyir/ /man ngag 'ga' zhig bri ba ni/"@bo-x-ewts .

    bdr:MW1NLM992_O1NLM992_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM992_O1NLM992_003_001 ;
        bdo:authorshipStatement "ngag dbang chos rgyal/"@bo-x-ewts ;
        bdo:colophon "ces dkon mchog gsum la skyabs su 'gro tshul gyi gdams pa rgya cher bshad pa khyad par gsum ldan zhes bya ba 'di/ de bar gshegs pa'i gsung rab dgongs 'grel dang bcas pa ji bzhin du 'byed pa la mkhas pa'i paN+Di ta chen po 'jam mgon bla ma blo bzang rgya mtsho zhal snga nas la sogs pa'i yongs 'dzin du ma'i zhabs ki padmo spyi bos len pa yul gangs can gyi skyabs 'gro ba 'khon dpal 'byor lhun grub kyis blo gsal zhing don du gnyer ba'i dun pa dang ldan pa ngag dbang chos rgyal gyis nan gyis bskul ba'i ngor rang dang gzhan la phan pa'i phyir ri sgo bkra shis chos rdzong mi phan rnam par rgyal ba'i sde chen por bya lo hor zla lnga pa'i tshe bzang po la sbyar ba 'di kyang rgyal ba'i bstan pa rin po yun ring du gnas par gyur cig / //dge legs 'phel/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM992_O1NLM992_003_001 ;
        bdo:contentWidth "38" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "44" ;
        bdo:hasTitle bdr:TTMW1NLM992_O1NLM992_003_001 ;
        bdo:inRootInstance bdr:MW1NLM992 ;
        bdo:note bdr:NTMW1NLM992_O1NLM992_003DC ;
        bdo:paginationExtentStatement "1a-23b" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM992 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dkon mchog gsum la skyabs su 'gro tshul gyi gdams pa zab mo snyan brgyud khyad par gsum ldan/"@bo-x-ewts .

    bdr:MW1NLM992_O1NLM992_004 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM992_O1NLM992_004_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM992_O1NLM992_004_001 ;
        bdo:contentWidth "38" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "44" ;
        bdo:hasTitle bdr:TTMW1NLM992_O1NLM992_004_001 ;
        bdo:inRootInstance bdr:MW1NLM992 ;
        bdo:paginationExtentStatement "1a-14a" ;
        bdo:partIndex 4 ;
        bdo:partOf bdr:MW1NLM992 ;
        bdo:partTreeIndex "004" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bstan bcos 'dul ba mdo rtsa ba'i spyi don TIk chen rin chen phreng ba'i dgongs rgyan las dus tshigs kyi rnam bzhag smra ba'i rgyan/"@bo-x-ewts .

    bdr:NTMW1NLM992_O1NLM992_003DC a bdo:Note ;
        bdo:noteText "hor zla lnga pa'i tshe/"@bo-x-ewts .

    bdr:O1NLM992 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM992 .

    bdr:TTMW1NLM992_O1NLM992_001B_001 a bdo:TitlePageTitle ;
        rdfs:label "phyag na rdo rje'i grib sel ma/"@bo-x-ewts .

    bdr:TTMW1NLM992_O1NLM992_002_001 a bdo:TitlePageTitle ;
        rdfs:label "bla ma dang mgon po 'jam pa'i dbyangs la phyag 'tshal lo/ /dus ngan snyigs ma'i mtsho brdol nas/ /sems can las ngan sna tshogs bsags/ /nad gdon gnod pa sna tshogs 'byung/ /de rnams sel phyir rgyal ba yis/ /thabs ni sna tshogs gsung pa rnams/ /'gro la phan phyir 'dir bsdus nas/ /blo dman rnams la bshad par bya/ /de la 'dir sems can rnams la phan par bya ba'i phyir/ /man ngag 'ga' zhig bri ba ni/"@bo-x-ewts .

    bdr:TTMW1NLM992_O1NLM992_003_001 a bdo:TitlePageTitle ;
        rdfs:label "dkon mchog gsum la skyabs su 'gro tshul gyi gdams pa zab mo snyan brgyud khyad par gsum ldan/"@bo-x-ewts .

    bdr:TTMW1NLM992_O1NLM992_004_001 a bdo:TitlePageTitle ;
        rdfs:label "bstan bcos 'dul ba mdo rtsa ba'i spyi don TIk chen rin chen phreng ba'i dgongs rgyan las dus tshigs kyi rnam bzhag smra ba'i rgyan/"@bo-x-ewts .

    bdr:MW1NLM992 bdo:hasPart bdr:MW1NLM992_O1NLM992_001B,
            bdr:MW1NLM992_O1NLM992_002,
            bdr:MW1NLM992_O1NLM992_003,
            bdr:MW1NLM992_O1NLM992_004 .
}

