@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM635 {
    bda:O1NLM635 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM635 ;
        adm:gitPath "8d/O1NLM635.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM635 ;
        adm:logEntry bda:LG0NLMOO1NLM635_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM635_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM635_O1NLM635_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 177 ;
        bdo:contentLocationInstance bdr:W1NLM635 ;
        bdo:contentLocationPage 2 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM635_O1NLM635_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 240 ;
        bdo:contentLocationInstance bdr:W1NLM635 ;
        bdo:contentLocationPage 178 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM635_O1NLM635_001_001 a bdr:NLMId ;
        rdf:value "M0058548-001" .

    bdr:IDMW1NLM635_O1NLM635_002_001 a bdr:NLMId ;
        rdf:value "M0058548-002" .

    bdr:MW1NLM635_O1NLM635_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM635_O1NLM635_001_001 ;
        bdo:authorshipStatement "grags pa rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "ces dpal rdo rje 'jigs byed kyi rim pa dang po'i dmar khrid dngos grub nor bu'i gtor mdzod ces bya ba 'di nyid ir du su or tog pe li'i shog nas dad gtong yon tan gyi rgyan ldan dge bsnyen ja lan dkon mchog skyabs kyis mthun rkyen sbyar nye chos ste chen po bkra shis 'khyil du par du bsgrubs pa'i tshe smon tshig 'di lta bu zhig dgos zhes par du sgrub pa po nyid kyis bskul ngor/ rgyal mkhan po grags pa rgyal mtshan gyis sbyar ba'i yi ge pa ni dka' bcu blo bzang dar rgyas kyis bgyis pa dza yan+tu/ /"@bo-x-ewts ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM635_O1NLM635_001_001 ;
        bdo:contentWidth "29" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "35" ;
        bdo:hasTitle bdr:TTMW1NLM635_O1NLM635_001_001 ;
        bdo:inRootInstance bdr:MW1NLM635 ;
        bdo:paginationExtentStatement "1a-88a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM635 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dpal rdo rje 'jigs byed kyi bskyed rim dmar khrid dngos grub gter mdzod/"@bo-x-ewts .

    bdr:MW1NLM635_O1NLM635_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM635_O1NLM635_002_001 ;
        bdo:authorshipStatement "ngag dbang blo bzang chos ldan/"@bo-x-ewts ;
        bdo:colophon "ces bsnyen yig dngos grub rgya mtsho zhes bya ba 'di ni rang gi nye 'khor ba dka' bcu shes dar ba sogs du ma dang/ sngon goms legs sbyar gyi bag chags 'thug po sad cing/ skye bas thob pa dang/ sbyangs pa'i rnam dpyod mchog tu yangs pa rgyal dbang dge 'dun rgya mtsho'i gdun sa chen po rgyal me tog thang gi seng ge'i khrir 'khor ba'i mkhan rin po che chos pa bde thang sprul sku blo bzang rin chen tshangs dbyangs phyogs las rnam par rgyal ba dang/ nad rigs pa dang gso ba rig pa la rnam dpyod yangs pa'i rab 'byams smra ba ngag dbang bstan 'dzin mdo sngags rab 'byams la sbyangs pa mi dman zhing rnam dpyod yangs pa'i rab 'byams smra ba bstan 'dzin grags pa/ skye bas thob pa'i blo gros kyi rtsal gsung rab la lta ba'i mig dang ldan zhing don gnyer dang bcas pa'i dge slong chos rgya mtsho dang tshul khrims dpal bzang la sogs pa'i don gnyer can mang pos @#/ /bskul ba'i ngor sbrang ban snyoms las pa ngag dbang blo bzang chos ldan gyis mtsho bdun gyi lha khang chen mor sbyar ba'i yi ge pa ni rnam dpyod dang brtson pa mi dman pa'i dge slong grags pa'o/ /'dis kyang rgyal ba'i bstan pa dar zhing rgyal la yun ring du gnas par gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM635_O1NLM635_002_001 ;
        bdo:contentWidth "29" ;
        bdo:dimHeight "8" ;
        bdo:dimWidth "35" ;
        bdo:hasTitle bdr:TTMW1NLM635_O1NLM635_002_001 ;
        bdo:inRootInstance bdr:MW1NLM635 ;
        bdo:paginationExtentStatement "1a-31a inc [*missing page from 24a-24b *]" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM635 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "bsnyen yig dngos grub rgya mtsho"@bo-x-ewts .

    bdr:O1NLM635 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM635 .

    bdr:TTMW1NLM635_O1NLM635_001_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal rdo rje 'jigs byed kyi bskyed rim dmar khrid dngos grub gter mdzod/"@bo-x-ewts .

    bdr:TTMW1NLM635_O1NLM635_002_001 a bdo:TitlePageTitle ;
        rdfs:label "bsnyen yig dngos grub rgya mtsho"@bo-x-ewts .

    bdr:MW1NLM635 bdo:hasPart bdr:MW1NLM635_O1NLM635_001,
            bdr:MW1NLM635_O1NLM635_002 .
}

