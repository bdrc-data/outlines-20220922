@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM164 {
    bda:O1NLM164 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM164 ;
        adm:gitPath "12/O1NLM164.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM164 ;
        adm:logEntry bda:LG0NLMOO1NLM164_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM164_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM164_O1NLM164_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 213 ;
        bdo:contentLocationInstance bdr:W1NLM164 ;
        bdo:contentLocationPage 2 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM164_O1NLM164_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 426 ;
        bdo:contentLocationInstance bdr:W1NLM164 ;
        bdo:contentLocationPage 214 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM164_O1NLM164_001_001 a bdr:NLMId ;
        rdf:value "M0058077-001" .

    bdr:IDMW1NLM164_O1NLM164_002_001 a bdr:NLMId ;
        rdf:value "M0058077-002" .

    bdr:MW1NLM164_O1NLM164_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM164_O1NLM164_001_001 ;
        bdo:authorshipStatement "brtson grus 'phags/"@bo-x-ewts ;
        bdo:colophon "dpal gsang ba 'dus pa'i zab mo bskyed pa'i rim pa dngos grub kyi 'dod pa 'jo ba zhes bya ba 'di ni/ rdo rje 'chang chen po brtson grus 'phags kyi zhal gyi bum bzang las 'ong ngo/ /'dis kyang gsang sngags zab mo'i lugs bzang phyogs dus kun tu dar zhing rgyas pa'i rgyur 'gyur cig / //dge'o// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM164_O1NLM164_001_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "52.5" ;
        bdo:hasTitle bdr:TTMW1NLM164_O1NLM164_001_001 ;
        bdo:inRootInstance bdr:MW1NLM164 ;
        bdo:paginationExtentStatement "1a-106a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM164 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dpal gsang ba 'dus pa'i zab mo bskyed pa'i rim pa dngos grub 'dod 'jo/"@bo-x-ewts .

    bdr:MW1NLM164_O1NLM164_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM164_O1NLM164_002_001 ;
        bdo:authorshipStatement "brtson grus 'phags/"@bo-x-ewts ;
        bdo:colophon "ces gsal sgron gter gyi bang mdzod kyi lde mig mkhas bdag 'byed zhes bya ba 'di ni/ rje bla de nyid la dad pa mi phyed pa'i dad pa cung @#/ /zad rnyed pa'i gtsang stod rgyud pa brtson 'grus 'phags kyis rgyal pas lung bstan pa'i gnas mchog dam pa/ dpal chos grwa chen po dga' ldan chos kyi pho brang du sbyar ba'o/ /'dis kyang bstan pa rin po che dar zhing rgyas pa dang khyad par du/ 'gro la phan bde 'byung ba'i sgo gcig pu/ / blo bzang grags pa'i mdo sngags bstan pa 'di/ /phyogs dus kun tu dar zhing rgyas gyur nas/ /'gro kun rdo rje 'dzin pa'i sa thob shog / //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM164_O1NLM164_002_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "52.5" ;
        bdo:hasTitle bdr:TTMW1NLM164_O1NLM164_002_001 ;
        bdo:inRootInstance bdr:MW1NLM164 ;
        bdo:paginationExtentStatement "1a-106a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM164 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "dga' ldan chos kyi pho brang/"@bo-x-ewts ;
        skos:prefLabel "gsal sgron gter gyi bang mdzod 'byed pa'i lde mig mkhas pa dag 'byed ces bya ba mthong na nyams dga' zin na blo bde/"@bo-x-ewts .

    bdr:O1NLM164 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM164 .

    bdr:TTMW1NLM164_O1NLM164_001_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal gsang ba 'dus pa'i zab mo bskyed pa'i rim pa dngos grub 'dod 'jo/"@bo-x-ewts .

    bdr:TTMW1NLM164_O1NLM164_002_001 a bdo:TitlePageTitle ;
        rdfs:label "gsal sgron gter gyi bang mdzod 'byed pa'i lde mig mkhas pa dag 'byed ces bya ba mthong na nyams dga' zin na blo bde/"@bo-x-ewts .

    bdr:MW1NLM164 bdo:hasPart bdr:MW1NLM164_O1NLM164_001,
            bdr:MW1NLM164_O1NLM164_002 .
}

