@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7299 {
    bda:LG0NLMOO1NLM7299_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7299  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7299 ;
        adm:gitPath         "61/O1NLM7299.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7299 ;
        adm:logEntry        bda:LG0NLMOO1NLM7299_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7299_O1NLM7299_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  168 ;
        bdo:contentLocationInstance  bdr:W1NLM7299 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7299_O1NLM7299_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  228 ;
        bdo:contentLocationInstance  bdr:W1NLM7299 ;
        bdo:contentLocationPage  169 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7299_O1NLM7299_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  330 ;
        bdo:contentLocationInstance  bdr:W1NLM7299 ;
        bdo:contentLocationPage  229 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7299_O1NLM7299_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0065217-001" .
    
    bdr:IDMW1NLM7299_O1NLM7299_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0065217-002" .
    
    bdr:IDMW1NLM7299_O1NLM7299_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0065217-003" .
    
    bdr:MW1NLM7299  bdo:hasPart  bdr:MW1NLM7299_O1NLM7299_001 , bdr:MW1NLM7299_O1NLM7299_002 , bdr:MW1NLM7299_O1NLM7299_003 .
    
    bdr:MW1NLM7299_O1NLM7299_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7299_O1NLM7299_001_001 ;
        bdo:authorshipStatement  "dkon mchog bstan pa'i sgron me/"@bo-x-ewts ;
        bdo:colophon        "ces bslab pa yongs su sbyong ba'i thabs gzhi gsum gsal byed ces bya ba 'di ni/ sgrub pa snying por mdzad pa lha skyabs slob dpon pa blo bzang dpal 'byor gyis gzhi gsum so so'i lag len dang/ don gyi gnad rnams gsal bar ston pa bstan @#/ /pa'i rgyun la phan pa zhig ci nas kyang dgos zhes yang dang yang du nan chen pos bskul ba la brten nas/ 'dul ba 'dzin pa ye shes rgyal mtshan gyis bal bod mtshams kyi dben gnas bkra shis bsam gtan gling du sbyar ba'o/ /'dis kyang thub pa'i bstan pa rin po che mi nub pa'i rgyal mtshan du gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7299_O1NLM7299_001_001 ;
        bdo:contentWidth    "49" ;
        bdo:dimHeight       "11.5" ;
        bdo:dimWidth        "59.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7299_O1NLM7299_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7299 ;
        bdo:paginationExtentStatement  "1a-84a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7299 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "bslab pa yongs su sbyong ba'i thabs gzhi gsum gsal byed/"@bo-x-ewts .
    
    bdr:MW1NLM7299_O1NLM7299_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7299_O1NLM7299_002_001 ;
        bdo:authorshipStatement  "ngag dbang dpal ldan/"@bo-x-ewts ;
        bdo:colophon        "ces dus tshigs kyi mchan 'grel dka' gnad sgo brgya 'byed pa'i lde mig zhes bya ba 'di ni/ snga sor yig char drangs pa'i gzhung 'ga' la zur mchan btab pa mthong bas rkyen byas te/ dad brtson rnam dpyod ldan pa dge slong blo bzang dkon mchog dang blo gsal chos la 'dun pa dge slong ngag dbang tshangs pa gnyis kyis 'phro rdzogs par byed dgos zhes bskul ba ltar/ slar bcos bcug dang mgo mjug tshang bar byas te/ btsun gzugs ngag dbang dpal ldan gyis nyin byed dbang po nab so'i dga' mar dgyes mgur rol ba'i dus su/ blo gsal mkhas mang 'byung gnas khal kha khu re chen mor 'khrigs chags su bkod pa 'dis kyang rgyal bstan rin po che dar zhing rgyas pa'i rgyur gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "8.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7299_O1NLM7299_002_001 ;
        bdo:contentWidth    "52" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "62" ;
        bdo:hasTitle        bdr:TTMW1NLM7299_O1NLM7299_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7299 ;
        bdo:paginationExtentStatement  "1a-30a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7299 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "dus tshigs kyi mchan 'grel dka' gnad sgo brgya 'byed pa'i lde mig"@bo-x-ewts .
    
    bdr:MW1NLM7299_O1NLM7299_003
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7299_O1NLM7299_003_001 ;
        bdo:authorshipStatement  "dkon mchog bstan pa'i sgron me/"@bo-x-ewts ;
        bdo:colophon        "'di ni btsun pa dkon mchog bstan pa'i sgron mes rang lo nyer gsum pa la lha ldan smon lam grwa skor gyi dus gtugs pas rtsas blo 'dzin ngos/ der mi gsal ba kha 'geng dgos rnams dpe klog gang bgyis dran tho rags pa tsam sug bris su btab pa las/ /phyis dbe rim par 'khyar te chos grwa chen po dpal ldan bkra shis 'khyil gyi dga' ram 'dzin grwI chos grwa ba rnams nas rtsod pa la phan skyed che bar 'dug pas par du 'khod dgos zhes par yig btab zin 'dug kyang/ spyir dang pho nas rang gi brjed tho tsam du bsam ste tshig sbyor sogs rtsom pa'i lugs su ma byas shing lhag par de skabs rtsod pa cha tshang ma song bas gong 'og ma 'brel pa lta bu re gnyis 'dug pa zhu dag cung zad bgyis yod kyang/ dngung blo gros can rnams kyis dpyad par mdzad na legs so/ /'dir 'bad legs byas gang+ga'i klung rgyun gyis/ nyes ltung rnyog pa'i dri ma rab sbyangs nas/ chos 'dul rgya mtsho'i gling du phyin gyur te/ bslab gsum nor bu'i mdzod la dbang 'byor shog / //sarba mang+ga laM// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "8.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7299_O1NLM7299_003_001 ;
        bdo:contentWidth    "52" ;
        bdo:dimHeight       "10.5" ;
        bdo:dimWidth        "62" ;
        bdo:hasTitle        bdr:TTMW1NLM7299_O1NLM7299_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM7299 ;
        bdo:paginationExtentStatement  "1a-51b" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM7299 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "'dul ba rgya mtsho'i dka' gnad mdor bsdus pa nor bu'i phreng ba/"@bo-x-ewts .
    
    bdr:O1NLM7299  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7299 .
    
    bdr:TTMW1NLM7299_O1NLM7299_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bslab pa yongs su sbyong ba'i thabs gzhi gsum gsal byed/"@bo-x-ewts .
    
    bdr:TTMW1NLM7299_O1NLM7299_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dus tshigs kyi mchan 'grel dka' gnad sgo brgya 'byed pa'i lde mig"@bo-x-ewts .
    
    bdr:TTMW1NLM7299_O1NLM7299_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "'dul ba rgya mtsho'i dka' gnad mdor bsdus pa nor bu'i phreng ba/"@bo-x-ewts .
}
