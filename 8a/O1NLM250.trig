@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM250 {
    bda:O1NLM250 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM250 ;
        adm:gitPath "8a/O1NLM250.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM250 ;
        adm:logEntry bda:LG0NLMOO1NLM250_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM250_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM250_O1NLM250_001B_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 72 ;
        bdo:contentLocationInstance bdr:W1NLM250 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM250_O1NLM250_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 126 ;
        bdo:contentLocationInstance bdr:W1NLM250 ;
        bdo:contentLocationPage 73 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM250_O1NLM250_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 154 ;
        bdo:contentLocationInstance bdr:W1NLM250 ;
        bdo:contentLocationPage 127 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM250_O1NLM250_001B_001 a bdr:NLMId ;
        rdf:value "M0058163-001" .

    bdr:IDMW1NLM250_O1NLM250_002_001 a bdr:NLMId ;
        rdf:value "M0058163-002" .

    bdr:IDMW1NLM250_O1NLM250_003_001 a bdr:NLMId ;
        rdf:value "M0058163-003" .

    bdr:MW1NLM250_O1NLM250_001B a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM250_O1NLM250_001B_001 ;
        bdo:authorshipStatement "shA sa na d+hI baM/"@bo-x-ewts ;
        bdo:colophon "ces pa 'di dag btsun gzugs shA sa na d+hI baM zhes pas sbyar ba'i yi ge pa ni dge slong ye shes tshe spel lo// //oM swa sti/ rgyal bstan spyi dang khyad par btsong kha pa'i/ /bstan pa'i snying po ches cher 'phel phyir du/ /dga' ldan bshad sgrub gling gi chos grwa 'dir/ chos sbyin spel phyir 'phrul chen spar 'di bsgrubs/ /mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "8" ;
        bdo:contentLocation bdr:CLMW1NLM250_O1NLM250_001B_001 ;
        bdo:contentWidth "54" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "61" ;
        bdo:hasTitle bdr:TTMW1NLM250_O1NLM250_001B_001 ;
        bdo:inRootInstance bdr:MW1NLM250 ;
        bdo:paginationExtentStatement "1a-36a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM250 ;
        bdo:partTreeIndex "001B" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "chos sbyin spel phyir/"@bo-x-ewts ;
        skos:prefLabel "shing rta'i srol 'byed dang skyes bu gsum gyi rnam bzhag gi 'bru bsnon dogs dpyod dang bcas pa blo gsar dga' bskyed/"@bo-x-ewts .

    bdr:MW1NLM250_O1NLM250_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM250_O1NLM250_002_001 ;
        bdo:colophon "'di ni u lu ki zhes pa'i sa char dpar 'di bsgrub pa'i dge bas 'dir 'brel thogs pa kun tshe rabs thams cad du bka' bod lnga la mkhas zhing rgyal ba gnyis pa'i bstan pa rin po che dang mjal nas lung rtogs kyi dam chos 'dzin par gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "8" ;
        bdo:contentLocation bdr:CLMW1NLM250_O1NLM250_002_001 ;
        bdo:contentWidth "57" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "61" ;
        bdo:hasTitle bdr:TTMW1NLM250_O1NLM250_002_001 ;
        bdo:inRootInstance bdr:MW1NLM250 ;
        bdo:paginationExtentStatement "1a-21b" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM250 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "shing rta srol 'byed rtsom 'phro/"@bo-x-ewts .

    bdr:MW1NLM250_O1NLM250_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM250_O1NLM250_003_001 ;
        bdo:authorshipStatement "bskal bzang lha dbang/"@bo-x-ewts ;
        bdo:colophon "zhes blo rigs kyi nam gtag gsal bar byas pa kun phan nyi ma'i 'od zer zhes bya ba 'di ni rnam dpyod mchog tu langs pa'i dge bshes rab 'byams pa ngag dbang chos bzang gis blo gsang ba rnams la phan 'dod kyi bsam pas bskul ba la brten nas/ dgon lung byams pa gling gi mkhan po u cu mu chen lha rams pa bskal bzang lha dbang gis/ u cu mu chen dbang gi chos kyi sde chen po theg chen chos 'khor gling du sbyar ba'i yi ge pa ni sdom brtson dge slong blo bzang snyan grags so/ /'dis kyang rgyal ba'i bstan pa rin po che phyogs dus thams cad du dar zhing rgyas te 'gro kun dge ba'i dpal la spyod par gyur cig / //sarba mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "9" ;
        bdo:contentLocation bdr:CLMW1NLM250_O1NLM250_003_001 ;
        bdo:contentWidth "57" ;
        bdo:dimHeight "10" ;
        bdo:dimWidth "61" ;
        bdo:hasTitle bdr:TTMW1NLM250_O1NLM250_003_001 ;
        bdo:inRootInstance bdr:MW1NLM250 ;
        bdo:paginationExtentStatement "7a-20a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM250 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "theg chen chos 'khor gling/"@bo-x-ewts ;
        skos:prefLabel "blo rigs kyi rnam gzhag gsal bar byas pa kun phan nyi ma'i 'od zer/"@bo-x-ewts .

    bdr:O1NLM250 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM250 .

    bdr:TTMW1NLM250_O1NLM250_001B_001 a bdo:TitlePageTitle ;
        rdfs:label "shing rta'i srol 'byed dang skyes bu gsum gyi rnam bzhag gi 'bru bsnon dogs dpyod dang bcas pa blo gsar dga' bskyed/"@bo-x-ewts .

    bdr:TTMW1NLM250_O1NLM250_002_001 a bdo:TitlePageTitle ;
        rdfs:label "shing rta srol 'byed rtsom 'phro/"@bo-x-ewts .

    bdr:TTMW1NLM250_O1NLM250_003_001 a bdo:TitlePageTitle ;
        rdfs:label "blo rigs kyi rnam gzhag gsal bar byas pa kun phan nyi ma'i 'od zer/"@bo-x-ewts .

    bdr:MW1NLM250 bdo:hasPart bdr:MW1NLM250_O1NLM250_001B,
            bdr:MW1NLM250_O1NLM250_002,
            bdr:MW1NLM250_O1NLM250_003 .
}

