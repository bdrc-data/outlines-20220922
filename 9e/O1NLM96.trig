@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM96 {
    bda:O1NLM96 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM96 ;
        adm:gitPath "9e/O1NLM96.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM96 ;
        adm:logEntry bda:LG0NLMOO1NLM96_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM96_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM96_O1NLM96_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 60 ;
        bdo:contentLocationInstance bdr:W1NLM96 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM96_O1NLM96_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 82 ;
        bdo:contentLocationInstance bdr:W1NLM96 ;
        bdo:contentLocationPage 61 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM96_O1NLM96_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 128 ;
        bdo:contentLocationInstance bdr:W1NLM96 ;
        bdo:contentLocationPage 83 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM96_O1NLM96_004_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 132 ;
        bdo:contentLocationInstance bdr:W1NLM96 ;
        bdo:contentLocationPage 129 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM96_O1NLM96_005_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 136 ;
        bdo:contentLocationInstance bdr:W1NLM96 ;
        bdo:contentLocationPage 133 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM96_O1NLM96_001_001 a bdr:NLMId ;
        rdf:value "M0058009-001" .

    bdr:IDMW1NLM96_O1NLM96_002_001 a bdr:NLMId ;
        rdf:value "M0058009-002" .

    bdr:IDMW1NLM96_O1NLM96_003_001 a bdr:NLMId ;
        rdf:value "M0058009-003" .

    bdr:IDMW1NLM96_O1NLM96_004_001 a bdr:NLMId ;
        rdf:value "M0058009-004" .

    bdr:IDMW1NLM96_O1NLM96_005_001 a bdr:NLMId ;
        rdf:value "M0058009-005" .

    bdr:MW1NLM96_O1NLM96_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM96_O1NLM96_001_001 ;
        bdo:authorshipStatement "dkon mchog 'jigs med dbang po/"@bo-x-ewts ;
        bdo:colophon "ces phyi nang gi grub mtha'i rnam bzhag mdor bsdus pa rin po che'i 'phreng ba zhes bya ba 'di ni dad pa brtson rnam dpyod dang ldan gu shri ngag dbang skal bzang dang/ dge slong ngag dbang bzang po gnyis kyis bskul ngor/ btsun pa dkon mchog 'jigs med dbang pos chu sprul chu stod zla ba'i yar tshes la sbyar ba'i yi ge pa ni rta mgrin tshe ring ngo// //mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM96_O1NLM96_001_001 ;
        bdo:contentWidth "44" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "52.5" ;
        bdo:hasTitle bdr:TTMW1NLM96_O1NLM96_001_001 ;
        bdo:inRootInstance bdr:MW1NLM96 ;
        bdo:paginationExtentStatement "1a-30a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM96 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "grub pa'i mtha'i rnam par bzhag pa rin po che'i 'phreng ba/"@bo-x-ewts .

    bdr:MW1NLM96_O1NLM96_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM96_O1NLM96_002_001 ;
        bdo:authorshipStatement "ye shes rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "zhes dpal mgon tra k+shad kyi gtor chog dngos grub kun 'byung zhes bya ba 'di ni byang @#/ /phyogs bstan pa'i gsal byed chen po khal kha rje btsun dam pas thugs dam zhal 'don mdzad rgyur chog sgrigs cha lag tshang ba 'di lta bu zhig gyis shig ces gangs can 'gro ba'i mgon po kun gzigs rgyal po'i dbang po'i bka' lung phebs pa spyi bos nos te paN chen blo bzang chos kyi rgyal mtshan gyis mdzad pa'i dpal mgon tA k+shad kyi gtor chog la gzhi byas thugs bzhed ltar kha 'gengs dgos pa rnams mkhas grub thams cad mkhyen pas mdzad pa'i mgon po'i gtor chog chen mo dang gser zhun ma sogs nas kha bskang ste khungs dang ldan par byas te dge slong ye shes rgyal mtshan gyis pho brang chen po'i gzim chung bde ba can du sbyar ba'o/ /'dis kyang rgyal ba'i bstan pa rin po che phyogs kun tu 'phel zhing rgyas par gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM96_O1NLM96_002_001 ;
        bdo:contentWidth "44" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "53.5" ;
        bdo:hasTitle bdr:TTMW1NLM96_O1NLM96_002_001 ;
        bdo:inRootInstance bdr:MW1NLM96 ;
        bdo:paginationExtentStatement "1a-11a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM96 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "dpal mgon tra k+shad la mchod gtor 'bul tshul dngos grub kun 'byung /"@bo-x-ewts .

    bdr:MW1NLM96_O1NLM96_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM96_O1NLM96_003_001 ;
        bdo:authorshipStatement "pa Tu sid+d+ha/"@bo-x-ewts ;
        bdo:colophon "ces pa 'di yang dad ldan gzim 'gug blo bzang tshe dbang nas @#/ /skyabs mgon mchog gi sprul pa'i sku rin po che'i zhabs brtan du bzhengs pa'i spar byang du d+harma swa mi'i ming gis sgro 'dogs pa pa Tu sid+d+has bris dge legs 'phel/ /mang+ga lam/ /"@bo-x-ewts ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM96_O1NLM96_003_001 ;
        bdo:contentWidth "40" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "49" ;
        bdo:hasTitle bdr:TTMW1NLM96_O1NLM96_003_001 ;
        bdo:inRootInstance bdr:MW1NLM96 ;
        bdo:paginationExtentStatement "1a-23a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM96 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "tshe dpag med lha dgu'i dkyil 'khor gyi cho ga nag 'gros su bkod pa/"@bo-x-ewts .

    bdr:MW1NLM96_O1NLM96_004 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM96_O1NLM96_004_001 ;
        bdo:conditionGrade 2 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM96_O1NLM96_004_001 ;
        bdo:contentWidth "44.5" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "49" ;
        bdo:hasTitle bdr:TTMW1NLM96_O1NLM96_004_001 ;
        bdo:inRootInstance bdr:MW1NLM96 ;
        bdo:paginationExtentStatement "1a-2a" ;
        bdo:partIndex 4 ;
        bdo:partOf bdr:MW1NLM96 ;
        bdo:partTreeIndex "004" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "'phags pa 'da' ka ye shes/"@bo-x-ewts .

    bdr:MW1NLM96_O1NLM96_005 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM96_O1NLM96_005_001 ;
        bdo:conditionGrade 2 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM96_O1NLM96_005_001 ;
        bdo:contentWidth "44.5" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "49" ;
        bdo:hasTitle bdr:TTMW1NLM96_O1NLM96_005_001 ;
        bdo:inRootInstance bdr:MW1NLM96 ;
        bdo:paginationExtentStatement "1a-2a" ;
        bdo:partIndex 5 ;
        bdo:partOf bdr:MW1NLM96 ;
        bdo:partTreeIndex "005" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "'phags pa 'da' ka ye shes/"@bo-x-ewts .

    bdr:O1NLM96 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM96 .

    bdr:TTMW1NLM96_O1NLM96_001_001 a bdo:TitlePageTitle ;
        rdfs:label "grub pa'i mtha'i rnam par bzhag pa rin po che'i 'phreng ba/"@bo-x-ewts .

    bdr:TTMW1NLM96_O1NLM96_002_001 a bdo:TitlePageTitle ;
        rdfs:label "dpal mgon tra k+shad la mchod gtor 'bul tshul dngos grub kun 'byung*/"@bo-x-ewts .

    bdr:TTMW1NLM96_O1NLM96_003_001 a bdo:TitlePageTitle ;
        rdfs:label "tshe dpag med lha dgu'i dkyil 'khor gyi cho ga nag 'gros su bkod pa/"@bo-x-ewts .

    bdr:TTMW1NLM96_O1NLM96_004_001 a bdo:TitlePageTitle ;
        rdfs:label "'phags pa 'da' ka ye shes/"@bo-x-ewts .

    bdr:TTMW1NLM96_O1NLM96_005_001 a bdo:TitlePageTitle ;
        rdfs:label "'phags pa 'da' ka ye shes/"@bo-x-ewts .

    bdr:MW1NLM96 bdo:hasPart bdr:MW1NLM96_O1NLM96_001,
            bdr:MW1NLM96_O1NLM96_002,
            bdr:MW1NLM96_O1NLM96_003,
            bdr:MW1NLM96_O1NLM96_004,
            bdr:MW1NLM96_O1NLM96_005 .
}

