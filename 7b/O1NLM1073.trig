@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM1073 {
    bda:O1NLM1073 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM1073 ;
        adm:gitPath "7b/O1NLM1073.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM1073 ;
        adm:logEntry bda:LG0NLMOO1NLM1073_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM1073_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM1073_O1NLM1073_001B_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 82 ;
        bdo:contentLocationInstance bdr:W1NLM1073 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM1073_O1NLM1073_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 140 ;
        bdo:contentLocationInstance bdr:W1NLM1073 ;
        bdo:contentLocationPage 83 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM1073_O1NLM1073_001B_001 a bdr:NLMId ;
        rdf:value "M0058986-001" .

    bdr:IDMW1NLM1073_O1NLM1073_002_001 a bdr:NLMId ;
        rdf:value "M0058986-002" .

    bdr:MW1NLM1073_O1NLM1073_001B a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1073_O1NLM1073_001B_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM1073_O1NLM1073_001B_001 ;
        bdo:contentWidth "48.5" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "54" ;
        bdo:hasTitle bdr:TTMW1NLM1073_O1NLM1073_001B_001 ;
        bdo:inRootInstance bdr:MW1NLM1073 ;
        bdo:paginationExtentStatement "1a-41a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM1073 ;
        bdo:partTreeIndex "001B" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "mon gro'i rdo rje gcod pa bzhin du 'gyur/ /dogs nas lha sa'i mtsho TIka bar gsar dang/ /khu re chen mo'i bar gyi stod cha gnyis/ yi ge mi 'dra btus nas zur du bris/"@bo-x-ewts .

    bdr:MW1NLM1073_O1NLM1073_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM1073_O1NLM1073_002_001 ;
        bdo:authorshipStatement "jam dbyangs bzhad pa'i rdo rje/"@bo-x-ewts ;
        bdo:colophon "rgyal bstan dri ma med pa 'gro ba kun gyi phan bde'i dpyid du dar zhing rgyas la yun ring du gnas pa na gsung rab dgongs 'grel dang bcam par thos bsam kyis 'dug pa la rag lus shing/ de yang gang la 'jug yul gsung rab kyi glegs pas rnams kyang dag gsal legs pa zhig dgos 'dug snyam pa'i bsam pa rgyun chags su yod mur \\u0f38 rgyal mchog lnga pa chen pos mdzad pa'i dbu phar mdzod 'grel bcas dang/ lhag par du kun mkhyen mtsho sna ba chen pos mdzad pa'i 'dul TIka rnam bshad nyi ma'i 'od zer bcas sdon nas par brkos su 'dug kyang/ deng dus 'gres skyon shin tu che bas/ /par gsar btab rnams ji bzhin phan thogs dkal ba mthor thos su gyur ba na 'di dag par gsar bzhengs kyi 'dun pa bskyed de/ chos srid 'phrin las kyi gtso 'dzin ji hran shan si de mo ho thog thu chen por gnas tshul zhib mo la byung 'phral srid skyong mchog nas kyang thugs 'gan chos bskyed kyis mkhan drung blo bzang tshul khrims/ /mkhan chung grags pa blo ldan/ rtse gnyer ngag dbang blo gsal bcas par brko'i do dam du bsgo bzhag dang/ par rnying rnams la yig skyon sogs yod med dang gsar brkos la yang zhu dag rung dgos rgyur yongs 'dzin phur lcog rin po che dang/ mtshan zhabs pa sgo mang dge bshes blo bzang ngag dbang/ blo gled ba dge bshes ye shes thabs mkhas/ bde langs pa bstan 'dzin 'phrin las 'od zer/ ser byis pa byams pa kun bzang bcas la zhib 'jug legs par dgos tshul ngos dang srid skyong yongs 'dzin rin po che zung nas nan smos bgyis par mtsho TIka par rnying gi yig chen yig chung ci rigs la bris non mang du yod rigs rnams bsal dgos la ma dpe khungs dag gcig dgos nges kyi gnas tshul dang/ gzhung sa'i gsung rten glegs bam khrod du mtsho TIka dris snying mi 'dra ba 'ga' zhig 'dug kyang de dag bar rnying dang cha 'dra sha stag tu 'dug pa dang/ gnyal grwa sgor dgon par kun mkhyen mtsho sna pa'i phyag dpe ngo ma'i steng nas 'bras blo gleng dge bshes tshul khrims rnam rgyal gyis zhus dag bgyis pa'i dpe zhig 'bram tre'u khams tshan du yong nges? 'dug pa 'phral bzhes bskyangs gnang mdzad dgos smos byung bar brten/ de bzhin 'phral blang bgyis bar gong gsal zhu dag pa rnams nas 'bras tre'i dpe dang/ dpe rnying bris ma bkras lhun par ma bcas go bsdur gyis par rnying la gong 'og 'khrugs pa dang chad lhag nor 'khrul yod rigs rnams bsal te yongs 'dzin zhus dag ru nga bgyis pa par gsar bzhengs kyi par yig 'dri mi drung @#/ /dbu chen bkra shis rab brtan dang/ drung dpal 'byor/ lha bris drung dpu ngag dbang dpal 'byor dang/ yig 'dri dpu chen don grub g.yul rgyal/ dpu chung phun tshogs rnam rgyal dang/ phun tshogs yar 'phel/ bar brko dpu chung bu tshe ring dang/ ngag dbang/ brgya byin/ padma ja dgra 'dul/ tshe brtan/ skun mdun/ tshe ring bsam grub bcas dang/ byings gras tshe nor/ don grub/ brgya byin/ bkra shis rdo rje/ tshe dbang phun tshogs/ mgon po/ tshe dbang dgra 'dul/ dpal 'byor/ don grub nor bu/ rdo rje/ mgon po rab brten/ ngag dbang/ rnam rgyal tshe ring/ karma nor bu/ skal ldan/ don grub dbang rgyal/ e tsid/ bkra shis/ nor bu don grub/ don grub rgyal po/ dar rgyas/ mda' lung/ phun tshogs dbang 'dus/ don grub rnam rgyal/ rdo rje rab brtan/ dngos grub phun tshogs/ bstan pa dar rgyas bcas dang/ bar shing b+jo mi shing b+jo drung dpu rgod skyes/ dpu chung rdo rje rab brtan/ byings skal bzang bstan dar/ slob ma rdo rje dbang rgyal bcas nas sor mo'i 'du byed ru ngas rab byung bco lnga pa'i chu 'phrug lo nang gsar bzhings bgyis pa'i par byang smon tshigs su shAkya'i btsun pa chos smra pa ngag dbang blo bzang thub bstan rgya mtsho 'jigs bral dbang phyug las rnam par rgyal ba'i sdes smon pa rnam par dag pa'i mtshams sbyor dang bcas te spel ba'o// //par 'di do mtshar lhun grub zil gnon rje 'bum lha khang du bzhugs/ gsung par 'di gra chos gra ba rnams la phan phyir bzhengs pas par yon sprod len mi chog /dge legs 'phel//"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM1073_O1NLM1073_002_001 ;
        bdo:contentWidth "47" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "54" ;
        bdo:hasTitle bdr:TTMW1NLM1073_O1NLM1073_002_001 ;
        bdo:inRootInstance bdr:MW1NLM1073 ;
        bdo:paginationExtentStatement "1a-29a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM1073 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "mon gro'i rdo rje gcod pa bzhin du 'gyur/ dogs nas lha sa'i mtsho TIka bar gsar dang/ khu re chen mo'i bar gyi smad cha gnyis/ yi ge mi 'dra btus nas zur du bris/"@bo-x-ewts .

    bdr:O1NLM1073 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM1073 .

    bdr:TTMW1NLM1073_O1NLM1073_001B_001 a bdo:TitlePageTitle ;
        rdfs:label "mon gro'i rdo rje gcod pa bzhin du 'gyur/ /dogs nas lha sa'i mtsho TIka bar gsar dang/ /khu re chen mo'i bar gyi stod cha gnyis/ yi ge mi 'dra btus nas zur du bris/"@bo-x-ewts .

    bdr:TTMW1NLM1073_O1NLM1073_002_001 a bdo:TitlePageTitle ;
        rdfs:label "mon gro'i rdo rje gcod pa bzhin du 'gyur/ dogs nas lha sa'i mtsho TIka bar gsar dang/ khu re chen mo'i bar gyi smad cha gnyis/ yi ge mi 'dra btus nas zur du bris/"@bo-x-ewts .

    bdr:MW1NLM1073 bdo:hasPart bdr:MW1NLM1073_O1NLM1073_001B,
            bdr:MW1NLM1073_O1NLM1073_002 .
}

