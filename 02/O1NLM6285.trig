@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM6285 {
    bda:LG0NLMOO1NLM6285_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM6285  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM6285 ;
        adm:gitPath         "02/O1NLM6285.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM6285 ;
        adm:logEntry        bda:LG0NLMOO1NLM6285_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM6285_O1NLM6285_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  112 ;
        bdo:contentLocationInstance  bdr:W1NLM6285 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM6285_O1NLM6285_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  148 ;
        bdo:contentLocationInstance  bdr:W1NLM6285 ;
        bdo:contentLocationPage  113 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM6285_O1NLM6285_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  186 ;
        bdo:contentLocationInstance  bdr:W1NLM6285 ;
        bdo:contentLocationPage  149 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM6285_O1NLM6285_004_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  190 ;
        bdo:contentLocationInstance  bdr:W1NLM6285 ;
        bdo:contentLocationPage  187 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM6285_O1NLM6285_005_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  262 ;
        bdo:contentLocationInstance  bdr:W1NLM6285 ;
        bdo:contentLocationPage  191 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM6285_O1NLM6285_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0064203-001" .
    
    bdr:IDMW1NLM6285_O1NLM6285_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0064203-002" .
    
    bdr:IDMW1NLM6285_O1NLM6285_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0064203-003" .
    
    bdr:IDMW1NLM6285_O1NLM6285_004_001
        a                   bdr:NLMId ;
        rdf:value           "M0064203-004" .
    
    bdr:IDMW1NLM6285_O1NLM6285_005_001
        a                   bdr:NLMId ;
        rdf:value           "M0064203-005" .
    
    bdr:MW1NLM6285  bdo:hasPart  bdr:MW1NLM6285_O1NLM6285_001 , bdr:MW1NLM6285_O1NLM6285_002 , bdr:MW1NLM6285_O1NLM6285_003 , bdr:MW1NLM6285_O1NLM6285_004 , bdr:MW1NLM6285_O1NLM6285_005 .
    
    bdr:MW1NLM6285_O1NLM6285_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM6285_O1NLM6285_001_001 ;
        bdo:authorshipStatement  "ye shes bstan 'dzin/"@bo-x-ewts ;
        bdo:colophon        "ces skyes tshoms lam bsgrigs nor bu'i phreng zhes bya ba 'di rje btsun la ma dam pa bshes gnyen kun spong chen po ye shes bstan 'dzin dpal zang po'i zhal snga nas kyi zhabs rdul spyi bos blangs pa bya bral tshul khrims nyi mas spor mu'i nags khrod du rdzogs par bgyis pa'o// 'dis kyang rgyal ba'i bka' dang 'dzam gling mdzes par byed pa'i mkhas grub chen po rnams kyi gzhung mtha' dag gdams mchog tu mthong ba'i rkyen byed par gyur cig /dge'o//"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM6285_O1NLM6285_001_001 ;
        bdo:contentWidth    "39" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "44" ;
        bdo:hasTitle        bdr:TTMW1NLM6285_O1NLM6285_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM6285 ;
        bdo:paginationExtentStatement  "1a-56a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM6285 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "skyes tshoms sbyar ba'i lam bsgrigs nor bu'i phreng /"@bo-x-ewts .
    
    bdr:MW1NLM6285_O1NLM6285_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM6285_O1NLM6285_002_001 ;
        bdo:authorshipStatement  "bsod nams rgya mtsho/"@bo-x-ewts ;
        bdo:colophon        "ces pa 'di ni bsod nams rgya mtshos rang nyid kyis bsgibs pa'i rgyud sde spyi'i rnam par bzhag pa gsang chen lam bzang gsal ba'i sgron me'i cha lag tu sbyar ba'i yi ge pa ni dad brtson rnam dpyod dang ldan pa bkra shis don rtogs kyis bgyis pa dge legs 'phel// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM6285_O1NLM6285_002_001 ;
        bdo:contentWidth    "37.5" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "44.5" ;
        bdo:hasTitle        bdr:TTMW1NLM6285_O1NLM6285_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM6285 ;
        bdo:paginationExtentStatement  "1a-18a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM6285 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "rgyud sde spyi'i don gsal ba'i sgron me'i cha lag tu nye bar mkho ba dbang bzhi pa'i don gsal byed/"@bo-x-ewts .
    
    bdr:MW1NLM6285_O1NLM6285_003
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM6285_O1NLM6285_003_001 ;
        bdo:authorshipStatement  "shA sa na d+h+wa dza/"@bo-x-ewts ;
        bdo:colophon        "dgra lha'i kwa 'dzugs rdo rje pha lam zhes bya ba 'di ni legs ldan tshogs bdag mgon po'i dgra lha bcu gsum dang /mnga' bdag gi gter byon dgra lha mched dgu/ legs ldan dgra chos nas byung ba'i kong tse lugs kyi dgra lha bcu gsum rnams rig 'dzin brgyud pa'i phyag bzhes bzhin bsgrigs te rang rang gi yig rnying byin rlabs che zhing kun la bkra shis pas phal cher rang sor bzhag /'don mi bde ba dang lhad snyam pa tsho bcos/ gzhung 'dir 'grel chags pa'i tshig sbyor gyi cha lag 'ga' mi bsnan ka med du song bas bsnan te/ \\u0f38dga' rigs kyi mdzod ban 'du gsum pa shA sa na d+h+wa dza'am ming gzhan dbyangs can dga' ba'i lang tshos bsgrigs pa dza yan+tu// //mang+ga laM//"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "5.5" ;
        bdo:contentLocation  bdr:CLMW1NLM6285_O1NLM6285_003_001 ;
        bdo:contentWidth    "35" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "43" ;
        bdo:hasTitle        bdr:TTMW1NLM6285_O1NLM6285_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM6285 ;
        bdo:paginationExtentStatement  "1a-19b" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM6285 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "dgra lha'i kwa 'dzugs rdo rje pha lam/"@bo-x-ewts .
    
    bdr:MW1NLM6285_O1NLM6285_004
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM6285_O1NLM6285_004_001 ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM6285_O1NLM6285_004_001 ;
        bdo:contentWidth    "40" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "44.5" ;
        bdo:hasTitle        bdr:TTMW1NLM6285_O1NLM6285_004_001 ;
        bdo:inRootInstance  bdr:MW1NLM6285 ;
        bdo:paginationExtentStatement  "1a-2b" ;
        bdo:partIndex       4 ;
        bdo:partOf          bdr:MW1NLM6285 ;
        bdo:partTreeIndex   "004" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "bzhi pa phra mo'i rnal 'byor ni/"@bo-x-ewts .
    
    bdr:MW1NLM6285_O1NLM6285_005
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM6285_O1NLM6285_005_001 ;
        bdo:authorshipStatement  "blo bzang bstan 'phel/"@bo-x-ewts ;
        bdo:colophon        "ces bla ma dam pa'i zhal shes zin bris nyams len bang mdzad ces bya ba 'di ni/ \\u0f38rje grub pa'i dbang phyug klong rdol bla ma rin po che ngag dbang blo bzang dpal bzang po dang /khyab bdag dang po'i sangs rgyas rgyal sras bskal bzang thub bstan 'jigs med rgya mtsho'i zhal snga nas dang /khyad par bka' drin mtshungs med thub dbang rdo rje 'chang dngos khri chen chos kyi rgyal po blo bzang ye shes bstan pa rab rgyas dpal bzang po sogs yongs 'dzin du ma'i zhabs rdul spyi bos reg cing /lam rim blo sbyong sogs kyi zab chos du ma thos pa'i drin las bong bu mi khyur chud pa'i skal bzang thob pa'i btsun gzugs blo bzang bstan spel zhes bya bas rang gi brjed thor bris pa'o/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM6285_O1NLM6285_005_001 ;
        bdo:contentWidth    "42.5" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "49" ;
        bdo:hasTitle        bdr:TTMW1NLM6285_O1NLM6285_005_001 ;
        bdo:inRootInstance  bdr:MW1NLM6285 ;
        bdo:paginationExtentStatement  "1a-36a" ;
        bdo:partIndex       5 ;
        bdo:partOf          bdr:MW1NLM6285 ;
        bdo:partTreeIndex   "005" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "bla ma dam pa'i zhal shes zin bris nyams len bang mdzod/"@bo-x-ewts .
    
    bdr:O1NLM6285  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM6285 .
    
    bdr:TTMW1NLM6285_O1NLM6285_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "skyes tshoms sbyar ba'i lam bsgrigs nor bu'i phreng*/"@bo-x-ewts .
    
    bdr:TTMW1NLM6285_O1NLM6285_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rgyud sde spyi'i don gsal ba'i sgron me'i cha lag tu nye bar mkho ba dbang bzhi pa'i don gsal byed/"@bo-x-ewts .
    
    bdr:TTMW1NLM6285_O1NLM6285_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dgra lha'i kwa 'dzugs rdo rje pha lam/"@bo-x-ewts .
    
    bdr:TTMW1NLM6285_O1NLM6285_004_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bzhi pa phra mo'i rnal 'byor ni/"@bo-x-ewts .
    
    bdr:TTMW1NLM6285_O1NLM6285_005_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bla ma dam pa'i zhal shes zin bris nyams len bang mdzod/"@bo-x-ewts .
}
