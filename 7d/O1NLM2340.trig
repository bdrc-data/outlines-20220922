@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM2340 {
    bda:O1NLM2340 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM2340 ;
        adm:gitPath "7d/O1NLM2340.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM2340 ;
        adm:logEntry bda:LG0NLMOO1NLM2340_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM2340_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM2340_O1NLM2340_001_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 144 ;
        bdo:contentLocationInstance bdr:W1NLM2340 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM2340_O1NLM2340_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 233 ;
        bdo:contentLocationInstance bdr:W1NLM2340 ;
        bdo:contentLocationPage 145 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM2340_O1NLM2340_001_001 a bdr:NLMId ;
        rdf:value "M0060258-001" .

    bdr:IDMW1NLM2340_O1NLM2340_002_001 a bdr:NLMId ;
        rdf:value "M0060258-002" .

    bdr:MW1NLM2340_O1NLM2340_001 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM2340_O1NLM2340_001_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7" ;
        bdo:contentLocation bdr:CLMW1NLM2340_O1NLM2340_001_001 ;
        bdo:contentWidth "29" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "35" ;
        bdo:hasTitle bdr:TTMW1NLM2340_O1NLM2340_001_001 ;
        bdo:inRootInstance bdr:MW1NLM2340 ;
        bdo:paginationExtentStatement "1a-74a" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM2340 ;
        bdo:partTreeIndex "001" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "'jam mgon bla ma tsong kha pa chen po gtso bor gyur ba'i skyabs gnas dam pa rnams la phyag mchod sogs bsag sbyang gi rim pa ji ltar bya ba'i tshul bsal bar bkod pa dge legs rgyas byed phan bde char 'bebs/"@bo-x-ewts .

    bdr:MW1NLM2340_O1NLM2340_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM2340_O1NLM2340_002_001 ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "7.5" ;
        bdo:contentLocation bdr:CLMW1NLM2340_O1NLM2340_002_001 ;
        bdo:contentWidth "30" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "35.5" ;
        bdo:hasTitle bdr:TTMW1NLM2340_O1NLM2340_002_001 ;
        bdo:inRootInstance bdr:MW1NLM2340 ;
        bdo:paginationExtentStatement "1a-45b" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM2340 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Manuscript ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "'jam mgon bla ma tsong kha pa chen po gtso bor gyur ba'i skyabs gnas dam pa rnams la phyag mchod sogs bsag sbyang gi rim pa ji ltar bya ba'i tshul bsal bar bkod pa dge legs rgyas byed phan bde char 'bebs/"@bo-x-ewts .

    bdr:O1NLM2340 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM2340 .

    bdr:TTMW1NLM2340_O1NLM2340_001_001 a bdo:TitlePageTitle ;
        rdfs:label "'jam mgon bla ma tsong kha pa chen po gtso bor gyur ba'i skyabs gnas dam pa rnams la phyag mchod sogs bsag sbyang gi rim pa ji ltar bya ba'i tshul bsal bar bkod pa dge legs rgyas byed phan bde char 'bebs/"@bo-x-ewts .

    bdr:TTMW1NLM2340_O1NLM2340_002_001 a bdo:TitlePageTitle ;
        rdfs:label "'jam mgon bla ma tsong kha pa chen po gtso bor gyur ba'i skyabs gnas dam pa rnams la phyag mchod sogs bsag sbyang gi rim pa ji ltar bya ba'i tshul bsal bar bkod pa dge legs rgyas byed phan bde char 'bebs/"@bo-x-ewts .

    bdr:MW1NLM2340 bdo:hasPart bdr:MW1NLM2340_O1NLM2340_001,
            bdr:MW1NLM2340_O1NLM2340_002 .
}

