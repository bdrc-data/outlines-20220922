@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7249 {
    bda:LG0NLMOO1NLM7249_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7249  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7249 ;
        adm:gitPath         "bf/O1NLM7249.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7249 ;
        adm:logEntry        bda:LG0NLMOO1NLM7249_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7249_O1NLM7249_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  118 ;
        bdo:contentLocationInstance  bdr:W1NLM7249 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7249_O1NLM7249_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  202 ;
        bdo:contentLocationInstance  bdr:W1NLM7249 ;
        bdo:contentLocationPage  119 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7249_O1NLM7249_003_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  256 ;
        bdo:contentLocationInstance  bdr:W1NLM7249 ;
        bdo:contentLocationPage  203 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7249_O1NLM7249_004_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  302 ;
        bdo:contentLocationInstance  bdr:W1NLM7249 ;
        bdo:contentLocationPage  257 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7249_O1NLM7249_005_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  346 ;
        bdo:contentLocationInstance  bdr:W1NLM7249 ;
        bdo:contentLocationPage  303 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7249_O1NLM7249_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0065167-001" .
    
    bdr:IDMW1NLM7249_O1NLM7249_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0065167-002" .
    
    bdr:IDMW1NLM7249_O1NLM7249_003_001
        a                   bdr:NLMId ;
        rdf:value           "M0065167-003" .
    
    bdr:IDMW1NLM7249_O1NLM7249_004_001
        a                   bdr:NLMId ;
        rdf:value           "M0065167-004" .
    
    bdr:IDMW1NLM7249_O1NLM7249_005_001
        a                   bdr:NLMId ;
        rdf:value           "M0065167-005" .
    
    bdr:MW1NLM7249  bdo:hasPart  bdr:MW1NLM7249_O1NLM7249_001 , bdr:MW1NLM7249_O1NLM7249_002 , bdr:MW1NLM7249_O1NLM7249_003 , bdr:MW1NLM7249_O1NLM7249_004 , bdr:MW1NLM7249_O1NLM7249_005 .
    
    bdr:MW1NLM7249_O1NLM7249_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7249_O1NLM7249_001_001 ;
        bdo:authorshipStatement  "dar ma rin chen/"@bo-x-ewts ;
        bdo:colophon        "dbu ma la 'jug pa'i bsdus pa'i don 'di ni mkhyen rab dang thugs rje gzhan dang mtshungs pa med pas mtha' yas pa'i 'gro ba la bden pa gnyis kyi de kho na nyin phyin ci ma log pa'i lam du bgrod pa'i dmigs bur gyur zhing/ khyad par du phyi nang gi rten cing 'brel bar 'byung ba'i chos rnams snang dus nyid nas mig 'phrul lta bur thugs su chud pa'i bla ma rje btsun chen po skyes mchog red 'da' pa'i zhal snga nas dang/ sde snod gsum dang rgyud sde bzhi'i dgongs pa'i don mtha' dag rten gyi gang zag gcig 'tshang rgya ba'i lam gyi yan lag tu 'gyur ba la ma rtogs pa dang log par rtog pa dang the tshom gyi dri ma spangs nas tshad ma'i lam las legs par drangs pa'i sgon sa lam gyi cha yongs su rdzogs pa la dar nyid sgrub pa thugs nyams su bzhes shing/ de nyid brtsa ba'i sgo nas ngag tshig dri ma med pas gdul bya rnams la legs par 'doms pa'i sgo nas rgyal ba rnams kyi dam pa'i chos ma lus pa rgyal ba'i dgongs pa ji lta ba bzhin du 'dzin par smon lam yongs su rdzogs pa'i sems dpa' chen po/ bla ma rje btsun blo bzang grags pa'i dpal gyi zhal snga nas kyi bka' drin las/ bden pa gnyis kyi rnam par bzhag pa la blo gros kyi snang ba cung zad thob pa rigs pa smra ba'i dge slong dar ma rin chen gyis/ rgyal ba'i 'byung gnas phyogs thams cad las rnam par rgyal ba'i sde 'brog ri bo che dge ldan rnam par rgyal ba'i gling du sbyar ba'i yi ge pa ni dka' bzhi pa kun dga' legs pa'i dpal lo/ /de yang rgyal tshab rjes mdzad pa'i dbu ma la 'jug pa'i sbyor TIka 'di ni dga' ldan khri rin po chie ngag dbang snyan grags pa'i nyi zhal @#/ /snga nas kyis kyang 'di'i dpe rgyun bod phyogs su mi 'dug /kun mkhyen 'jam dbyangs bzhad pa'i phyag dper yod 'gro bas rnyed byung na par du bkod tshe bstan rgyun la phan tshul gyi gsung nan mdzad pa bzhin bkra shis 'khyil gyi khri zur dpal mang tshang nas rtsol ba gnang te khyad dge dpon slob blo gros rgya mtsho'i phyag dpe 'khrul med cig rnyed 'dug pa 'di ni go bde la 'dus tshe ba blo gsal rnams la phan che bar mthong te/ /chos sde chen po bkra shis 'khyil du par du bsgrubs pa'o// //mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6" ;
        bdo:contentLocation  bdr:CLMW1NLM7249_O1NLM7249_001_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "9.5" ;
        bdo:dimWidth        "54.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7249_O1NLM7249_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7249 ;
        bdo:paginationExtentStatement  "1a-60a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7249 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "dbu ma la 'jug pa'i bsdus don/"@bo-x-ewts .
    
    bdr:MW1NLM7249_O1NLM7249_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7249_O1NLM7249_002_001 ;
        bdo:colophon        "ces shes rab kyi pha rol tu phyin pa'i mtha' dpyod nor bu'i phreng mdzes mkhas pa'i mgul rgyan las skabs gnyis pa'i don lesg par bshad zin to// //oM swasti/ gtso mdzad mi dbang hal ha thu shi gung/ /tho yon bla ma blo bzang bkra shis dang/ /lha chen skyabs dang dad ldan sbyin pa'i bdag /gsol dpon chos skyong bkra shis ltos bcas kyis/ /'dzad med nor gyis rgyu yon sbyar ba las/ /sku 'bum byams pa gling 'dir par bskrun mthus/ /gnas skabs bsam pa'i don kun 'grub pa dang/ /mthar thug sku bzhi go 'phang thob par shog / //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7249_O1NLM7249_002_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "8" ;
        bdo:dimWidth        "51" ;
        bdo:hasTitle        bdr:TTMW1NLM7249_O1NLM7249_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7249 ;
        bdo:paginationExtentStatement  "1a-42a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7249 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        bdo:sourcePrinteryStatement  "sku 'bum byams pa gling/"@bo-x-ewts ;
        skos:prefLabel      "shes rab kyi pha rol tu phyin pa'i mtha' dpyod nor bu'i phreng mdzes mkhas pa'i mgul rgyan las skabs gnyis pa/"@bo-x-ewts .
    
    bdr:MW1NLM7249_O1NLM7249_003
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7249_O1NLM7249_003_001 ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "6.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7249_O1NLM7249_003_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "8" ;
        bdo:dimWidth        "51" ;
        bdo:hasTitle        bdr:TTMW1NLM7249_O1NLM7249_003_001 ;
        bdo:inRootInstance  bdr:MW1NLM7249 ;
        bdo:paginationExtentStatement  "1a-27b" ;
        bdo:partIndex       3 ;
        bdo:partOf          bdr:MW1NLM7249 ;
        bdo:partTreeIndex   "003" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "shes rab kyi pha rol tu phyin pa'i mtha' dpyod nor bu'i 'phreng mdzes las skabs gnyis pa'i dka' gnas rnams gsal bar byed pa skal bzang 'jug ngogs/"@bo-x-ewts .
    
    bdr:MW1NLM7249_O1NLM7249_004
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7249_O1NLM7249_004_001 ;
        bdo:authorshipStatement  "dbyangs can dga' ba'i blo gros/"@bo-x-ewts ;
        bdo:colophon        "ces pa 'di ni dad brtson rnam dpyod yangs pa khe shag thu'i dge slong grags pa chos bzang gis yang yang nan tan che ba'i ngor/ mkhas grub dam pa mang po'i zhabs rdul spyi bo'i rgyan du thob pa'i gyi na pa dbyangs can dga' ba'i blo gros kyis rang gzhan la phan pa'i bsam pas g.yar khral du sbyar ba sarbe he tan+tu// sarba mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7249_O1NLM7249_004_001 ;
        bdo:contentWidth    "48" ;
        bdo:dimHeight       "9" ;
        bdo:dimWidth        "52" ;
        bdo:hasTitle        bdr:TTMW1NLM7249_O1NLM7249_004_001 ;
        bdo:inRootInstance  bdr:MW1NLM7249 ;
        bdo:paginationExtentStatement  "1a-23b" ;
        bdo:partIndex       4 ;
        bdo:partOf          bdr:MW1NLM7249 ;
        bdo:partTreeIndex   "004" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "dpal gsang ba 'dus pa 'phags lugs dang mthun pa'i sngags kyi sa lam rnam gzhag legs bshad skal bzang 'jug ngogs/"@bo-x-ewts .
    
    bdr:MW1NLM7249_O1NLM7249_005
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7249_O1NLM7249_005_001 ;
        bdo:authorshipStatement  "bstan 'dzin phun tshogs/"@bo-x-ewts ;
        bdo:colophon        "ces bdud nad gzhom pa'i gnyen po rtsi sman gyi nus pa rgyang sel rgyas par bshad pa gsal ston dri med shel gong zhes bya ba 'di ni snga nas rgyas bshad dri med shel 'phreng bgyis pa'i rtsa tshig phyogs gcig tu bsgrigs pa blong 'dzin bde dgos zhes rang slob 'ga' res bskul ngor dus mtha' sman 'bangs tha ma 'tsho mdzad bstan 'dzin phun tshogs kyis de'u dmar zab rgyas chos gling gi bsti gnas su sbyar ba 'gro ba kun gyi gsos su dge bar gyur cig / //mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7" ;
        bdo:contentLocation  bdr:CLMW1NLM7249_O1NLM7249_005_001 ;
        bdo:contentWidth    "46" ;
        bdo:dimHeight       "10" ;
        bdo:dimWidth        "54.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7249_O1NLM7249_005_001 ;
        bdo:inRootInstance  bdr:MW1NLM7249 ;
        bdo:paginationExtentStatement  "1a-22a" ;
        bdo:partIndex       5 ;
        bdo:partOf          bdr:MW1NLM7249 ;
        bdo:partTreeIndex   "005" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "bdud nad gzhom pa'i gnyen po rtsi sman gyi nus pa rkyang bshad gsal ston dri med shel gong /"@bo-x-ewts .
    
    bdr:O1NLM7249  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7249 .
    
    bdr:TTMW1NLM7249_O1NLM7249_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dbu ma la 'jug pa'i bsdus don/"@bo-x-ewts .
    
    bdr:TTMW1NLM7249_O1NLM7249_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "shes rab kyi pha rol tu phyin pa'i mtha' dpyod nor bu'i phreng mdzes mkhas pa'i mgul rgyan las skabs gnyis pa/"@bo-x-ewts .
    
    bdr:TTMW1NLM7249_O1NLM7249_003_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "shes rab kyi pha rol tu phyin pa'i mtha' dpyod nor bu'i 'phreng mdzes las skabs gnyis pa'i dka' gnas rnams gsal bar byed pa skal bzang 'jug ngogs/"@bo-x-ewts .
    
    bdr:TTMW1NLM7249_O1NLM7249_004_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dpal gsang ba 'dus pa 'phags lugs dang mthun pa'i sngags kyi sa lam rnam gzhag legs bshad skal bzang 'jug ngogs/"@bo-x-ewts .
    
    bdr:TTMW1NLM7249_O1NLM7249_005_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bdud nad gzhom pa'i gnyen po rtsi sman gyi nus pa rkyang bshad gsal ston dri med shel gong*/"@bo-x-ewts .
}
