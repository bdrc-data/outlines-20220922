@prefix adm: <http://purl.bdrc.io/ontology/admin/> .
@prefix bda: <http://purl.bdrc.io/admindata/> .
@prefix bdg: <http://purl.bdrc.io/graph/> .
@prefix bdo: <http://purl.bdrc.io/ontology/core/> .
@prefix bdr: <http://purl.bdrc.io/resource/> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM5375 {
    bda:O1NLM5375 a adm:AdminData ;
        adm:adminAbout bdr:O1NLM5375 ;
        adm:gitPath "c7/O1NLM5375.trig" ;
        adm:gitRepo bda:GR0016 ;
        adm:graphId bdg:O1NLM5375 ;
        adm:logEntry bda:LG0NLMOO1NLM5375_IDC001 ;
        adm:metadataLegal bda:LD_BDRC_CC0 ;
        adm:restrictedInChina false ;
        adm:status bda:StatusReleased .

    bda:LG0NLMOO1NLM5375_IDC001 a adm:InitialDataImport ;
        adm:logAgent "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate "2023-03-19T18:33:58.564040"^^xsd:dateTime ;
        adm:logMethod bda:BatchMethod .

    bdr:CLMW1NLM5375_O1NLM5375_001B_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 51 ;
        bdo:contentLocationInstance bdr:W1NLM5375 ;
        bdo:contentLocationPage 1 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5375_O1NLM5375_002_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 63 ;
        bdo:contentLocationInstance bdr:W1NLM5375 ;
        bdo:contentLocationPage 52 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5375_O1NLM5375_003_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 111 ;
        bdo:contentLocationInstance bdr:W1NLM5375 ;
        bdo:contentLocationPage 64 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5375_O1NLM5375_004_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 201 ;
        bdo:contentLocationInstance bdr:W1NLM5375 ;
        bdo:contentLocationPage 112 ;
        bdo:contentLocationVolume 1 .

    bdr:CLMW1NLM5375_O1NLM5375_005_001 a bdo:ContentLocation ;
        bdo:contentLocationEndPage 257 ;
        bdo:contentLocationInstance bdr:W1NLM5375 ;
        bdo:contentLocationPage 202 ;
        bdo:contentLocationVolume 1 .

    bdr:IDMW1NLM5375_O1NLM5375_001B_001 a bdr:NLMId ;
        rdf:value "M0063293-001" .

    bdr:IDMW1NLM5375_O1NLM5375_002_001 a bdr:NLMId ;
        rdf:value "M0063293-002" .

    bdr:IDMW1NLM5375_O1NLM5375_003_001 a bdr:NLMId ;
        rdf:value "M0063293-003" .

    bdr:IDMW1NLM5375_O1NLM5375_004_001 a bdr:NLMId ;
        rdf:value "M0063293-004" .

    bdr:IDMW1NLM5375_O1NLM5375_005_001 a bdr:NLMId ;
        rdf:value "M0063293-005" .

    bdr:MW1NLM5375_O1NLM5375_001B a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5375_O1NLM5375_001B_001 ;
        bdo:colophon "zhes pa 'di ni sku 'bum byams pa gling du dpar du bsgrub pa'o// mang+ga laM//"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM5375_O1NLM5375_001B_001 ;
        bdo:contentWidth "48.5" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "52.5" ;
        bdo:hasTitle bdr:TTMW1NLM5375_O1NLM5375_001B_001 ;
        bdo:inRootInstance bdr:MW1NLM5375 ;
        bdo:paginationExtentStatement "1a-27a inc [* missing page from 17a-18b *]" ;
        bdo:partIndex 1 ;
        bdo:partOf bdr:MW1NLM5375 ;
        bdo:partTreeIndex "001B" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "sku 'bum byams pa gling/"@bo-x-ewts ;
        skos:prefLabel "rang mtshan spyi mtshan gyi rnam gzhag rtsom 'phro/"@bo-x-ewts .

    bdr:MW1NLM5375_O1NLM5375_002 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5375_O1NLM5375_002_001 ;
        bdo:authorshipStatement "grags pa rgyal mtshan/"@bo-x-ewts ;
        bdo:colophon "ces pa 'di ni rang gi gam du 'khod pa gang ci'i las don la 'bad pa lhur len pa mdzod pa 'jam dbyangs brtson 'grus kyis nan tan chen pos bskul ngor/ \\u0f38rgyal mkhan po grags pa rgyal mtshan gyis phyogs thams cad las rnam par rgyal ba'i chos grwa chen po dpal ldan bkra shis 'khyil gyi gzim chung yid dga' chos 'phel du sbyar ba'i yi ge pa ni dge tshul blo bzang dar rgyas kyis bris pas zab lam gcod kyi gdams pa phyogs thams cad du dar zhing rgyas pa'i rgyur gyur cig / /sarba mang+ga laM//"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6" ;
        bdo:contentLocation bdr:CLMW1NLM5375_O1NLM5375_002_001 ;
        bdo:contentWidth "46" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "53.5" ;
        bdo:hasTitle bdr:TTMW1NLM5375_O1NLM5375_002_001 ;
        bdo:inRootInstance bdr:MW1NLM5375 ;
        bdo:paginationExtentStatement "1a-6a" ;
        bdo:partIndex 2 ;
        bdo:partOf bdr:MW1NLM5375 ;
        bdo:partTreeIndex "002" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        bdo:sourcePrinteryStatement "yid dga' chos 'phel/"@bo-x-ewts ;
        skos:prefLabel "zab lam gcod kyi rgyun 'khyer mkha' 'gro'i zhal lung /"@bo-x-ewts .

    bdr:MW1NLM5375_O1NLM5375_003 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5375_O1NLM5375_003_001 ;
        bdo:authorshipStatement "ngag dbang phun tshogs/"@bo-x-ewts ;
        bdo:colophon "ces ma cig khros ma nag gi mngon rtogs snyan brgyud kyi chu 'gu grub pa'i dbang phyug chos nyid rang grol nas brgyud pa'i rjes su g.yu ring ba bsod nams dbang phyug gi yig cha dang/ de las brgyud pa'i man ngag rje bka' drin can byams pa lha dbang rig 'dzin gyi yig cha gnyis /la gzhi byas te rang bzos ma bslad bar mngon dkyil shin tu 'khyer bde go sla'i tshul du ngag dbang phun tshogs kyis bre smad nags sprug tu phan 'dod lhag gsam kyis bkod pa la slar yang ngon 'gro'i chos dang mchod bstod cung zad spros pa gzhan nas blangs shing/ dngos grub blang ba gzhung du mi gsal yang brtan bzhugs sogs nas dgos par gsungs pa ltar len tshig sngags bcas gsar du brtsams pa dang/ tshogs mchod rgyas bsdus dang bsngo smon bkra shis thur du byung ba rnams chog khrigs su bkod pa'i sa bcad phran bu bsgrigs pa las go rim sogs yig cha rnying ba gnyis las zhus dag par bgyis shing legs chas brgyan te ma cig khros ma'i sgrub thabs shin tu tshang zhing gsal bar bkod pa 'di yang mkha' mnyam gyi sems can thams cad yum chen mo'i go 'phang du thob pa'i rgyur gyur cig / //'di'i lung brgyud ni mdzad pa po ngag dbang phun tshogs/ des dge slong blo bzang rab rgyas/ des bdag bsod nams grags pa la'o// //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "5.5" ;
        bdo:contentLocation bdr:CLMW1NLM5375_O1NLM5375_003_001 ;
        bdo:contentWidth "42" ;
        bdo:dimHeight "9.5" ;
        bdo:dimWidth "54" ;
        bdo:hasTitle bdr:TTMW1NLM5375_O1NLM5375_003_001 ;
        bdo:inRootInstance bdr:MW1NLM5375 ;
        bdo:paginationExtentStatement "1a-24a" ;
        bdo:partIndex 3 ;
        bdo:partOf bdr:MW1NLM5375 ;
        bdo:partTreeIndex "003" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "ma cig khros ma nag mo'i sgrub thabs mngon dkyil shin tu gsal ba go sla'i tshul du bkod pa/"@bo-x-ewts .

    bdr:MW1NLM5375_O1NLM5375_004 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5375_O1NLM5375_004_001 ;
        bdo:authorshipStatement "nor bzang rgya mtsho/"@bo-x-ewts ;
        bdo:colophon "byang chub sems dpa'i bslab bya don gsal zhes bya ba 'di rje nyid kyi rjes 'jug tu rlom pa bya ba btang pa nor bzang rgya mtshos 'o de gud rgyal gyi lha zhol dben gnas rin chen sgad du sbyar ba'o// 'dis kyang bstan pa dang sems can gyi don rgya chen po byed par nus gyur cig / //"@bo-x-ewts ;
        bdo:conditionGrade 5 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM5375_O1NLM5375_004_001 ;
        bdo:contentWidth "48" ;
        bdo:dimHeight "9" ;
        bdo:dimWidth "54" ;
        bdo:hasTitle bdr:TTMW1NLM5375_O1NLM5375_004_001 ;
        bdo:inRootInstance bdr:MW1NLM5375 ;
        bdo:paginationExtentStatement "1a-45b inc [* missing page from 29a-29b *]" ;
        bdo:partIndex 4 ;
        bdo:partOf bdr:MW1NLM5375 ;
        bdo:partTreeIndex "004" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 5 ;
        skos:prefLabel "byang chub sems dpa'i bslab bya don gsal sgron me/"@bo-x-ewts .

    bdr:MW1NLM5375_O1NLM5375_005 a bdo:Instance ;
        bf:identifiedBy bdr:IDMW1NLM5375_O1NLM5375_005_001 ;
        bdo:authorshipStatement "ngag dbang chos bzang/"@bo-x-ewts ;
        bdo:colophon "zhes yid dang kun gzhi'i rtsa 'grel gyi snying po bsdus ba glo gsal mgrin rgyan zhes bya ba 'di ni/ don gyi slad du mtshan nas smos te mi'u thung sku skyes skal bzang dbang rgyal gang de'i drin gyis nye bar 'tsho ba'i btsun gzugs rab 'byams pa ngag dbang chos bzang gis gsar bu ba rnams la phan pa'i phyir du dkar pa'i yid kis bslangs te bris pa 'dis kyang bstan pa dang 'gro ba yangs la phan thogs par gyur cig gu// //zhes pa 'di ni sku 'bum byams pa gling du dpar du bsgrubs// dge'o//"@bo-x-ewts ;
        bdo:conditionGrade 3 ;
        bdo:contentHeight "6.5" ;
        bdo:contentLocation bdr:CLMW1NLM5375_O1NLM5375_005_001 ;
        bdo:contentWidth "45" ;
        bdo:dimHeight "8.5" ;
        bdo:dimWidth "52" ;
        bdo:hasTitle bdr:TTMW1NLM5375_O1NLM5375_005_001 ;
        bdo:inRootInstance bdr:MW1NLM5375 ;
        bdo:paginationExtentStatement "1a-28a" ;
        bdo:partIndex 5 ;
        bdo:partOf bdr:MW1NLM5375 ;
        bdo:partTreeIndex "005" ;
        bdo:partType bdr:PartTypeText ;
        bdo:printMethod bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade 3 ;
        bdo:sourcePrinteryStatement "sku 'bum byams pa gling/"@bo-x-ewts ;
        skos:prefLabel "yid dang kun gzhi'i rtsa 'grel gyi snying po bsdus pa blo gsal mgrin rgyan/"@bo-x-ewts .

    bdr:O1NLM5375 a bdo:Outline ;
        bdo:authorshipStatement "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf bdr:MW1NLM5375 .

    bdr:TTMW1NLM5375_O1NLM5375_001B_001 a bdo:TitlePageTitle ;
        rdfs:label "rang mtshan spyi mtshan gyi rnam gzhag rtsom 'phro/"@bo-x-ewts .

    bdr:TTMW1NLM5375_O1NLM5375_002_001 a bdo:TitlePageTitle ;
        rdfs:label "zab lam gcod kyi rgyun 'khyer mkha' 'gro'i zhal lung*/"@bo-x-ewts .

    bdr:TTMW1NLM5375_O1NLM5375_003_001 a bdo:TitlePageTitle ;
        rdfs:label "ma cig khros ma nag mo'i sgrub thabs mngon dkyil shin tu gsal ba go sla'i tshul du bkod pa/"@bo-x-ewts .

    bdr:TTMW1NLM5375_O1NLM5375_004_001 a bdo:TitlePageTitle ;
        rdfs:label "byang chub sems dpa'i bslab bya don gsal sgron me/"@bo-x-ewts .

    bdr:TTMW1NLM5375_O1NLM5375_005_001 a bdo:TitlePageTitle ;
        rdfs:label "yid dang kun gzhi'i rtsa 'grel gyi snying po bsdus pa blo gsal mgrin rgyan/"@bo-x-ewts .

    bdr:MW1NLM5375 bdo:hasPart bdr:MW1NLM5375_O1NLM5375_001B,
            bdr:MW1NLM5375_O1NLM5375_002,
            bdr:MW1NLM5375_O1NLM5375_003,
            bdr:MW1NLM5375_O1NLM5375_004,
            bdr:MW1NLM5375_O1NLM5375_005 .
}

