@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:O1NLM7386 {
    bda:LG0NLMOO1NLM7386_IDC001
        a                   adm:InitialDataImport ;
        adm:logAgent        "buda-scripts/imports/NLM/import-outlines.py" ;
        adm:logDate         "2024-10-31T14:23:08.421222"^^xsd:dateTime ;
        adm:logMethod       bda:BatchMethod .
    
    bda:O1NLM7386  a        adm:AdminData ;
        adm:adminAbout      bdr:O1NLM7386 ;
        adm:gitPath         "f9/O1NLM7386.trig" ;
        adm:gitRepo         bda:GR0016 ;
        adm:graphId         bdg:O1NLM7386 ;
        adm:logEntry        bda:LG0NLMOO1NLM7386_IDC001 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  false ;
        adm:status          bda:StatusReleased .
    
    bdr:CLMW1NLM7386_O1NLM7386_001_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  174 ;
        bdo:contentLocationInstance  bdr:W1NLM7386 ;
        bdo:contentLocationPage  1 ;
        bdo:contentLocationVolume  1 .
    
    bdr:CLMW1NLM7386_O1NLM7386_002_001
        a                   bdo:ContentLocation ;
        bdo:contentLocationEndPage  278 ;
        bdo:contentLocationInstance  bdr:W1NLM7386 ;
        bdo:contentLocationPage  175 ;
        bdo:contentLocationVolume  1 .
    
    bdr:IDMW1NLM7386_O1NLM7386_001_001
        a                   bdr:NLMId ;
        rdf:value           "M0065304-001" .
    
    bdr:IDMW1NLM7386_O1NLM7386_002_001
        a                   bdr:NLMId ;
        rdf:value           "M0065304-002" .
    
    bdr:MW1NLM7386  bdo:hasPart  bdr:MW1NLM7386_O1NLM7386_001 , bdr:MW1NLM7386_O1NLM7386_002 .
    
    bdr:MW1NLM7386_O1NLM7386_001
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7386_O1NLM7386_001_001 ;
        bdo:colophon        "zhes chos mngon mdzod kyi mtha' dpyod thub bstan nor bu'i gter mdzod/ dus gsum rgyal ba'i bzhed don kun gsal las/ /gnas bzhi pa'i tshig don legs par bshad zin to/ /dge legs 'phel// //"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "8" ;
        bdo:contentLocation  bdr:CLMW1NLM7386_O1NLM7386_001_001 ;
        bdo:contentWidth    "55.5" ;
        bdo:dimHeight       "8.5" ;
        bdo:dimWidth        "62" ;
        bdo:hasTitle        bdr:TTMW1NLM7386_O1NLM7386_001_001 ;
        bdo:inRootInstance  bdr:MW1NLM7386 ;
        bdo:paginationExtentStatement  "1a-107a" ;
        bdo:partIndex       1 ;
        bdo:partOf          bdr:MW1NLM7386 ;
        bdo:partTreeIndex   "001" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "chos mngon pa'i mdzod kyi dgongs 'grel gyi bstan bcos thub bstan nor bu'i gter mdzod dus gsum rgyal ba'i bzhed don kun gsal zhes bya ba las gnas bzhi pa'i tshig don mtha' dpyod/"@bo-x-ewts .
    
    bdr:MW1NLM7386_O1NLM7386_002
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1NLM7386_O1NLM7386_002_001 ;
        bdo:colophon        "chos mngon pa'i mdzod kyi dgongs don gsal bar byed pa'i legs bshad nyin byed dbang po'i snang/ba las/ las bstan pa zhes bya ba gnas bzhi pa'i rnam par bshad pa'o/ /sarba mang+ga laM/ /"@bo-x-ewts ;
        bdo:conditionGrade  5 ;
        bdo:contentHeight   "7.5" ;
        bdo:contentLocation  bdr:CLMW1NLM7386_O1NLM7386_002_001 ;
        bdo:contentWidth    "52" ;
        bdo:dimHeight       "10" ;
        bdo:dimWidth        "61.5" ;
        bdo:hasTitle        bdr:TTMW1NLM7386_O1NLM7386_002_001 ;
        bdo:inRootInstance  bdr:MW1NLM7386 ;
        bdo:paginationExtentStatement  "1a-52a" ;
        bdo:partIndex       2 ;
        bdo:partOf          bdr:MW1NLM7386 ;
        bdo:partTreeIndex   "002" ;
        bdo:partType        bdr:PartTypeText ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:readabilityGrade  5 ;
        skos:prefLabel      "chos mngon pa'i mdzod kyi dgongs don gsal bar byed pa'i legs bshad nyin byed dbang po'i snang ba las las bstan pa zhes bya ba gnas bzhi pa'i rnam par bshad pa/"@bo-x-ewts .
    
    bdr:O1NLM7386  a        bdo:Outline ;
        bdo:authorshipStatement  "Initial outline data imported from the catalog produced at the National Library of Mongolia, in collaboration with Asian Legacy Library."@en ;
        bdo:outlineOf       bdr:MW1NLM7386 .
    
    bdr:TTMW1NLM7386_O1NLM7386_001_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "chos mngon pa'i mdzod kyi dgongs 'grel gyi bstan bcos thub bstan nor bu'i gter mdzod dus gsum rgyal ba'i bzhed don kun gsal zhes bya ba las gnas bzhi pa'i tshig don mtha' dpyod/"@bo-x-ewts .
    
    bdr:TTMW1NLM7386_O1NLM7386_002_001
        a                   bdo:TitlePageTitle ;
        rdfs:label          "chos mngon pa'i mdzod kyi dgongs don gsal bar byed pa'i legs bshad nyin byed dbang po'i snang ba las las bstan pa zhes bya ba gnas bzhi pa'i rnam par bshad pa/"@bo-x-ewts .
}
